<?php
	require_once "model/siparisModel.php";
	include_once("controller/kullaniciController.php");

	class siparisController
	{
		private $conn;
		private $siparisModel;
		private $kullaniciController;

		function __construct($db)
		{
			$this->conn = $db;
			$this->siparisModel = new siparisModel($this->conn);
			$this->kullaniciController = new kullaniciController($this->conn);
		}

		public function siparis($islem, $urunID, $diger = null)
		{
			$rData = false;
			if ($islem == "baslat") {
				$_SESSION['step'] = "baslangic";
				$_SESSION['urun'] = $this->siparisModel->urunDetay($urunID);
				if ($diger) {
					$_SESSION['diger'] = $diger;
				}
				$rData = true;
			}
			return $rData;
		}

		public function siparisKayit($eposta, $urunID, $aciklama, $il, $ilce, $mahalle, $ada, $parsel, $digerAciklama)
		{
			$kullaniciData = $this->kullaniciController->kullaniciData($eposta);
			$siparisData = $this->siparisModel->siparisKayit($kullaniciData['kullaniciID'], $urunID, $aciklama, $il, $ilce, $mahalle, $ada, $parsel, $digerAciklama);
			$_SESSION['step'] = "odeme";
			$_SESSION['siparisID'] = $siparisData['siparisID'];
			$_SESSION['siparis'] = $siparisData;
		}

		public function siparisOnay($siparisID)
		{
			return $this->siparisModel->siparisOnay($siparisID);
		}

		public function siparisDetay($siparisID)
		{
			return $this->siparisModel->siparisDetay($siparisID);
		}

		public function siparisList($siparisID)
		{
			return $this->siparisModel->siparisList($siparisID);
		}

		public function siparisListesi($type, $kullaniciID)
		{
			return $this->siparisModel->siparisListesi($type, $kullaniciID);
		}

		public function siparisGuncelle($kullanici, $bayi, $kullaniciID, $siparisID, $durum)
		{

			if ($kullanici) {
				$siparisData = $this->siparisDetay($siparisID);
				if ($kullaniciID == $siparisData['musteriID']) {
					$this->siparisModel->siparisGuncelle($siparisID, $durum);
				}
			}
			if ($bayi) {
				$siparisData = $this->siparisDetay($siparisID);
				if ($kullaniciID == $siparisData['bayiID']) {
					$this->siparisModel->siparisGuncelle($siparisID, $durum);
				}
			}

		}

		public function siparisDuzenle($siparisID, $il, $ilce, $mahalle, $ada, $parsel, $aciklama)
		{
			$this->siparisModel->siparisDuzenle($siparisID, $il, $ilce, $mahalle, $ada, $parsel, $aciklama);
			//echo $this->utility->Yonlendir('');
		}


		public function dosyaYukle($siparisID, $kullaniciID, $dosya, $aciklama)
		{
			return $this->siparisModel->dosyaYukle($siparisID, $kullaniciID, $dosya, $aciklama);
		}

		public function surecKayit($siparisID, $durum, $type, $kullaniciID)
		{
			if ($type == "musteri") {
				return $this->siparisModel->surecKayit($siparisID, $durum, 'musteri', $kullaniciID);
			} elseif ($type == "bayi") {
				return $this->siparisModel->surecKayit($siparisID, $durum, 'bayi', $kullaniciID);
			}
		}

	}