<?php

class dinamikController
{
    private $conn;

    function __construct($db)
    {
        $this->conn = $db;

    }

    public function sayfa($link)
    {
        //Standart Sayfa Verileri
        $sayfaData = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * from sayfa WHERE link='" . $link . "' "));

        $sayfaData['sayfaData'] = $this->sayfaData($link);

        //Diziye çevirilmiş sayfaData verisetine dil var ve veritabnında karşılığı var ise baslik ve icerik dil tablosundaki gibi değiştiriliyor.
        if (isset($_GET['Dil'])) {
            $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM sayfa_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $sayfaData['link'] . "'   "));
            if ($dilDB) {
                $sayfaData['baslik'] = $dilDB['baslik'];
                $sayfaData['icerik'] = $dilDB['icerik'];
            }
        }
        return $sayfaData;
    }

    public function sayfaData($link)
    {
        $data = [];
        $data['benzerSayfalar'] = [];
        //İlişkilendirmek istediğimiz sayfaya gidiyoruz.
        $sayfaData = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * from sayfa WHERE link='" . $link . "' "));
        //Dil seçenekleri
        if (isset($_GET['Dil'])) {
            $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM sayfa_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $link . "'   "));
            if ($dilDB) {
                $sayfaData['baslik'] = $dilDB['baslik'];
                $sayfaData['icerik'] = $dilDB['icerik'];
            }
        }

        if ($sayfaData) {
            if ($sayfaData['ustSayfaID'] == 0) {
                $benzerSayfalar = [];
                $sorgu = mysqli_query($this->conn, "SELECT sayfaID,baslik,altbaslik,resim,link FROM sayfa WHERE ustSayfaID='" . $sayfaData['sayfaID'] . "' ");
                while ($row = mysqli_fetch_assoc($sorgu)) {
                    $benzerSayfalar[$row['sayfaID']]['baslik'] = $row['baslik'];
                    $benzerSayfalar[$row['sayfaID']]['altbaslik'] = $row['altbaslik'];
                    $benzerSayfalar[$row['sayfaID']]['resim'] = $row['resim'];
                    $benzerSayfalar[$row['sayfaID']]['link'] = $row['link'];
                    if (isset($_GET['Dil'])) {
                        $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM sayfa_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $row['link'] . "'   "));
                        if ($dilDB) {
                            $benzerSayfalar[$row['sayfaID']]['baslik'] = $dilDB['baslik'];
                            $benzerSayfalar[$row['sayfaID']]['altbaslik'] = $dilDB['icerik'];
                        }
                    }
                }
                $data['benzerSayfalar'] = $benzerSayfalar;
                $data['anaMenu'] = true;
                $data['altMenu'] = false;
            } else {
                $data['anaMenu'] = false;
                $data['altMenu'] = true;
                $data['ustSayfaID'] = $sayfaData['ustSayfaID'];
                $ustSayfaDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * from sayfa WHERE sayfaID='" . $data['ustSayfaID'] . "' "));
                if ($ustSayfaDB) {
                    $data['ustSayfaLink'] = $ustSayfaDB['link'];
                    $data['ustSayfaBaslik'] = $ustSayfaDB['baslik'];
                    if (isset($_GET['Dil'])) {
                        $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * from sayfa_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $ustSayfaDB['link'] . "' "));
                        if ($dilDB) {
                            $data['ustSayfaBaslik'] = $dilDB['baslik'];
                        }
                    }
                    $benzerSayfalar = [];
                    $sorgu = mysqli_query($this->conn, "SELECT sayfaID,baslik,altbaslik,resim,link FROM sayfa WHERE ustSayfaID='" . $ustSayfaDB['sayfaID'] . "' ");
                    while ($row = mysqli_fetch_assoc($sorgu)) {
                        $benzerSayfalar[$row['sayfaID']]['baslik'] = $row['baslik'];
                        $benzerSayfalar[$row['sayfaID']]['altbaslik'] = $row['altbaslik'];
                        $benzerSayfalar[$row['sayfaID']]['resim'] = $row['resim'];
                        $benzerSayfalar[$row['sayfaID']]['link'] = $row['link'];
                        if (isset($_GET['Dil'])) {
                            $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM sayfa_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $row['link'] . "'   "));
                            if ($dilDB) {
                                $benzerSayfalar[$row['sayfaID']]['baslik'] = $dilDB['baslik'];
                                $benzerSayfalar[$row['sayfaID']]['altbaslik'] = $dilDB['icerik'];
                            }
                        }
                    }
                    $data['benzerSayfalar'] = $benzerSayfalar;

                } else {
                    $data['ustSayfaLink'] = null;
                    $data['ustSayfaBaslik'] = null;
                }
            }

        }
        return $data;
    }


}

