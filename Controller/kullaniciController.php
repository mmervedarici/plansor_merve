<?php
require_once "model/kullaniciModel.php";

class kullaniciController
{
    private $conn;
    private $kullaniciModel;
    private $utility;

    function __construct($db)
    {
        $this->conn = $db;
        $this->utility = new utility();
        $this->kullaniciModel = new kullaniciModel($this->conn);
    }

    public function uyeKayit($type, $eposta, $sifre, $adSoyad, $telefon, $vergiDairesi, $vergiNo, $firmaAdi,$il, $ilce,$ip)
    {
        $data = [];
        $kontrol = $this->kullaniciModel->kullaniciData($eposta);
        if (!$kontrol) {
            if ($type == 1) {
                $data['data'] = $this->kullaniciModel->uyeKayit($type, $eposta, $sifre, $adSoyad, $telefon, null,
	                null, null, $il, $ilce, $ip);
                $this->uyeGiris($eposta, $sifre);
                $data['status'] = 200;
                return $data;
            } elseif ($type == 2) {
                $data['data'] = $this->kullaniciModel->uyeKayit($type, $eposta, $sifre, $adSoyad, $telefon,
	                $vergiDairesi, $vergiNo, $firmaAdi, $il, $ilce, $ip);
                $this->uyeGiris($eposta, $sifre);
                $data['status'] = 200;
                return $data;
            }
        } else {
            $data['errMsg'] = "Eposta Kullanılıyor";
            $data['status'] = 403;
            return $data;
        }
    }

    public function uyeGiris($eposta, $sifre)
    {
        $uyeGiris = $this->kullaniciModel->uyeGiris($eposta, md5($sifre));
        if ($uyeGiris) {
            $data['status'] = 200;
            $userData = $this->kullaniciModel->kullaniciData($eposta);
            $_SESSION['hbt_login'] = true;
            $_SESSION['hbt_user'] = $eposta;
            $_SESSION['hbt_seviye'] = $userData['seviye'];
            $_SESSION['hbt_adSoyad'] = $userData['adSoyad'];
            $_SESSION['hbt_telefon'] = $userData['telefon'];
            $_SESSION['hbt_kullaniciID'] = $userData['kullaniciID'];
        } else {
            $data['status'] = 401;
            $data['errMsg'] = "hatalı kullanıcı";
        }
        return $data;
    }

    public function uyeCikis()
    {
        session_destroy();
        echo $this->utility->Yonlendir('./');
    }

    public function kullaniciData($eposta)
    {
        return $this->kullaniciModel->kullaniciData($eposta);
    }

    public function hesapData()
    {
        $hesapData['login'] = 0;
        $hesapData['seviye'] = 1;
        if (isset($_SESSION['hbt_login'])) {
            $hesapData['login'] = 1;
            $hesapData['seviye'] = $_SESSION['hbt_seviye'];
            $hesapData['adSoyad'] = $_SESSION['hbt_adSoyad'];
            $hesapData['eposta'] = $_SESSION['hbt_user'];
            $hesapData['data'] = $this->kullaniciModel->kullaniciData($_SESSION['hbt_user']);
        }
        return $hesapData;
    }

    public function profilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce)
    {
        $this->kullaniciModel->profilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce);
        echo $this->utility->Yonlendir('');
    }

	public function bayiProfilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce, $vergiDairesi, $vergiNo, $firmaAdi)
	{
		$this->kullaniciModel->bayiProfilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce,$vergiDairesi, $vergiNo, $firmaAdi);
		echo $this->utility->Yonlendir('');
	}


	public function sifreGuncelle($eposta, $eskiSifre, $sifre)
    {
        $mevcutSifre = $this->kullaniciModel->kullaniciData($eposta)['sifre'];
        if (md5($eskiSifre) != $mevcutSifre) {
            return $data['status'] = 400;

        }
        return $this->kullaniciModel->sifreGuncelle($eposta, $sifre);
    }

}
