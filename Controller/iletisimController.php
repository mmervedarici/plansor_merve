<?php
include_once('Core/XML2Array.php');


class iletisimController
{
    private $conn;

    function __construct($db)
    {
        $this->conn = $db;

    }

    public function iletisimEkle($adSoyad, $eposta, $telefon, $mesaj, $ip1, $ip2, $ip3)
    {
        date_default_timezone_set('Europe/Istanbul');
        $olusturulmaTarihi = date("Y-m-d H:i");

        $sql = "INSERT INTO iletisim(adSoyad,eposta,telefon,mesaj, ip1, ip2, ip3,olusturulmaTarihi ) VALUES ('" . $adSoyad . "','" . $eposta . "','" . $telefon . "','" . $mesaj . "', '" . $ip1 . "', '" . $ip2 . "', '" . $ip3 . "','" . $olusturulmaTarihi . "');";
        mysqli_query($this->conn, $sql);
        // if (mysqli_query($this->conn, $sql)) {
        // 	echo "Mesaj alındı ! ^.^ !";

        // } else {
        // 	echo "Mesaj alınamadı \o/";
        // }

        return "ok.";
    }

    public function telefonFormat($telefon)
    {
        $telefon = preg_replace("/[^0-9]/", "", $telefon);
        $first = substr("$telefon", 0, 1);
        if ($first == "0") {
            $telefon = substr($telefon, 1);
        }

        $doksan = substr("$telefon", 0, 2);
        if ($doksan = "0") {
            $new_telefon = "Gecersiz: Ulke kodu TR degil.";
        } else {
            $numara = substr($telefon, 0);
            if (substr("$numara", 0, 1) == "0") {
                $numara = substr($numara, 1);
            }

            if (strlen($numara) != "10") {
                $new_telefon = "$numara";
            } else {
                $new_telefon = "$numara";
            }
        }
        return $new_telefon;
    }

    public function sendRequest($send_xml)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.iletimerkezi.com/v1/send-sms');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $send_xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        $result = curl_exec($ch);

        return $result;
    }

    public function smsGonder($isim, $telefon, $mesaj)
    {
        $date = "";
        $telefon = $this->telefonFormat($telefon);
        $isim = stripslashes($isim);
        $isim = substr($isim, 0, 20);
        $siteadresi = "www.plansor.com.tr";
        $icerik = $mesaj;                  // iletisim formundan gelen mesaj
        $username = '5063916061';             // iletimerkezi.com üye olurken kullandığınız gsm numarası
        $password = 'hakanreis5819';          // iletimerkezi.com üye olurken kullandığınız şifreniz
        $orgin_name = 'Hakan BT';               // iletimerkezi.com onaylanmış başlık talebiniz
        $yonetici_telefonlari[] = '5320586313'; // mesajın iletilmesini istediğiniz kişinin cep telefonu numarası
        $musteri_tel[] = $telefon;
        $date1 = date('Y-m-d H:i:s');
        $date = date_create("$date");
        $yil = date_format($date, "Y");
        $ay = date_format($date, "m");
        $gun = date_format($date, "d");
        $saat = date('H:i');
        $tarih = $gun . "/" . $ay . "/" . $yil . " " . $saat;


        $xml =
<<<EOS
<request>
<authentication>
<username>{$username}</username>
<password>{$password}</password>
</authentication>
<order>
<sender>{$orgin_name}</sender>
<sendDateTime>{$tarih}</sendDateTime>
<message>
<text><![CDATA[{$isim}, $siteadresi, $icerik
www.hakanbt.com.tr]]></text>
<receipents>
<number>{$yonetici_telefonlari[0]}</number>
<number>{$musteri_tel[0]}</number>                
</receipents>
</message>
</order>
</request>
EOS;

        $result = $this->sendRequest($xml);
        $array = XML2Array::createArray($result);
        return $array;
    }


}

