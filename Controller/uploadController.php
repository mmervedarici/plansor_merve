<?php

class uploadController
{
	public function uploadConfig()
	{
		$config['maxBoyut'] = 20000000;
		$config['dosyaTip'] = array(

			"image/x-png", // png A
			"image/png", // png B
			"image/bmp", // bmp
			"image/pjpeg", // jpg
			"image/jpeg", // jpg
			"image/jpg", // jpg
			"image/gif", // gif
			"application/msword", // doc
			"extension/docx",
			"application/vnd.ms-excel", // xls
			"extension/xlsx",
			"application/vnd.ms-powerpoint", // ppt, pps
			"application/pdf", // pdf


		);
		return $config;
	}

    public function dosyaAdi($dosyaAdi)
    {
        $find = array('Ç', 'Ş', 'Ğ', 'Ü', 'İ', 'Ö', 'ç', 'ş', 'ğ', 'ü', 'ö', 'ı', '+', '#', 'Ä±',  ',', ' - ');
        $replace = array('c', 's', 'g', 'u', 'i', 'o', 'c', 's', 'g', 'u', 'o', 'i', '-', '-', 'i',  '', '-');
        $dosyaAdi = strtolower(str_replace($find, $replace, $dosyaAdi));
        $dosyaAdi = trim(preg_replace('/\s+/', ' ', $dosyaAdi));
        $dosyaAdi = str_replace(' ', '-', $dosyaAdi);
        return $dosyaAdi;
    }

    public function dosyaYukle($dosya)
    {
        $dosyaTip = $this->uploadConfig()['dosyaTip'];
        $maxBoyut = $this->uploadConfig()['maxBoyut'];
        $dosyaAdi = $this->dosyaAdi($dosya['name']);
        $dosyaAdi = rand(10, 10000) . "-" .$dosyaAdi;
        $dizin = "userFiles/upload/";
        if (is_uploaded_file($dosya['tmp_name'])) {
            if (in_array($dosya['type'], $dosyaTip)) {
                if($dosya['size'] < $maxBoyut){
                    move_uploaded_file($dosya['tmp_name'], $dizin.$dosyaAdi);
                    $data['status'] = 200;
                    $data['dosyaAdi'] = $dosyaAdi;
                    return $data;
                }else{
                    $data['status'] = 400;
                    $data['errMsg'] = "Dosya boyutu izin verilan boyutun dışında.";
                    return $data;
                }
            } else {
                $data['status'] = 400;
                $data['errMsg'] = "Dosya uzantısı uygun değil";
                return $data;
            }
        } else {
            $data['status'] = 400;
            $data['errMsg'] = "Geçersiz istek";
            return $data;
        }
    }


}