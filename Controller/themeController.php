<?php

class themeController
{
    private $conn;
    private $siteAyar;


    function __construct($db)
    {
        $this->conn = $db;
        $this->siteAyar = new config($db);

    }

    public function activeLink($link = 'anasayfa')
    {

        if (isset($_GET['Sayfa'])) {
            if ($_GET['Sayfa'] == $link) {
                $class = "active";
            } else {
                $class = "";
            }
        } elseif (empty($_GET['Sayfa']) and $link == 'anasayfa') {
            $class = "active";
        }
        return $class;
    }


    public function seoKontrol($link)
    {
        $seoData = [];
        $seoDb = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT view,link,baslik,icerik,sayac FROM seo WHERE link='" . $link . "' "));
        if ($seoDb) {
            $seoData = $seoDb;
            if (file_exists('View/pages/' . $seoData['view'] . '.php')) {
                //Sayac SEO ya eklencek.
                mysqli_query($this->conn,"UPDATE seo SET sayac='".($seoDb['sayac']+1)."' WHERE link='" . $link . "'  ");
                $seoData['status'] = true;
                $seoData['title'] = $seoDb['baslik'];
                $seoData['description'] = $seoDb['icerik'];
                $seoData['link'] = $seoDb['link'];
                if (isset($_GET['Dil'])) {

                    if ($_GET['Dil'] == 'tr') {
                    } else {
                        $seoDil = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT baslik,icerik FROM seo_dil WHERE link='" . $link . "' and dil='" . $_GET['Dil'] . "' "));
                        if ($seoDil) {
                            $seoData['title'] = $seoDil['baslik'];
                            $seoData['description'] = $seoDil['icerik'];
                        }
                    }
                }
            } else {
                $seoData['status'] = false;
                if (isset($_GET['Dil'])) {
                    $dilKod = str_replace("/", "", $_GET['Dil']);
                    $seoData['title'] = $this->siteAyar->dilCevir($dilKod)['statik']['title'];
                    $seoData['description'] = $this->siteAyar->dilCevir($dilKod)['statik']['description'];

                } else {
                    $seoData['title'] = $this->siteAyar->dilCevir()['statik']['title'];
                    $seoData['description'] = $this->siteAyar->dilCevir()['statik']['description'];

                }
            }
        } else {
            $seoData['status'] = false;
        }
        return $seoData;

    }


    public function load()
    {

        $anaSayfa['header'] = 'View/theme/header.php';
        $anaSayfa['footer'] = 'View/theme/footer.php';

        if (isset($_GET['Sayfa'])) {
            $seoKontrol = $this->seoKontrol($_GET["Sayfa"]);
            if ($seoKontrol['status']) {
                if (file_exists('View/pages/' . $seoKontrol['view'] . '.php')) {

                    $anaSayfa['main'] = 'View/pages/' . $seoKontrol['view'] . '.php';
                    $anaSayfa['title'] = $seoKontrol['title'];
                    $anaSayfa['description'] = $seoKontrol['description'];
                    $anaSayfa['link'] = $seoKontrol['link'];
                    $anaSayfa['anaSayfa'] = false;


                } else {
                    //$anaSayfa['main'] 	 = 'View/theme/404.php';
                    //404lerde nofollow title var
                    $anaSayfa['main'] = 'View/theme/main.php';
                    if (isset($_GET['Dil'])) {
                        $dilKod = str_replace("/", "", $_GET['Dil']);
                        $anaSayfa['title'] = $this->siteAyar->dilCevir($dilKod)['statik']['title'];
                        $anaSayfa['description'] = $this->siteAyar->dilCevir($dilKod)['statik']['description'];
                    } else {
                        $anaSayfa['title'] = $this->siteAyar->dilCevir()['statik']['title'];
                        $anaSayfa['description'] = $this->siteAyar->dilCevir()['statik']['description'];
                    }
                    $anaSayfa['anaSayfa'] = true;
                }
            } else {
                //$anaSayfa['main'] 	 = 'View/theme/404.php';
                $anaSayfa['main'] = 'View/theme/main.php';
                if (isset($_GET['Dil'])) {
                    $dilKod = str_replace("/", "", $_GET['Dil']);
                    $anaSayfa['title'] = $this->siteAyar->dilCevir($dilKod)['statik']['title'];
                    $anaSayfa['description'] = $this->siteAyar->dilCevir($dilKod)['statik']['description'];
                } else {
                    $anaSayfa['title'] = $this->siteAyar->dilCevir()['statik']['title'];
                    $anaSayfa['description'] = $this->siteAyar->dilCevir()['statik']['description'];
                }
                $anaSayfa['anaSayfa'] = true;

            }
        } else {
            $anaSayfa['main'] = 'View/theme/main.php';

            if (isset($_GET['Dil'])) {
                $dilKod = str_replace("/", "", $_GET['Dil']);
                $anaSayfa['title'] = $this->siteAyar->dilCevir($dilKod)['statik']['title'];
                $anaSayfa['description'] = $this->siteAyar->dilCevir($dilKod)['statik']['description'];
            } else {
                $anaSayfa['title'] = $this->siteAyar->dilCevir()['statik']['title'];
                $anaSayfa['description'] = $this->siteAyar->dilCevir()['statik']['description'];
            }

            $anaSayfa['anaSayfa'] = true;

        }
        return $anaSayfa;
    }


}