<?php 



class sliderController
{
	private $conn;
	
	function __construct($db)
	{
		$this->conn = $db;
		
	}

	public function slider(){
		$sliderData = [];
		$sorgu = mysqli_query($this->conn,"SELECT * FROM slider WHERE durum='1' ");
        while($row = mysqli_fetch_assoc($sorgu) ){
            $sliderData[$row['sliderID']] = $row;
            if (explode("/", $_GET['Dil'])[0]) {
                $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM slider_dil WHERE dil='" . explode("/", $_GET['Dil'])[0] . "' and sliderID='" . $row['sliderID'] . "'   "));
                if ($dilDB) {
                    $sliderData[$row['sliderID']]['baslik'] = $dilDB['baslik'];
                    $sliderData[$row['sliderID']]['altBaslik'] = $dilDB['altBaslik'];
                    $sliderData[$row['sliderID']]['aciklama'] = $dilDB['aciklama'];
                    $sliderData[$row['sliderID']]['resim']=$dilDB['resim'];
                    $sliderData[$row['sliderID']]['linkButon1'] = $dilDB['linkButon1'];
                    $sliderData[$row['sliderID']]['linkButon2'] = $dilDB['linkButon2'];
                }
            }
        }
		return $sliderData;
	}


}

