<?php
	require_once "model/hizmetModel.php";
	class hizmetController
	{
		private $conn;
		private $hizmetModel;

		function __construct($db)
		{
			$this->conn = $db;
			$this->hizmetModel = new hizmetModel($this->conn);
		}

		public function hizmetKategori()
		{
			$data = [];
			$hizmetListesi = $this->hizmetModel->hizmetKategori();
			$i = 1;
			foreach ($hizmetListesi as $hizmet){
				$data[$i]['tag'] = $hizmet['tag'];
				$data[$i]['baslik'] = $hizmet['baslik'];
				$data[$i]['urunler'] = $this->hizmetModel->hizmetListesi('true',$hizmet['hizmetID']);
				$i++;
			}
			return $data;
		}



	}