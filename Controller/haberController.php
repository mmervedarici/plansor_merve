<?php

class haberController
{
    private $conn;

    function __construct($db)
    {
        $this->conn = $db;

    }

    public function haberDetay($link)
    {
        //Standart haber Verileri
        $haberData = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * from haber WHERE link='" . $link . "'"));
        $katData   = mysqli_fetch_assoc(mysqli_query($this->conn,"SELECT link AS kategoriLink,baslik FROM haber_kategori WHERE hkategoriID='".$haberData['hkategoriID']."' "));
        $haberData['kategoriData']['baslik'] = $katData['baslik'];
        $haberData['kategoriData']['link'] = $katData['kategoriLink'];
        if(isset($_GET['Dil'])){
            $katDatadil   = mysqli_fetch_assoc(mysqli_query($this->conn,"SELECT link AS kategoriLink,baslik FROM haber_kategoridil WHERE dil='" . $_GET['Dil'] . "' and  link='".$katData['kategoriLink']."' "));

            if($katDatadil){
                $haberData['kategoriData']['baslik'] = $katDatadil['baslik'];
            }
        }

        //Diziye çevirilmiş haberData verisetine dil var ve veritabnında karşılığı var ise baslik ve icerik dil tablosundaki gibi değiştiriliyor.
        if (isset($_GET['Dil'])) {
            $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM haber_dil WHERE dil='" . $_GET['Dil'] . "' and link='" . $haberData['link'] . "'   "));
            if ($dilDB) {
                $haberData['baslik'] = $dilDB['baslik'];
                $haberData['icerik'] = $dilDB['icerik'];
            }
        }
        $haberData['benzerHaberler'] = $this->haberListesi($katData['kategoriLink'],5);
        $haberData['galeri'] = $this->galeriListesi($link);
        return $haberData;
    }

    public function haberListesi($filtre = false, $limit = false)
    {
        $haberList = [];
        if ($filtre) {
            if ($limit) {
                $limitQ = " LIMIT " . $limit;
            } else {
                $limitQ = "";
            }
            $kategoriDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT hkategoriID,baslik,link FROM haber_kategori WHERE link='" . $filtre . "' "));
            if (explode("/", $_GET['Dil'])[0]) {
                $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM haber_kategoridil WHERE dil='" . explode("/", $_GET['Dil'])[0] . "' and link='" . $kategoriDB['link'] . "'   "));
                if ($dilDB) {
                    $kategoriDB['baslik'] = $dilDB['baslik'];
                }
            }
            $sorgu = mysqli_query($this->conn, "SELECT * FROM haber WHERE durum='1' and hkategoriID='" . $kategoriDB['hkategoriID'] . "' " . $limitQ);
            while ($row = mysqli_fetch_assoc($sorgu)) {
                $haberList[$row['haberID']] = $row;
                $haberList[$row['haberID']]['kategoriBaslik'] = $kategoriDB['baslik'];
                if (explode("/", $_GET['Dil'])[0]) {
                    $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM haber_dil WHERE dil='" . explode("/", $_GET['Dil'])[0] . "' and link='" . $row['link'] . "'   "));
                    if ($dilDB) {
                        $haberList[$row['haberID']]['baslik'] = $dilDB['baslik'];
                        $haberList[$row['haberID']]['icerik'] = $dilDB['icerik'];
                    }
                }
            }
        } else {
            if ($limit) {
                $limitQ = " LIMIT " . $limit;
            } else {
                $limitQ = "";
            }
            $sorgu = mysqli_query($this->conn, "SELECT * FROM haber WHERE durum='1'" . $limitQ);
            while ($row = mysqli_fetch_assoc($sorgu)) {
                $haberList[$row['haberID']] = $row;
                if (explode("/", $_GET['Dil'])[0]) {
                    $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM haber_dil WHERE dil='" .explode("/", $_GET['Dil'])[0]. "' and link='" . $row['link'] . "'   "));
                    if ($dilDB) {
                        $haberList[$row['haberID']]['baslik'] = $dilDB['baslik'];
                        $haberList[$row['haberID']]['icerik'] = $dilDB['icerik'];
                    }
                }
            }
        }
        return $haberList;
    }

    public function kategoriListesi()
    {
        $kategoriList = [];
        $sorgu = mysqli_query($this->conn, "SELECT * FROM haber_kategori WHERE durum='1'");
        while ($row = mysqli_fetch_assoc($sorgu)) {
            $kategoriList[$row['hkategoriID']] = $row;
            if ($_GET['Dil']) {
                $dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM haber_kategoridil WHERE dil='" . $_GET['Dil'] . "' and link='" . $row['link'] . "'   "));
                if ($dilDB) {
                    $kategoriList[$row['hkategoriID']]['baslik'] = $dilDB['baslik'];
                }
            }
        }
        return $kategoriList;
    }

    public function galeriListesi($link){
        return mysqli_fetch_all(mysqli_query($this->conn,"SELECT * FROM galeri WHERE link='".$link."' "),MYSQLI_ASSOC);
    }

}

