const anaSlider = new Swiper('#anaSlider', {
    speed: 1000,
    effect: 'fade',
    loop: true,
    fadeEffect: {
        crossFade: true
    },
    autoplay: {
        delay: 8000,
    },
    navigation: {
        nextEl: ".anaSliderIleri",
        prevEl: ".anaSliderGeri",
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">0' + (index + 1) + "</span>";
        },
    },
    on: {
        init: function () {
            setTimeout(function () {
                anime.timeline({loop: false})
                .add({
                    targets: '.swiper-slide-active .harf',
                    opacity: [0, 1],
                    translateX: [100, 0],
                    rotate: ['15deg', '0deg'],
                    easing: "easeOutExpo",
                    delay: (el, i) => 40 * (i + 1),
                    "complete": function () {
                        $(".swiper-slide-active .harf").removeAttr("style")
                    }
                })
            }, 0)
        },
    }
});


$(window).on('load', () => {
    $('#preLoader').delay(100).fadeOut('100')
    siteAnimasyonlari()
    new WOW().init(); // Wow
    $(".hc-offcanvas-nav").find(".nav-item").removeClass("nav-item");
    $(".hc-offcanvas-nav").find(".d-none").removeClass("d-none");
    $(".hc-offcanvas-nav").find(".nav-link").removeClass("nav-link");
    $(".hc-offcanvas-nav").find(".navbar-nav").removeClass("navbar-nav");
    $(".hc-offcanvas-nav").find(".ml-auto").removeClass("ml-auto");
    $(".hc-offcanvas-nav").find(".dropdown").removeClass("dropdown");
    $(".hc-offcanvas-nav").find(".dropdown-item").removeClass("dropdown-item");
    $(".hc-offcanvas-nav").find(".dropdown-menu").removeClass("dropdown-menu");
    $(".hc-offcanvas-nav").find(".dropdown-toggle").removeClass("dropdown-toggle");
});


$(".scrollup").on('click', function (e) {
    e.preventDefault();
    $(window).scrollTop(0);
    return false;
});


$(window).scroll(function () {
    if ($(this).scrollTop() === 0) {
        siteAnimasyonlari()
    }

    if ($(this).scrollTop() > 150) {
        $('.scrollup').addClass('aktif');
    } else {
        $('.scrollup').removeClass('aktif');
    }

    // if ($(window).width() > 992) {
        if ($(this).scrollTop() > 1) {
            $('header').addClass('is-sticky');
            $('body').addClass('ustPadding')
        } else {
            $('header').removeClass('is-sticky');
            $('body').removeClass('ustPadding')
        }
    // }
});


var Nav = new hcOffcanvasNav('#navbarNavDropdown', {
    disableAt: 992,
    customToggle: false,
    levelSpacing: 40,
    navTitle: 'Plansor',
    levelTitles: true,
    levelTitleAsBack: true,
    pushContent: $('#mainDiv'),
    labelClose: false
});


function siteAnimasyonlari() {
    anime({
        "targets": "#navbarUst .basvuruBtn",
        "translateY": ["-500", "0"],
        "delay": anime.stagger(100, {start: 350}),
        "duration": 400,
        "easing": "spring(1, 50, 15, 0)",
        "complete": function () {
            $("#navbarUst .basvuruBtn").removeAttr("style")
        }
    });
    anime({
        "targets": "#navbarUst li",
        "translateY": ["-200", "0"],
        "delay": anime.stagger(100, {start: 250}),
        "duration": 200,
        "easing": "spring(1, 50, 15, 0)",
        "complete": function () {
            $("#navbarUst li").removeAttr("style")
        }
    });

    anime({
        "targets": ".navbar-brand",
        "translateY": ["-200", "0"],
        "delay": anime.stagger(100 * 2, {start: 250}),
        "duration": 400,
        "easing": "spring(1, 50, 10, 0)",
        "complete": function () {
            $(".navbar-brand").removeAttr("style")
        }
    })
    anime({
        "targets": "#navbarNavDropdown",
        "translateY": ["-200", "0"],
        "delay": anime.stagger(100 * 2, {start: 250}),
        "duration": 400,
        "easing": "spring(1, 50, 10, 0)",
        "complete": function () {
            $(".navbar-brand").removeAttr("style")
        }
    })
}

var swiper3 = new Swiper("#haberSwiper", {
    loop: true,
    spaceBetween: 20,
    slidesPerView: 2,
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    breakpoints: {
        480: {slidesPerView: 2, spaceBetween: 20},
        768: {slidesPerView: 2, spaceBetween: 20},
        992: {slidesPerView: 4, spaceBetween: 40}
    }

});
var swiper4 = new Swiper(".detayGaleri", {
    loop: true,
    spaceBetween: 20,
    slidesPerView: 2,
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    breakpoints: {
        480: {slidesPerView: 2, spaceBetween: 20},
        768: {slidesPerView: 2, spaceBetween: 20},
        992: {slidesPerView: 4, spaceBetween: 40}
    }

});
var swiper5 = new Swiper(".detayGaleri2", {
    loop: true,
    spaceBetween: 20,
    slidesPerView: 2,
    navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
    breakpoints: {
        480: {slidesPerView: 2, spaceBetween: 20},
        768: {slidesPerView: 2, spaceBetween: 20},
        992: {slidesPerView: 6, spaceBetween: 40}
    }

});

