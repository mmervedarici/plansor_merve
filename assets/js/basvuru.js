const imar = document.querySelector(".basvuru-hizmetler-mainButton-item.imar");
const tapu = document.querySelector(".basvuru-hizmetler-mainButton-item.tapu");
const diger = document.querySelector(".basvuru-hizmetler-mainButton-item.diger");
const imarOption = document.querySelector(".bavsuru-hizmetler-altButton-wrapper.imar ");
const tapuOption = document.querySelector(".bavsuru-hizmetler-altButton-wrapper.tapu ");
const digerOption = document.querySelector(".bavsuru-hizmetler-altButton-wrapper.diger ");
const basvuruAltHizmetler = document.querySelectorAll(".bavsuru-hizmetler-altButton-item")

imar.onclick = (e) => {
	e.preventDefault();
	digerOption.classList.remove("bavsuru-style");
	diger.classList.remove("basvuru-mainButton-focus");
	tapu.classList.remove("basvuru-mainButton-focus");
	tapuOption.classList.remove("bavsuru-style");
	imar.classList.toggle("basvuru-mainButton-focus");
	imarOption.classList.toggle("bavsuru-style");
}

diger.onclick = (e) => {
	e.preventDefault();
	imar.classList.remove("basvuru-mainButton-focus");
	imarOption.classList.remove("bavsuru-style");
	tapu.classList.remove("basvuru-mainButton-focus");
	tapuOption.classList.remove("bavsuru-style");
	diger.classList.toggle("basvuru-mainButton-focus");
	digerOption.classList.toggle("bavsuru-style");
}

tapu.onclick = (e) => {
	e.preventDefault();
	digerOption.classList.remove("bavsuru-style");
	diger.classList.remove("basvuru-mainButton-focus");
	imar.classList.remove("basvuru-mainButton-focus");
	imarOption.classList.remove("bavsuru-style");
	tapu.classList.toggle("basvuru-mainButton-focus");
	tapuOption.classList.toggle("bavsuru-style");
}


