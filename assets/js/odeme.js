const cardNumber = document.querySelector(".card-number");
const cardOwner = document.querySelector(".card-owner");
const cardSNTMonth = document.querySelector("#card-month");
const cardSNTYear = document.querySelector("#card-year");
const cardSntMonth = document.querySelector("#card-month");
const cardCVC = document.querySelector(".card-cvc");
const cardOnizlemeNumber = document.querySelector(".credit-card-onizleme-no");
const cardOnizlemeOwner = document.querySelector(".credit-card-onizleme-owner");
const cardOnizlemeSTNMonth = document.querySelector(".credit-card-onizleme-sntMonth");
const cardOnizlemeSTNYear = document.querySelector(".credit-card-onizleme-sntYear");
let carCvcCount = true
$(document).ready(function () {
	for (var index = new Date().getFullYear(); index <= new Date().getFullYear() + 10; index++) {
		$('#card-year').append('<option value ="' + index + '">' + index + '</option>')
	}
});

cardNumber.addEventListener("input", (e) => {
	$(".flip-box-inner").css("transform", "rotateY(0deg)");
	cardOnizlemeNumber.innerHTML = cardNumber.value;
	if (cardNumber.value.length === 0) {
		cardOnizlemeNumber.innerHTML = "**** **** **** ****"
	}
})
cardOwner.addEventListener("input", () => {
	$(".flip-box-inner").css("transform", "rotateY(0deg)")
	cardOnizlemeOwner.innerHTML = cardOwner.value;
	if (cardOwner.value.length == 0)
		cardOnizlemeOwner.innerHTML = "------- -------"
});

cardSntMonth.addEventListener("input", () => {
	$(".flip-box-inner").css("transform", "rotateY(0deg)")
	cardOnizlemeSTNMonth.innerHTML = cardSntMonth.value;
});

cardSNTYear.addEventListener("input", () => {
	$(".flip-box-inner").css("transform", "rotateY(0deg)")
	cardOnizlemeSTNYear.innerHTML = cardSNTYear.value.slice(2, 4)
})

cardCVC.addEventListener("input", () => {
	$(".flip-box-inner").css("transform", "rotateY(180deg)")
	$(".flip-box-back .cvc-text").text(cardCVC.value)
})

$(".cvc-number").click(function () {
	if (carCvcCount) {
		$(".cvc-aciklama").css("opacity", "1")
		carCvcCount = false
	}
	else {
		$(".cvc-aciklama").css("opacity", "0")
		carCvcCount = true
	}
});