<?php

	class siparisModel
	{
		private $conn;

		function __construct($db)
		{
			$this->conn = $db;
		}

		public function urunDetay($urunID)
		{
			$query = "SELECT  hizmet_detay.hizmet_detayID,  hizmet_detay.baslik AS urunBaslik, fiyat, dolar, euro, hizmet.baslik AS hizmetBaslik FROM hizmet_detay 
                  LEFT JOIN hizmet ON (hizmet_detay.hizmetID = hizmet.hizmetID)
                  WHERE hizmet_detay.hizmet_detayID = '" . $urunID . "' ";
			$urunData = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
			if (isset($_GET['Dil'])) {
				$query = "SELECT hizmet_detay_dil.baslik AS urunBaslik, hizmet_dil.baslik AS hizmetBaslik FROM hizmet_detay_dil 
            LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID = hizmet_detay_dil.hizmet_detayID)
            LEFT JOIN hizmet ON (hizmet_detay.hizmetID = hizmet.hizmetID)
            LEFT JOIN hizmet_dil ON (hizmet.hizmetID = hizmet_dil.hizmetID and hizmet_dil.dil = '" . $_GET['Dil'] . "')

            WHERE hizmet_detay_dil.dil='" . $_GET['Dil'] . "' and hizmet_detay_dil.hizmet_detayID='" . $urunID . "'   ";
				$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
				if ($dilDB) {
					$urunData['urunBaslik'] = $dilDB['urunBaslik'];
					$urunData['hizmetBaslik'] = $dilDB['hizmetBaslik'];
				}
			}


			return $urunData;

		}

		public function siparisKayit($kullaniciID, $urunID, $aciklama, $il, $ilce, $mahalle, $ada, $parsel, $digerAciklama)
		{

			$query = " INSERT INTO siparis (musteriID,urunID,aciklama,kayitTarih) VALUES ('" . $kullaniciID . "','" .
				$urunID . "','" . $aciklama . "','" . date('Y-m-d H:i') . "') ";
			mysqli_query($this->conn, $query);
			$siparisID = mysqli_insert_id($this->conn);
			//detayları kaydedelim.
			$query = "INSERT INTO siparis_detay (siparisID,il,ilce,mahalle,ada,parsel,aciklama,diger_aciklama,kayitTarih) VALUES 
        ('" . $siparisID . "' , '" . $il . "' , '" . $ilce . "' , '" . $mahalle . "' , '" . $ada . "' , '" . $parsel
				. "' ,  '" . $aciklama . "' ,'" . $digerAciklama . "' , '" . date('Y-m-d H:i') . "' )
        ";
			mysqli_query($this->conn, $query);
			$siparis_detayID = mysqli_insert_id($this->conn);
			$data['siparisID'] = $siparisID;
			$data['siparis_detayID'] = $siparis_detayID;
			return $data;
		}

		public function siparisOnay($siparisID)
		{

			return mysqli_query($this->conn, "UPDATE siparis SET siparis_durum=1 WHERE siparisID='" . $siparisID . "' ");
		}

		public function siparisDetay($siparisID)
		{
			return mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada,
        siparis_detay.parsel AS parsel, siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama, siparis_detay.durum AS durum, siparis_detay.kayitTarih as kayitTarih,
        siparis.urunID AS urunID , siparis.musteriID AS musteriID , siparis.bayiID AS bayiID
        FROM siparis_detay 
        LEFT JOIN siparis ON (siparis.siparisID = siparis_detay.siparisID)
        WHERE siparis_detay.siparisID='" . $siparisID . "' "));

		}


		public function siparisListesi($type, $kullaniciID)
		{
			$hucre = 'musteriID';
			if ($type == 2) {
				$hucre = 'bayiID';
			}

			$siparisListesi['onayBekleyenler'] = [];
			$siparisListesi['islemdekiler'] = [];
			$siparisListesi['tamamlandi'] = [];
			//0:onay bekliyor. 1: iş birlikçi onayı,2:işlemde,3:değerlendirmede,4:tamamlandı,5:firma red,6:müşteri iptal

			$onayBekleyenler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
        siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
        FROM siparis 
        LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
        LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
        LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
        WHERE siparis.musteriID='" . $kullaniciID . "' and siparis.durum='0' ";
			$islemdekiSiparisler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
        siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
        FROM siparis 
        LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
        LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
        LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
        WHERE siparis.musteriID='" . $kullaniciID . "' and siparis.durum in ('1','2','3') ";
			$tamamlananSiparisler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
        siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
        FROM siparis 
        LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
        LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
        LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
        WHERE siparis.musteriID='" . $kullaniciID . "' and siparis.durum in ('4','5','6') ";
			//BAYİ İÇİN
			if ($type == 2) {
				$onayBekleyenler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
            siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
            FROM siparis 
            LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
            LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
            LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
            WHERE siparis.bayiID='" . $kullaniciID . "' and siparis.durum='1' ";
				$islemdekiSiparisler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
            siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
            FROM siparis 
            LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
            LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
            LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
            WHERE siparis.bayiID='" . $kullaniciID . "' and siparis.durum in ('2','3') ";
				$tamamlananSiparisler = "SELECT siparis.durum AS durum, siparis.siparisID as siparisID, siparis_detay.il AS il, siparis_detay.ilce AS ilce, siparis_detay.mahalle AS mahalle, siparis_detay.ada AS ada, siparis_detay.parsel AS parsel,
            siparis_detay.aciklama AS aciklama, siparis_detay.diger_aciklama AS diger_aciklama ,  hizmet.baslik AS hizmetBaslik, hizmet_detay.baslik AS urunBaslik , siparis.kayitTarih AS kayitTarih ,
            hizmet.hizmetID AS hizmetID , hizmet_detay.hizmet_detayID AS hizmet_detayID
            FROM siparis 
            LEFT JOIN siparis_detay ON ( siparis_detay.siparisID = siparis.siparisID ) 
            LEFT JOIN hizmet_detay ON (hizmet_detay.hizmet_detayID=siparis.urunID )
            LEFT JOIN hizmet ON (hizmet.hizmetID = hizmet_detay.hizmetID)
            WHERE siparis.bayiID='" . $kullaniciID . "' and siparis.durum in ('4','5','6') ";
			}

			$sorgu = mysqli_query($this->conn, $onayBekleyenler);
			while ($row = mysqli_fetch_assoc($sorgu)) {
				$siparisListesi['onayBekleyenler'][$row['siparisID']] = $row;

				if (isset($_GET['Dil'])) {
					$query = "SELECT * from hizmet_dil WHERE dil='" . $_GET['Dil'] . "' and hizmetID='" . $row['hizmetID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['onayBekleyenler'][$row['siparisID']]['hizmetBaslik'] = $dilDB['baslik'];
					}
					$query = "SELECT * from hizmet_detay_dil WHERE dil='" . $_GET['Dil'] . "' and hizmet_detayID='" . $row['hizmet_detayID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['onayBekleyenler'][$row['siparisID']]['urunBaslik'] = $dilDB['baslik'];
					}
				}
			}


			$sorgu = mysqli_query($this->conn, $islemdekiSiparisler);
			while ($row = mysqli_fetch_assoc($sorgu)) {
				$siparisListesi['islemdekiler'][$row['siparisID']] = $row;

				if (isset($_GET['Dil'])) {
					$query = "SELECT * from hizmet_dil WHERE dil='" . $_GET['Dil'] . "' and hizmetID='" . $row['hizmetID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['islemdekiler'][$row['siparisID']]['hizmetBaslik'] = $dilDB['baslik'];
					}
					$query = "SELECT * from hizmet_detay_dil WHERE dil='" . $_GET['Dil'] . "' and hizmet_detayID='" . $row['hizmet_detayID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['islemdekiler'][$row['siparisID']]['urunBaslik'] = $dilDB['baslik'];
					}
				}
			}
			$sorgu = mysqli_query($this->conn, $tamamlananSiparisler);
			while ($row = mysqli_fetch_assoc($sorgu)) {
				$siparisListesi['tamamlandi'][$row['siparisID']] = $row;

				if (isset($_GET['Dil'])) {
					$query = "SELECT * from hizmet_dil WHERE dil='" . $_GET['Dil'] . "' and hizmetID='" . $row['hizmetID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['tamamlandi'][$row['siparisID']]['hizmetBaslik'] = $dilDB['baslik'];
					}
					$query = "SELECT * from hizmet_detay_dil WHERE dil='" . $_GET['Dil'] . "' and hizmet_detayID='" . $row['hizmet_detayID'] . "'   ";
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, $query));
					if ($dilDB) {
						$siparisListesi['tamamlandi'][$row['siparisID']]['urunBaslik'] = $dilDB['baslik'];
					}
				}
			}

			//Süreç listesini ekleyelim.
			$sorgu = mysqli_query($this->conn, "SELECT * FROM siparis_surec WHERE " . $hucre . "='" . $kullaniciID . "' ");
			while ($row = mysqli_fetch_assoc($sorgu)) {


				if (isset($siparisListesi['onayBekleyenler'][$row['siparisID']])) {

					$siparisListesi['onayBekleyenler'][$row['siparisID']]['surecler'] = $row;
				}
				if (isset($siparisListesi['islemdekiler'][$row['siparisID']])) {
					$siparisListesi['islemdekiler'][$row['siparisID']]['surecler'] = $row;
				}
				if (isset($siparisListesi['tamamlandi'][$row['siparisID']])) {
					$siparisListesi['tamamlandi'][$row['siparisID']]['surecler'] = $row;
				}
			}

			return $siparisListesi;
		}

		public function siparisList($siparisID)
		{
			return mysqli_fetch_all(mysqli_query($this->conn, "SELECT * FROM siparis_surec WHERE  siparisID='"
				. $siparisID . "' "), 1);
		}

		public function siparisGuncelle($siparisID, $durum)
		{
			return mysqli_query($this->conn, "UPDATE siparis SET durum=" . $durum . " WHERE siparisID='" . $siparisID . "' ");
		}

		public function siparisDuzenle($siparisID, $il, $ilce, $mahalle, $ada, $parsel, $aciklama)
		{
			return mysqli_query($this->conn, "UPDATE siparis_detay SET  il='" . $il . "' , ilce='" . $ilce . "', mahalle='" . $mahalle . "', ada='" . $ada . "', parsel='" . $parsel . "', aciklama='" . $aciklama . "' WHERE siparisID='" . $siparisID . "'  ");
		}

		public
		function dosyaYukle($siparisID, $kullaniciID, $dosya, $aciklama)
		{
			$query = " INSERT INTO dosyalar (siparisID,kullaniciID,dosya,aciklama,kayitTarih) VALUES ('" . $siparisID . "','" .
				$kullaniciID . "','" . $dosya . "','" . $aciklama . "','" . date('Y-m-d H:i') . "') ";
			return mysqli_query($this->conn, $query);
		}

		public
		function surecKayit($siparisID, $durum, $type, $kullaniciID)
		{
			if ($type == "musteri") {
				$query = " INSERT INTO siparis_surec (siparisID,musteriID,durum,kayitTarih) VALUES ('" . $siparisID . "','" .
					$kullaniciID . "','" . $durum . "','" . date('Y-m-d H:i') . "') ";
			} elseif ($type == "bayi") {
				$query = " INSERT INTO siparis_surec (siparisID,bayiID,durum,kayitTarih) VALUES ('" . $siparisID . "','" .
					$kullaniciID . "','" . $durum . "','" . date('Y-m-d H:i') . "') ";
			}
			return mysqli_query($this->conn, $query);
		}


	}