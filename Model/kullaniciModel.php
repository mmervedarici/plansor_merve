<?php

class kullaniciModel
{
    private $conn;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function kullaniciData($eposta)
    {

        return mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM kullanici WHERE eposta='" . $eposta . "' "));
    }


    public function uyeKayit($type, $eposta, $sifre, $adSoyad, $telefon, $vergiDairesi, $vergiNo, $firmaAdi, $il, $ilce,$ip)
    {
        if ($type == 2) {
            $query = " INSERT INTO kullanici  (seviye,eposta,sifre,adSoyad,telefon,vergiDairesi,vergiNo,firmaAdi,il,ilce,ip1,durum,olusturmaTarihi)
            VALUES 
            ('2' , '" . $eposta . "' ,'" . md5($sifre) . "' , '" . $adSoyad . "' , '" . $telefon . "' , '" .
$vergiDairesi . "' , '" . $vergiNo . "' ,  '" . $firmaAdi . "' , '" . $il . "' , '" . $ilce . "' , '" . $ip . "' ,
            '1' , '" . date('Y-m-d H:i') . "')";

            return mysqli_query($this->conn, $query);
        } elseif ($type == 1) {
            $query = " INSERT INTO kullanici  (seviye,eposta,sifre,adSoyad,telefon,il,ilce,ip1,durum,olusturmaTarihi)
            VALUES 
            ('1' , '" . $eposta . "' ,'" . md5($sifre) . "' , '" . $adSoyad . "' , '" . $telefon . "'  , '" . $il . "' , '" . $ilce . "' , '" . $ip . "',
            '1' , '" . date('Y-m-d H:i') . "')
            ";
            return mysqli_query($this->conn, $query);
        }

    }

    public function uyeGiris($eposta, $sifre)
    {
        return mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM kullanici WHERE eposta='" . $eposta . "' and sifre='" . $sifre . "'  "));
    }

    public function profilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce)
    {
        return mysqli_query($this->conn, "UPDATE kullanici SET adSoyad='" . $adSoyad . "' , telefon='" . $telefon . "', il='" . $il . "', ilce='" . $ilce . "' WHERE eposta='" . $eposta . "'  ");
    }

	public function bayiProfilGuncelle($eposta, $adSoyad, $telefon, $il, $ilce, $vergiDairesi, $vergiNo, $firmaAdi)
	{
		return mysqli_query($this->conn, "UPDATE kullanici SET adSoyad='" . $adSoyad . "' , telefon='" . $telefon . "', il='" . $il . "', ilce='" . $ilce . "', vergiDairesi='" . $vergiDairesi . "' , vergiNo='" . $vergiNo . "' , firmaAdi='" . $firmaAdi . "'  WHERE eposta='" . $eposta . "'  ");
	}

    public function sifreGuncelle($eposta,$sifre)
    {
        return mysqli_query($this->conn, "UPDATE kullanici SET sifre='" . md5($sifre) . "'  WHERE eposta='" . $eposta . "'  ");
    }

}