<?php

	class hizmetModel
	{
		private $conn;

		function __construct($db)
		{
			$this->conn = $db;
		}

		public function hizmetKategori()
		{
			$sorgu = mysqli_query($this->conn, "SELECT * FROM hizmet");
			while ($row = mysqli_fetch_assoc($sorgu)) {
				$hizmetData[$row['hizmetID']] = $row;
				if (isset($_GET['Dil'])) {
					$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM hizmet_dil WHERE dil='" . $_GET['Dil'] . "' 
                and hizmetID='" . $row['hizmetID'] . "'   "));
					if ($dilDB) {
						$hizmetData[$row['hizmetID']]['baslik'] = $dilDB['baslik'];
					}
				}
			}

			return $hizmetData;
		}

		public function hizmetListesi($filtre = false, $hizmetID = null)
		{
			if ($filtre) {
				$sorgu = mysqli_query($this->conn, "SELECT * FROM hizmet_detay WHERE hizmetID='" . $hizmetID . "' ");
				while ($row = mysqli_fetch_assoc($sorgu)) {
					$hizmetData[$row['hizmet_detayID']] = $row;
					if (isset($_GET['Dil'])) {
						$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM hizmet_detay_dil WHERE dil='" . $_GET['Dil'] . "' 
                and hizmet_detayID='" . $row['hizmet_detayID'] . "'   "));
						if ($dilDB) {
							$hizmetData[$row['hizmet_detayID']]['baslik'] = $dilDB['baslik'];
						}
					}
				}
				return $hizmetData;
			} else {
				$sorgu = mysqli_query($this->conn, "SELECT * FROM hizmet_detay");
				while ($row = mysqli_fetch_assoc($sorgu)) {
					$hizmetData[$row['hizmet_detayID']] = $row;
					if (isset($_GET['Dil'])) {
						$dilDB = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM hizmet_detay_dil WHERE dil='" . $_GET['Dil'] . "' 
                and hizmet_detayID='" . $row['hizmet_detayID'] . "'   "));
						if ($dilDB) {
							$hizmetData[$row['hizmet_detayID']]['baslik'] = $dilDB['baslik'];
						}
					}
				}
				return $hizmetData;
			}
		}

	}