<?php 
session_start(); 
ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Asia/Baghdad');


include_once ("Config/database.php");

$database  = new database();
$db 	   = $database->getConnection();


include_once ("Config/config.php");
include_once ("Core/utility.php");
$utility  = new utility();

$config	   = new config($db);
$siteBilgi = $config->siteBilgi();
$textCagir = $config->textCagir();
include_once ("Controller/themeController.php");

include_once ("Core/utility.php");

include_once ("Route/routing.php");
$theme = new themeController($db);


$temaConfig = $theme->load();


include_once($temaConfig['header']);
include_once($temaConfig['main']);
include_once($temaConfig['footer']);




mysqli_close($db);