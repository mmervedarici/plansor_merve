<?php
include_once("controller/iletisimController.php");
include_once("controller/kullaniciController.php");
include_once("controller/siparisController.php");
include_once("controller/uploadController.php");
$iletisimController = new iletisimController($db);
$kullaniciController = new kullaniciController($db);
$siparisController = new siparisController($db);
$uploadController = new uploadController();

$route = false;
$data = null;
if (isset($_POST['methodName'])) {
    $methodName = $utility->temizle($db, $_POST["methodName"]);
    $data = $_POST;
    $route = true;
}
if (isset($_GET['methodName'])) {
    $methodName = $utility->temizle($db, $_GET["methodName"]);
    $data = $_GET;
    $route = true;
}

if ($route) {

    switch ($methodName) {
        case "kullaniciSiparis":
            if(isset($_POST['islem'])){
                if ($_POST['islem'] == 'dosyaYukle'){
                    $upload = $uploadController->dosyaYukle($_FILES['dosya']);
                    if($upload['status'] == 200){
                        $siparisController->dosyaYukle($_POST['siparisID'],$_SESSION['hbt_kullaniciID'],$upload['dosyaAdi'],$_POST['aciklama']);
                    }
                }
            }
	        if(isset($_POST['islem'])){
		        if ($_POST['islem'] == 'ret'){

			        $siparisController->siparisGuncelle(true,false,$_SESSION['hbt_kullaniciID'],$_POST['siparisID'],6);
			        $siparisController->surecKayit($_POST['siparisID'],6,'kullanici',$_SESSION['hbt_kullaniciID']);
		        }
	        }
            break;
        case "bayiSiparis":
            if(isset($_POST['islem'])){
                if ($_POST['islem'] == 'onay'){
                    $siparisController->siparisGuncelle(false,true,$_SESSION['hbt_kullaniciID'],$_POST['siparisID'],2);
                    $siparisController->surecKayit($_POST['siparisID'],2,'bayi',$_SESSION['hbt_kullaniciID']);

                }
                if ($_POST['islem'] == 'ret'){

                    $siparisController->siparisGuncelle(false,true,$_SESSION['hbt_kullaniciID'],$_POST['siparisID'],5);
                    $siparisController->surecKayit($_POST['siparisID'],5,'bayi',$_SESSION['hbt_kullaniciID']);
                }
            }
	        if(isset($_POST['islem'])){
		        if ($_POST['islem'] == 'dosyaYukle'){
			        $upload = $uploadController->dosyaYukle($_FILES['dosya']);
			        if($upload['status'] == 200){
				        $siparisController->dosyaYukle($_POST['siparisID'],$_SESSION['hbt_kullaniciID'],$upload['dosyaAdi'],$_POST['aciklama']);
			        }
		        }
	        }
            break;
        case "cikisYap":
            $data = $kullaniciController->uyeCikis();
            break;
        case "profilGuncelle":
            if (isset($_SESSION['hbt_login'])) {
                if ($_SESSION['hbt_login']) {
                    $kullaniciController->profilGuncelle($_SESSION['hbt_user'], $_POST['adSoyad'], $_POST['telefon'], $_POST['il'], $_POST['ilce']);
                }
            }
            break;
	    case "bayiProfilGuncelle":
		    if (isset($_SESSION['hbt_login'])) {
			    if ($_SESSION['hbt_login']) {
				    $kullaniciController->bayiProfilGuncelle($_SESSION['hbt_user'], $_POST['adSoyad'], $_POST['telefon'],
					    $_POST['il'], $_POST['ilce'], $_POST['vergiDairesi'],$_POST['vergiNo'], $_POST['firmaAdi']);
			    }
		    }
		    break;
        case "sifreGuncelle":
            if (isset($_SESSION['hbt_login'])) {
                if ($_SESSION['hbt_login']) {
                    if (isset($_POST['sifre']) and isset($_POST['yeniSifre'])) {
                        $data = $kullaniciController->sifreGuncelle($_SESSION['hbt_user'], $_POST['sifre'], $_POST['yeniSifre']);
                    }
                }
            }
            break;
        case "siparis":
            if (isset($_POST['step'])) {
                //İlk Başvuru Sayfası
                if ($_POST['step'] == "baslat") {
                    $diger = null;
                    if ($_POST['urunID'] == 11) {
                        $diger = $_POST['mesaj'];
                    }
                    $rData['status'] = $siparisController->siparis('baslat', $_POST['urunID'], $diger);
                    if (isset($_SESSION['hbt_login'])) {
                        $rData['login'] = true;
                    } else {
                        $rData['login'] = false;
                    }

                }
                //İkinci Sayfa
                if ($_POST['step'] == "bilgiGiris") {
                    //Üyelik işlemleri
                    if (isset($_POST['islem'])) {
                        if ($_POST['islem'] == "uyeGiris") {
                            $kullaniciController->uyeGiris($_POST['eposta'], $_POST['sifre']);
                        }
                        if ($_POST['islem'] == "uyeKayit") {
                            $ip = "0";
                            $kullaniciController->uyeKayit('1', $_POST['eposta'], $_POST['sifre'], $_POST['adSoyad'], $_POST['telefon'],null, null, $_POST['il'], $_POST['ilce'], $ip);
                        }
                    }
                }
                //Odeme Ekranına Geçiş
                if ($_POST['step'] == "odemeEkrani") {
                    if (isset($_POST['islem'])) {
                        if ($_POST['islem'] == "onKayit") {
                            $digerAciklama = '';
                            if (isset($_SESSION['diger'])) {
                                $digerAciklama = $_SESSION['diger'];
                            }
                            $siparisController->siparisKayit($_SESSION['hbt_user'], $_SESSION['urun']['hizmet_detayID'], $_POST['aciklama'], $_POST['il'], $_POST['ilce'],
                                $_POST['mahalle'], $_POST['ada'], $_POST['parsel'], $digerAciklama);
                        }
                    }
                }
                if ($_POST['step'] == "tamamlandi") {
                    $siparisController->siparisOnay($_SESSION['siparis']['siparisID']);
                    $_SESSION['step'] = 'arsiv';
                }
            }
            break;

        case "iletisimFormu":
            $ip1 = $utility->GetIP1();
            $ip2 = $utility->GetIP2();
            $ip3 = $utility->GetIP3();
            $iletisimEkle = $iletisimController->iletisimEkle($utility->temizle($db, $_POST["adSoyad"]), $utility->temizle($db, $_POST["eposta"]), $utility->temizle($db, $_POST["telefon"]), $utility->temizle($db, $_POST["mesaj"]), $ip1, $ip2, $ip3);
            $iletisimController->smsGonder($utility->temizle($db, $_POST["adSoyad"]), $utility->temizle($db, $_POST["telefon"]), $utility->temizle($db, $_POST["mesaj"]));
            break;
        case "bayiKayit":
            $ip = "0";
            $data = $kullaniciController->uyeKayit('2', $_POST['eposta'], $_POST['sifre'], $_POST['adSoyad'], $_POST['telefon'],
                $_POST['vergiDairesi'], $_POST['vergiNo'], $_POST['firmaAdi'] ,$_POST['il'], $_POST['ilce'], $ip );
            if ($data['status'] == 200) {
                $kullaniciController->uyeGiris($_POST['eposta'], $_POST['sifre']);
                echo $utility->Yonlendir('bayi-basvuru-takip');
            } else {
                echo $data['errMsg'];
            }
            break;
        case "uyeKayit":
            $ip = "0";
            $data = $kullaniciController->uyeKayit('1', $_POST['eposta'], $_POST['sifre'], $_POST['adSoyad'], $_POST['telefon'],
                null, null,null, $_POST['il'], $_POST['ilce'], $ip);
            if ($data['status'] == 200) {
                $kullaniciController->uyeGiris($_POST['eposta'], $_POST['sifre']);
                echo $utility->Yonlendir('kullanici-basvuru-takip');
            } else {
                echo $data['errMsg'];
            }
            break;
        case "girisYap":
            $data = $kullaniciController->uyeGiris($_POST['eposta'], $_POST['sifre']);

            if ($data['status'] == 200) {
                if ($_SESSION['hbt_seviye'] == 1) {
                    echo $utility->Yonlendir('kullanici-basvuru-takip');
                } elseif ($_SESSION['hbt_seviye'] == 2) {
                    echo $utility->Yonlendir('bayi-basvuru-takip');
                }
            }

            break;

	    case "siparisDuzenle":
		    if (isset($_SESSION['hbt_login'])) {
			    if ($_SESSION['hbt_login']) {

				    $siparisController->siparisDuzenle( $_POST['siparisID'], $_POST['il'], $_POST['ilce'],$_POST['mahalle'], $_POST['ada'], $_POST['parsel'], $_POST['aciklama']);
				  
			    }
		    }

        default:
            $kullaniciData = [];
            $kullaniciData["status"] = 404;
            $kullaniciData["data"] = "servis methodu bulunamadı ";
            break;
    }

}
$hesapData = $kullaniciController->hesapData();