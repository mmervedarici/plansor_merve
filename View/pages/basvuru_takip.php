<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $textCagir['form']['bayi-basvuru-takip']; ?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <ul>
                            <li><a title="Anasayfa" href="./"><i class="fas fa-home"></i></a></li>
                            <li><a title="Haberler" href="./"><?php echo $textCagir['form']['basvuru-takip']; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mx-auto" style="position: relative;max-width: 1140px;">
    <div class="basvuru-takip-header row">
        <div class="col-3">
            <a href="" class="baslik py-3 active">
                <img src="../assets/img/talep-anasayfa.svg" alt="">
                <span class="ml-4"><?php echo $textCagir['icSayfa']['talep-anaSayfa']; ?></span>
            </a>
        </div>
        <div class="col-3">
            <a href="" class="baslik py-3">
                <img src="../assets/img/peopleicon.svg">
                <span class="ml-4"><?php echo $textCagir['menu']['profil-bilgileri']; ?></span>
            </a>
        </div>
        <div class="col-3">
            <a href="" class="baslik py-3 ">
                <img src="../assets/img/kilit.svg">
                <span class="ml-4"><?php echo $textCagir['menu']['sifre-degistir']; ?></span>
            </a>
        </div>
        <div class="col-3">
            <a href="" class="baslik py-3">
                <img src="../assets/img/cıkıs.svg">
                <span class="ml-4"><?php echo $textCagir['menu']['cikis-yap']; ?></span>
            </a>
        </div>
    </div>
    <div class="pt-5 basvuru-takip-tablo w-100 position-relative" style="background-color: rgba(255, 255, 255, .5);">
        <div class="d-flex w-100 position-relative">
            <div class=" position-absolute" style="left: -140px;">
                <div class="talep-durum active px-2 py-2"><?php echo $textCagir['form']['onay-bekleyen']; ?></div>
                <div class="talep-durum px-2 py-2"><?php echo $textCagir['form']['islemdekiler']; ?></div>
                <div class="talep-durum px-2 py-2"><?php echo $textCagir['form']['tamamlandi']; ?></div>
            </div>
            <div class="d-flex align-items-center w-100">
                <div class="basvuru-takip-tablo-header-item">
                    <img src="../assets/img/peopleicon.svg" alt="">
                    <span><?php echo $textCagir['menu']['kullaniciID']; ?></span>
                </div>
                <div class="basvuru-takip-tablo-header-item">
                    <img src="../assets/img/peopleicon.svg" alt="">
                    <span><?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
                </div>
                <div class="basvuru-takip-tablo-header-item">
                    <img src="../assets/img/peopleicon.svg" alt="">
                    <span><?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
                </div>
                <div class="basvuru-takip-tablo-header-item">
                    <img src="../assets/img/peopleicon.svg" alt="">
                    <span><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
                </div>
                <div class="basvuru-takip-tablo-header-item">
                    <img src="../assets/img/peopleicon.svg" alt="">
                    <span><?php echo $textCagir['menu']['islemler']; ?></span>
                </div>
            </div>
        </div>
    </div>
</section>