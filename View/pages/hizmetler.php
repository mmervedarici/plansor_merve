
<section class="hizmetBg">
	<section>
		<div class="container my-5 pt-lg-4">
			<div class="row">
				<div class="col-12">
					<div class="icCircle1"></div>
					<div class="icCircle2"></div>
				</div>
				<div class="col-12 col-lg-6">
				<div class="breadBaslik">
						<h1><?php echo $textCagir['menu']['hizmetler']; ?></h1>
					</div>
				</div>
				<div class="col-lg-6"></div>
				<div class="col-lg-6"></div>
				<div class="col-12 col-lg-6">
					<div class="breadCrumbLinkDis">
						<div class="breadCrumbLink">
							<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
							<li><a title="hizmetler" href="hizmetler"><?php echo $textCagir['menu']['hizmetler']; ?></a></li>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 ortalaX">
                <div class="plansorBaslik">
                    <h4><?php echo $textCagir['menu']['hizmetler']; ?></h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="hizmetUcgen1"></div>
            </div>
            <div class="col-6 col-lg-4">
               <div class="row">
                <div class="col-12">
                    <div class="plansorBaslik">
                        <h4><?php echo $textCagir['menu']['imar']; ?></h4>
                    </div>
                </div>
                <div class="col-12 hizmetBtnDis my-3">
                    <a href="imar-durum-bilgisi">
                        <div class="row no-gutters hizmetBtnIc">
                            <div class="col-2">
                                <div class="hizmetNumber">01</div>
                            </div>
                            <div class="col-10">
                                <div class="hizmetText">
	                                <?php echo $textCagir['menu']['imar-durum-bilgisi']; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis my-3">
                    <a href="parsele-kac-daire-sigar">
                        <div class="row no-gutters hizmetBtnIc">
                            <div class="col-2">
                                <div class="hizmetNumber">02</div>
                            </div>
                            <div class="col-10">
                                <div class="hizmetText">
	                                <?php echo $textCagir['menu']['parsele-kac-daire-sigar']; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis my-3">
                    <a href="kat-irtifaki-kurulmasi">
                        <div class="row no-gutters hizmetBtnIc">
                            <div class="col-2">
                                <div class="hizmetNumber">03</div>
                            </div>
                            <div class="col-10">
                                <div class="hizmetText">
	                                <?php echo $textCagir['menu']['kat-irtifaki-kurulmasi']; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis my-3">
                    <a href="proje-yatirim-yonlendirme">
                        <div class="row no-gutters hizmetBtnIc">
                            <div class="col-2">
                                <div class="hizmetNumber">04</div>
                            </div>
                            <div class="col-10">
                                <div class="hizmetText">
	                                <?php echo $textCagir['menu']['proje-yatirim-yonlendirme']; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
        </div>


        <div class="col-6  col-lg-4 hizmetImg">
            <img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>assets/img/katmanlar/hizmetler.png" alt="plansor">
        </div>


        <div class="col-6 col-lg-4">
            <div class="row">
                <div class="col-12">
                    <div class="plansorBaslik">
                        <h4><?php echo $textCagir['menu']['tapu']; ?></h4>
                    </div>
                </div>
                <div class="col-12 hizmetBtnDis sag my-3">
                    <a href="mulkiyet-analizi">
                        <div class="row no-gutters hizmetBtnIc">

                            <div class="col-10">
                                <div class="hizmetText justify-content-end sag">
	                                <?php echo $textCagir['menu']['mulkiyet-analizi']; ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="hizmetNumber sag">01</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis sag my-3">
                    <a href="hisse-cozumleme">
                        <div class="row no-gutters hizmetBtnIc">

                            <div class="col-10">
                                <div class="hizmetText justify-content-end sag">
	                                <?php echo $textCagir['menu']['hisse-cozumleme']; ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="hizmetNumber sag">02</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis sag my-3">
                    <a href="emsal-deger-belirleme">
                        <div class="row no-gutters hizmetBtnIc">

                            <div class="col-10">
                                <div class="hizmetText justify-content-end sag">
	                                <?php echo $textCagir['menu']['emsal-deger-belirleme']; ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="hizmetNumber sag">03</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 hizmetBtnDis sag my-3">
                    <a href="intikal-islemleri">
                        <div class="row no-gutters hizmetBtnIc">

                            <div class="col-10">
                                <div class="hizmetText sag justify-content-end">
	                                <?php echo $textCagir['menu']['intikal-islemleri']; ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="hizmetNumber sag">04</div>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
        </div>
    </div>
</div>
</section>