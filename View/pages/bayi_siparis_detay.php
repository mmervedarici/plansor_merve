<?php
include_once("controller/siparisController.php");
if ($_SESSION['hbt_login']) {
    if (isset($_POST['siparisID'])) {

        $siparisData = $siparisController->siparisDetay($_POST['siparisID']);
        $siparisModel = new siparisModel($db);
        $urunData = $siparisModel->urunDetay($siparisData['urunID']);

        if ($siparisData['bayiID'] != $_SESSION['hbt_kullaniciID']) {
            echo $utility->yonlendir('./');
        }
    } else {
        echo $utility->yonlendir('./');
    }
} else {
    echo $utility->yonlendir('./');
}
?>
<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $textCagir['icSayfa']['siparis-detay']; ?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
                        <li><a title="<?php echo $textCagir['menu']['kullanici']; ?>" href="kullanici-basvuru-takip"><?php echo $textCagir['menu']['kullanici']; ?></a></li>
                        <li><?php echo $textCagir['icSayfa']['siparis-detay']; ?></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mx-auto" style="position:relative; max-width:1140px;">


    <div class="siparis-ozet col-md-12 ">
        <div class="anabilgiler mt-md-3">
            <div class="bilgiler  col-md-12">
                <div class="bilgibasliklari"><?php echo $textCagir['form']['kisisel-bilgi']; ?></div>
                <div class="sekil d-flex align-items-center justify-content-center ">
                    <div class="sekilcizgi"></div>
                    <div class="sekilyuvarlak"></div>
                    <div class="sekilcizgi"></div>
                </div>
                <div class="text-center genelbilgi ">
                    <div class="basliklar col-md-12 d-flex">
                        <div><?php echo $textCagir['form']['ad']; ?><?php echo $textCagir['form']['soyad']; ?></div>
                        <div><?php echo $textCagir['form']['telefon']; ?></div>
                        <div><?php echo $textCagir['form']['eposta']; ?></div>
                        <div><?php echo $textCagir['form']['aciklama']; ?></div>
                    </div>
                    <div class="baslikbilgi col-md-12 d-flex">
                        <div> <?php echo $_SESSION['hbt_adSoyad']; ?> </div>
                        <div> <?php echo $_SESSION['hbt_telefon']; ?> </div>
                        <div><?php echo $_SESSION['hbt_user']; ?></div>
                        <div><?php echo $siparisData['aciklama']; ?></div>
                    </div>
                </div>
            </div>
            <div class="bilgiler col-md-12">
                <div class="bilgibasliklari"><?php echo $textCagir['icSayfa']['parsel-bilgileri']; ?></div>
                <div class="sekil d-flex align-items-center justify-content-center">
                    <div class="sekilcizgi"></div>
                    <div class="sekilyuvarlak"></div>
                    <div class="sekilcizgi"></div>
                </div>
                <div class="text-center genelbilgi">
                    <div class="basliklar col-md-12 d-flex">
                        <div><?php echo $textCagir['form']['il']; ?></div>
                        <div><?php echo $textCagir['form']['ilce']; ?></div>
                        <div><?php echo $textCagir['form']['mahalle']; ?></div>
                        <div><?php echo $textCagir['form']['ada']; ?></div>
                        <div><?php echo $textCagir['form']['parsel']; ?></div>


                    </div>
                    <div class="baslikbilgi col-md-12 d-flex">
                        <div><?php echo $siparisData['il']; ?></div>
                        <div><?php echo $siparisData['ilce']; ?></div>
                        <div><?php echo $siparisData['mahalle']; ?></div>
                        <div><?php echo $siparisData['ada']; ?></div>
                        <div><?php echo $siparisData['parsel']; ?></div>

                    </div>
                </div>
            </div>
            <div class="bilgiler col-md-12">
                <div class="bilgibasliklari"><?php echo $textCagir['icSayfa']['hizmet-bilgileri']; ?></div>
                <div class="sekil d-flex align-items-center justify-content-center">
                    <div class="sekilcizgi"></div>
                    <div class="sekilyuvarlak"></div>
                    <div class="sekilcizgi"></div>
                </div>
                <div class="text-center genelbilgi ">
                    <div class="basliklar col-md-12 d-flex">
                        <div><?php echo $textCagir['icSayfa']['talep-tarih']; ?></div>
                        <div><?php echo $textCagir['icSayfa']['talep-saat']; ?></div>
                        <div><?php echo $textCagir['form']['hizmet']; ?></div>
                        <div><?php echo $textCagir['icSayfa']['hizmet-detay']; ?></div>
	                    <div><?php echo $textCagir['form']['siparis-no']; ?></div>
                    </div>
                    <div class="baslikbilgi col-md-12 d-flex">
                        <div><?php echo $utility->tarihYazdir(substr($siparisData['kayitTarih'], 0, 10));?></div>
                        <div><?php echo $utility->saatYazdir(substr($siparisData['kayitTarih'], 11, 5)); ?></div>
                        <div><?php echo $urunData['hizmetBaslik']; ?></div>
                        <div><?php echo $urunData['urunBaslik']; ?></div>
	                    <div><?php echo $_POST["siparisID"];?></div>

                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="d-md-flex mt-md-1 justify-content-center ">
        <button class="basvuru-back-button w-sm-100 mr-md-3 talep-buton"><a
                    href="./"><?php echo $textCagir['menu']['anaSayfa-don']; ?></a>
        </button>
        <button class="basvuru-back-button w-sm-100 ml-md-3 talep-buton"><a href="bayi-basvuru-takip"><?php echo
                $textCagir['menu']['profile-don']; ?></a>
        </button>
    </div>

</section>