<?php
	if (isset($_SESSION['step'])) {
		if(isset($_SESSION['hbt_login'])){
			echo $utility->yonlendir('parsel-bilgilerini-gir');
		}

	} else {
		echo $utility->yonlendir('basvuru-yap');
	}

?>
<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="basvuru-yap"><i class="fas fa-home"></i></a>
						<li><a title="Haberler"
						       href="basvuru-yap"><?php echo $textCagir['icSayfa']['basvuru-sayfasi']; ?></a></li>
						<li><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mx-auto " style="position: relative;max-width: 1140px;">
	<div class="ic-kisim ">
		<div class="ic-kisim-header ">

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper ">
						<i class="far fa-cogs"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['hizmetini-sec']; ?></div>
			</div>

			<div class="basvuru-header basvuru-header-active basvuru-phone-header-active">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-file-invoice"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-credit-card"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['odeme-yap']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-thumbs-up"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['talep-gor']; ?></div>
			</div>

		</div>
	</div>


	<section class="bilgilerini-gir d-flex flex-column justify-content-between ">
		<div class="d-flex  justify-content-between">
			<div class="girisyap-header phone-active col-6 col-md-5">
				<div class="d-flex  justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.859" height="17.12"
					     viewBox="0 0 17.859 17.12">
						<g id="Group_1571" data-name="Group 1571" transform="translate(-4877.51 -580.466)">
							<path id="Path_4277" data-name="Path 4277"
							      d="M4889.173,588.861a4.646,4.646,0,1,0-5.467,0,8.979,8.979,0,0,0-6.194,7.852.812.812,0,0,0,.216.611.822.822,0,0,0,.6.262h16.219a.818.818,0,0,0,.818-.872A8.979,8.979,0,0,0,4889.173,588.861Zm5.666,8.173a.4.4,0,0,1-.29.126H4878.33a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.29,8.555,8.555,0,0,1,6.235-7.581,4.619,4.619,0,0,0,4.535,0,8.553,8.553,0,0,1,6.234,7.582A.377.377,0,0,1,4894.839,597.034Zm-4.18-11.922a4.217,4.217,0,0,1-2.022,3.6c-.086.053-.173.1-.263.151s-.2.1-.309.147a4.216,4.216,0,0,1-3.251,0c-.106-.045-.208-.095-.309-.147s-.177-.1-.263-.151a4.217,4.217,0,1,1,6.417-3.6Z"
							      fill="rgb(202, 200, 200)"/>
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['giris-yap']; ?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
			<div class="uyeol-header col-md-6 col-6 ">
				<div class="d-flex justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.858" height="19.829"
					     viewBox="0 0 17.858 19.829">
						<g id="Group_1577" data-name="Group 1577" transform="translate(-5506.911 -579.43)">
							<path id="Path_4278" data-name="Path 4278"
							      d="M5518.573,587.824a4.646,4.646,0,1,0-5.467,0,8.978,8.978,0,0,0-6.193,7.853.81.81,0,0,0,.215.61.823.823,0,0,0,.6.262h16.22a.824.824,0,0,0,.6-.262.812.812,0,0,0,.215-.61A8.981,8.981,0,0,0,5518.573,587.824ZM5524.24,596a.4.4,0,0,1-.29.126h-16.22a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.289,8.555,8.555,0,0,1,6.235-7.582,4.62,4.62,0,0,0,4.536,0,8.555,8.555,0,0,1,6.234,7.582A.382.382,0,0,1,5524.24,596Zm-4.18-11.921a4.218,4.218,0,0,1-2.022,3.6c-.087.053-.174.1-.264.151s-.2.1-.308.147a4.218,4.218,0,0,1-3.252,0c-.106-.044-.208-.095-.308-.147s-.178-.1-.264-.151a4.217,4.217,0,1,1,6.418-3.6Z"
							      fill="rgb(202, 200, 200)"/>
							<g id="Group_1576" data-name="Group 1576">
								<path id="Path_4279" data-name="Path 4279"
								      d="M5518.634,596.024a.892.892,0,0,0-.072-.252.505.505,0,0,0-.172-.206.433.433,0,0,0-.245-.076H5516.7v-1.576a.475.475,0,0,0-.067-.248.485.485,0,0,0-.222-.193.925.925,0,0,0-.254-.07,2.612,2.612,0,0,0-.637,0,.935.935,0,0,0-.254.07.511.511,0,0,0-.218.182.471.471,0,0,0-.077.259v1.576h-1.438a.438.438,0,0,0-.255.081.507.507,0,0,0-.162.2.909.909,0,0,0-.071.252,2.037,2.037,0,0,0-.02.3,1.776,1.776,0,0,0,.024.311.889.889,0,0,0,.08.25.526.526,0,0,0,.17.2.442.442,0,0,0,.251.076h1.421v1.582a.446.446,0,0,0,.087.267.523.523,0,0,0,.208.163.991.991,0,0,0,.254.07,2.692,2.692,0,0,0,.638,0,1,1,0,0,0,.253-.07.5.5,0,0,0,.213-.174.45.45,0,0,0,.076-.256v-1.582h1.427a.464.464,0,0,0,.247-.071.507.507,0,0,0,.179-.2.811.811,0,0,0,.08-.257,2.288,2.288,0,0,0,0-.6Zm-2.509.558v2.078l-.052.01a2.135,2.135,0,0,1-.466,0l-.058-.01v-2.078h-1.921c0-.013-.007-.03-.011-.051a1.154,1.154,0,0,1-.015-.207,1.405,1.405,0,0,1,.013-.213q0-.027.009-.045h1.925v-2.082a.557.557,0,0,1,.058-.011,2.02,2.02,0,0,1,.466,0c.021,0,.038.007.052.01v2.082l1.933.01.006.036a1.405,1.405,0,0,1,.013.213,1.368,1.368,0,0,1-.013.214c0,.018,0,.032-.008.044Z"
								      fill="#008a83"/>
								<g id="Group_1575" data-name="Group 1575">
									<g id="Group_1574" data-name="Group 1574">
										<path id="Path_4280" data-name="Path 4280"
										      d="M5513.535,595.887a.088.088,0,0,0-.06.057.509.509,0,0,0-.037.14,1.676,1.676,0,0,0-.015.24,1.466,1.466,0,0,0,.017.24.512.512,0,0,0,.042.137.146.146,0,0,0,.038.051.058.058,0,0,0,.031.009h1.818v1.979a.047.047,0,0,0,.01.032.133.133,0,0,0,.054.037.537.537,0,0,0,.147.038,2.25,2.25,0,0,0,.519,0,.551.551,0,0,0,.148-.038.113.113,0,0,0,.048-.033.062.062,0,0,0,.01-.036v-1.979h1.823a.066.066,0,0,0,.037-.011.118.118,0,0,0,.038-.049.5.5,0,0,0,.039-.135,1.653,1.653,0,0,0,.015-.242,1.538,1.538,0,0,0-.016-.24.527.527,0,0,0-.037-.14.121.121,0,0,0-.034-.048l-1.865-.009v-1.973a.09.09,0,0,0-.013-.047.106.106,0,0,0-.045-.033.551.551,0,0,0-.148-.038,2.146,2.146,0,0,0-.519,0,.537.537,0,0,0-.147.038.123.123,0,0,0-.051.037.076.076,0,0,0-.013.043v1.973Z"
										      fill="#e9f6f5"/>
									</g>
								</g>
							</g>
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['uye-ol']; ?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
		</div>
		<div class="d-flex col-12 justify-content-between">
			<div class="basvuru-girisyap col-12  col-md-5 ">
				<div class="girisyap">
					<form class="girisyap-form col-12" method="post">
						<div class="girisYap-formInput d-flex flex-row-reverse justify-content-between">
							<input name="eposta" class="col-11" type="email" placeholder="E-Posta">
							<input name="eposta" class="col-11" type="email" placeholder="<?php echo
								$textCagir['form']['eposta'] ;?>">
							<label class="dot"></label>
						</div>
						<div class="girisYap-formInput d-flex flex-row-reverse justify-content-between">
							<input name="sifre" class="col-11 sifre" id="passwd1" type="password"
							       placeholder="Şifre">
							       placeholder="<?php echo $textCagir['form']['sifre'] ;?>">
							<label class="dot"></label>
							<i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd1',this)"></i>
						</div>
						<button type="submit" class="col-11 ml-auto d-block"><?php echo
							$textCagir['menu']['giris-yap']; ?></button>
						<div class="d-flex align-items-center mt-3 ml-4 col-11" style="gap: 6px;">
							<input type="hidden" name="methodName" value="siparis">
							<input type="hidden" name="step" value="bilgiGiris">
							<input type="hidden" name="islem" value="uyeGiris">
							<input class="checkbox-düzen" type="checkbox" id="benihatırlaCb">
							<label for="benihatırlaCb"
							       class="benihatırla mb-0"><?php echo $textCagir['menu']['beni-hatirla']; ?></label>

						</div>
					</form>
				</div>
			</div>
			<div class="basvuru-uyeol col-12  phone-form-display col-md-6 pr-0">
				<div class="uyeol">

					<form class="uyeol-form phone-uyeol col-12 " method="post">
						<div class="uyeOl-formInput d-flex flex-row-reverse justify-content-between">
							<input name="adSoyad" class="col-11" type="text"
							       placeholder="<?php echo $textCagir['form']['ad']; ?> <?php echo $textCagir['form']['soyad']; ?>">
							<label class="dot"></label>
						</div>

						<div class="uyeOl-formInput d-flex flex-row-reverse justify-content-between">
							<input name="telefon" class="col-11" type="number"
							       placeholder="<?php echo $textCagir['form']['telefon']; ?>">
							<label class="dot"></label>
						</div>
						<div class="uyeOl-formInput d-flex flex-row-reverse justify-content-between">
							<input name="eposta" class="col-11" type="email"
							       placeholder="<?php echo $textCagir['form']['eposta']; ?>">
							<label class="dot"></label>
						</div>

						<div class="uyeOl-formInput d-flex flex-row-reverse justify-content-between">
							<input name="sifre" class="col-11" id="passwd" type="password"
							       placeholder="<?php echo $textCagir['menu']['sifre']; ?>">
							<label class="dot"></label>
							<i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd',this)"></i>
						</div>
						<input type="hidden" name="methodName" value="siparis">
						<input type="hidden" name="step" value="bilgiGiris">
						<input type="hidden" name="islem" value="uyeKayit">


						<button type="submit" class="col-11 ml-auto d-block"><?php echo $textCagir['menu']['uye-ol']; ?>
						</button>
						<div class="d-flex align-items-center mt-3 ml-4 col-11" style="gap: 6px;">
							<input class="checkbox-düzen" type="checkbox" id="benihatırlaCb">
							<label for="benihatırlaCb"
							       class="benihatırla mb-0"><?php echo $textCagir['menu']['beni-hatirla']; ?></label>

						</div>


					</form>

				</div>
			</div>
		</div>
	</section>

</section>














