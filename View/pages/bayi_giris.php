<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['form']['bayi-giris'] ;?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>

						<li><?php echo $textCagir['form']['bayi-giris'] ;?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bayi px-md-5 mx-lg-auto " style="max-width: 1240px;">
	<div class=" d-flex justify-content-between ">
		<div class="d-flex col-12 justify-content-between">
			<div class="girisyap-header  phone-active col-6 col-md-5">
				<div class="d-flex  justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.859" height="17.12"
					     viewBox="0 0 17.859 17.12">
						<g id="Group_1571" data-name="Group 1571" transform="translate(-4877.51 -580.466)">
							<path id="Path_4277" data-name="Path 4277"
							      d="M4889.173,588.861a4.646,4.646,0,1,0-5.467,0,8.979,8.979,0,0,0-6.194,7.852.812.812,0,0,0,.216.611.822.822,0,0,0,.6.262h16.219a.818.818,0,0,0,.818-.872A8.979,8.979,0,0,0,4889.173,588.861Zm5.666,8.173a.4.4,0,0,1-.29.126H4878.33a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.29,8.555,8.555,0,0,1,6.235-7.581,4.619,4.619,0,0,0,4.535,0,8.553,8.553,0,0,1,6.234,7.582A.377.377,0,0,1,4894.839,597.034Zm-4.18-11.922a4.217,4.217,0,0,1-2.022,3.6c-.086.053-.173.1-.263.151s-.2.1-.309.147a4.216,4.216,0,0,1-3.251,0c-.106-.045-.208-.095-.309-.147s-.177-.1-.263-.151a4.217,4.217,0,1,1,6.417-3.6Z"
							      fill="rgb(202, 200, 200)" />
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['giris-yap']; ?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
			<div class="uyeol-header col-md-5 col-6 ">
				<div class="d-flex justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.858" height="19.829"
					     viewBox="0 0 17.858 19.829">
						<g id="Group_1577" data-name="Group 1577" transform="translate(-5506.911 -579.43)">
							<path id="Path_4278" data-name="Path 4278"
							      d="M5518.573,587.824a4.646,4.646,0,1,0-5.467,0,8.978,8.978,0,0,0-6.193,7.853.81.81,0,0,0,.215.61.823.823,0,0,0,.6.262h16.22a.824.824,0,0,0,.6-.262.812.812,0,0,0,.215-.61A8.981,8.981,0,0,0,5518.573,587.824ZM5524.24,596a.4.4,0,0,1-.29.126h-16.22a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.289,8.555,8.555,0,0,1,6.235-7.582,4.62,4.62,0,0,0,4.536,0,8.555,8.555,0,0,1,6.234,7.582A.382.382,0,0,1,5524.24,596Zm-4.18-11.921a4.218,4.218,0,0,1-2.022,3.6c-.087.053-.174.1-.264.151s-.2.1-.308.147a4.218,4.218,0,0,1-3.252,0c-.106-.044-.208-.095-.308-.147s-.178-.1-.264-.151a4.217,4.217,0,1,1,6.418-3.6Z"
							      fill="rgb(202, 200, 200)" />
							<g id="Group_1576" data-name="Group 1576">
								<path id="Path_4279" data-name="Path 4279"
								      d="M5518.634,596.024a.892.892,0,0,0-.072-.252.505.505,0,0,0-.172-.206.433.433,0,0,0-.245-.076H5516.7v-1.576a.475.475,0,0,0-.067-.248.485.485,0,0,0-.222-.193.925.925,0,0,0-.254-.07,2.612,2.612,0,0,0-.637,0,.935.935,0,0,0-.254.07.511.511,0,0,0-.218.182.471.471,0,0,0-.077.259v1.576h-1.438a.438.438,0,0,0-.255.081.507.507,0,0,0-.162.2.909.909,0,0,0-.071.252,2.037,2.037,0,0,0-.02.3,1.776,1.776,0,0,0,.024.311.889.889,0,0,0,.08.25.526.526,0,0,0,.17.2.442.442,0,0,0,.251.076h1.421v1.582a.446.446,0,0,0,.087.267.523.523,0,0,0,.208.163.991.991,0,0,0,.254.07,2.692,2.692,0,0,0,.638,0,1,1,0,0,0,.253-.07.5.5,0,0,0,.213-.174.45.45,0,0,0,.076-.256v-1.582h1.427a.464.464,0,0,0,.247-.071.507.507,0,0,0,.179-.2.811.811,0,0,0,.08-.257,2.288,2.288,0,0,0,0-.6Zm-2.509.558v2.078l-.052.01a2.135,2.135,0,0,1-.466,0l-.058-.01v-2.078h-1.921c0-.013-.007-.03-.011-.051a1.154,1.154,0,0,1-.015-.207,1.405,1.405,0,0,1,.013-.213q0-.027.009-.045h1.925v-2.082a.557.557,0,0,1,.058-.011,2.02,2.02,0,0,1,.466,0c.021,0,.038.007.052.01v2.082l1.933.01.006.036a1.405,1.405,0,0,1,.013.213,1.368,1.368,0,0,1-.013.214c0,.018,0,.032-.008.044Z"
								      fill="#008a83" />
								<g id="Group_1575" data-name="Group 1575">
									<g id="Group_1574" data-name="Group 1574">
										<path id="Path_4280" data-name="Path 4280"
										      d="M5513.535,595.887a.088.088,0,0,0-.06.057.509.509,0,0,0-.037.14,1.676,1.676,0,0,0-.015.24,1.466,1.466,0,0,0,.017.24.512.512,0,0,0,.042.137.146.146,0,0,0,.038.051.058.058,0,0,0,.031.009h1.818v1.979a.047.047,0,0,0,.01.032.133.133,0,0,0,.054.037.537.537,0,0,0,.147.038,2.25,2.25,0,0,0,.519,0,.551.551,0,0,0,.148-.038.113.113,0,0,0,.048-.033.062.062,0,0,0,.01-.036v-1.979h1.823a.066.066,0,0,0,.037-.011.118.118,0,0,0,.038-.049.5.5,0,0,0,.039-.135,1.653,1.653,0,0,0,.015-.242,1.538,1.538,0,0,0-.016-.24.527.527,0,0,0-.037-.14.121.121,0,0,0-.034-.048l-1.865-.009v-1.973a.09.09,0,0,0-.013-.047.106.106,0,0,0-.045-.033.551.551,0,0,0-.148-.038,2.146,2.146,0,0,0-.519,0,.537.537,0,0,0-.147.038.123.123,0,0,0-.051.037.076.076,0,0,0-.013.043v1.973Z"
										      fill="#e9f6f5" />
									</g>
								</g>
							</g>
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['uye-ol'] ;?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12  d-flex justify-content-between mt-3">
        <form class="col-12  col-md-5 basvuru-girisyap " method="post">
            <div class="input-group flex-nowrap w-75 mx-auto">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="15.832" height="15.993" viewBox="0 0 15.832 15.993">
                  <g id="Group_316" data-name="Group 316" transform="translate(-796.483 -621.702)">
                    <path id="Path_841" data-name="Path 841"
                          d="M811.3,637.7H797.5a1.022,1.022,0,0,1-.745-.325,1.01,1.01,0,0,1-.268-.759,7.935,7.935,0,0,1,15.827,0,1.01,1.01,0,0,1-.268.759A1.022,1.022,0,0,1,811.3,637.7Zm-6.9-7.435a6.958,6.958,0,0,0-6.916,6.423l.016.012h13.8A6.934,6.934,0,0,0,804.4,630.26Z"
                          fill="#d1d1d1" />
                    <path id="Path_842" data-name="Path 842"
                          d="M804.4,630.245a4.271,4.271,0,1,1,4.272-4.271A4.276,4.276,0,0,1,804.4,630.245Zm0-7.543a3.271,3.271,0,1,0,3.272,3.272A3.275,3.275,0,0,0,804.4,622.7Z"
                          fill="#d1d1d1" />
                  </g>
                </svg>
              </span>
                <input name="eposta" type="text" class="form-control  border-0" placeholder="<?php echo $textCagir['form']['eposta'];?>" aria-describedby="addon-wrapping">
            </div>
            <div class="input-group flex-nowrap w-75 mx-auto my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="14.833" height="19.39" viewBox="0 0 14.833 19.39">
                  <g id="Group_315" data-name="Group 315" transform="translate(-797.983 -677.916)" opacity="0.9">
                    <g id="Group_313" data-name="Group 313">
                      <g id="Group_311" data-name="Group 311">
                        <path id="Path_838" data-name="Path 838"
                              d="M811.225,697.306H799.574a1.593,1.593,0,0,1-1.591-1.591v-9.988a1.592,1.592,0,0,1,1.591-1.59h11.651a1.592,1.592,0,0,1,1.591,1.59v9.988A1.593,1.593,0,0,1,811.225,697.306Zm-11.651-12.568a.99.99,0,0,0-.989.989v9.988a.989.989,0,0,0,.989.989h11.651a.99.99,0,0,0,.989-.989v-9.988a.991.991,0,0,0-.989-.989Z"
                              fill="#d1d1d1" />
                      </g>
                      <g id="Group_312" data-name="Group 312">
                        <path id="Path_839" data-name="Path 839"
                              d="M809.9,684.6a.3.3,0,0,1-.3-.3v-1.734a4.046,4.046,0,1,0-8.091,0V684.3a.3.3,0,1,1-.6,0v-1.734a4.647,4.647,0,1,1,9.293,0V684.3A.3.3,0,0,1,809.9,684.6Z"
                              fill="#d1d1d1" />
                      </g>
                    </g>
                    <g id="Group_314" data-name="Group 314">
                      <path id="Path_840" data-name="Path 840"
                            d="M805.4,691.323a1.524,1.524,0,1,1,1.524-1.524A1.525,1.525,0,0,1,805.4,691.323Zm0-2.4a.88.88,0,1,0,.88.88A.88.88,0,0,0,805.4,688.919Z"
                            fill="#d1d1d1" />
                      <line id="Line_1" data-name="Line 1" y2="3.003" transform="translate(805.399 690.679)" fill="none"
                            stroke="#d1d1d1" stroke-miterlimit="10" stroke-width="0.816" />
                    </g>
                  </g>
                </svg>
              </span>
                <input name="sifre" type="password" class="form-control border-0" id="passwd2" placeholder="<?php echo $textCagir['menu']['sifre'];?>" aria-label="Username" aria-describedby="addon-wrapping">
                <i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd2',this)"></i>

            </div>
            <input type="hidden" name="methodName" value="girisYap">

            <button class="w-75  " type="submit"><?php echo $textCagir['menu']['giris-yap'];?></button>
            <div class="d-md-flex d-block w-75 mt-4 mx-auto justify-content-between">

                <a href="" class="forgotPassword "><?php echo $textCagir['menu']['parola'];?></a>
            </div>
        </form>

		<form class="col-12  col-md-5 basvuru-uyeol phone-form-display" method="post">
			<div class="w-75 mx-auto input-group flex-nowrap mb-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.832" height="15.993"
                                     viewBox="0 0 15.832 15.993">
                                    <g id="Group_316" data-name="Group 316" transform="translate(-796.483 -621.702)">
                                        <path id="Path_841" data-name="Path 841"
                                              d="M811.3,637.7H797.5a1.022,1.022,0,0,1-.745-.325,1.01,1.01,0,0,1-.268-.759,7.935,7.935,0,0,1,15.827,0,1.01,1.01,0,0,1-.268.759A1.022,1.022,0,0,1,811.3,637.7Zm-6.9-7.435a6.958,6.958,0,0,0-6.916,6.423l.016.012h13.8A6.934,6.934,0,0,0,804.4,630.26Z"
                                              fill="#d1d1d1" />
                                        <path id="Path_842" data-name="Path 842"
                                              d="M804.4,630.245a4.271,4.271,0,1,1,4.272-4.271A4.276,4.276,0,0,1,804.4,630.245Zm0-7.543a3.271,3.271,0,1,0,3.272,3.272A3.275,3.275,0,0,0,804.4,622.7Z"
                                              fill="#d1d1d1" />
                                    </g>
                                </svg>
                            </span>
				<input name="adSoyad" type="text" class="form-control  border-0" placeholder="<?php echo $textCagir['form']['ad']." ". $textCagir['form']['soyad'];?>" aria-describedby="addon-wrapping">
			</div>

			<div class="w-75 mx-auto input-group flex-nowrap my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <i class="far fa-lg fa-phone-alt  font-weight-bold"></i>
                            </span>
				<input name="telefon" type="number" class="form-control  border-0" placeholder="<?php echo
				$textCagir['form']['telefon'];?>"  aria-describedby="addon-wrapping">	</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <svg id="katman_1" class="mail-basvuru-svg" data-name="katman 1"
                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                    <defs>
                                        <style>
                                            .cls-1 {
	                                            fill: #010101;
                                            }
                                        </style>
                                    </defs>
                                    <path class="cls-1"
                                          d="M13.57,2.65v0l0,0,0,0a.18.18,0,0,0-.1,0H.58a.18.18,0,0,0-.1,0l0,0,0,0v0s0,0,0,0v8.66a.16.16,0,0,0,.16.16H13.42a.16.16,0,0,0,.16-.16V2.67S13.57,2.66,13.57,2.65ZM.74,3,5.16,7l-4.42,4ZM8.07,7.65h0l.52-.47L13,11.12H1l4.4-3.94.53.47A1.61,1.61,0,0,0,7,8.06,1.56,1.56,0,0,0,8.07,7.65ZM8.84,7,13.26,3v7.9ZM13,2.82,7.87,7.42a1.31,1.31,0,0,1-1.74,0L1,2.82Z" />
                                </svg>
                            </span>
				<input name="eposta" type="text" class="form-control  border-0" placeholder="<?php echo $textCagir['form']['eposta'];?>" aria-describedby="addon-wrapping">
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14.833" height="19.39"
                                     viewBox="0 0 14.833 19.39">
                                    <g id="Group_315" data-name="Group 315" transform="translate(-797.983 -677.916)"
                                       opacity="0.9">
                                        <g id="Group_313" data-name="Group 313">
                                            <g id="Group_311" data-name="Group 311">
                                                <path id="Path_838" data-name="Path 838"
                                                      d="M811.225,697.306H799.574a1.593,1.593,0,0,1-1.591-1.591v-9.988a1.592,1.592,0,0,1,1.591-1.59h11.651a1.592,1.592,0,0,1,1.591,1.59v9.988A1.593,1.593,0,0,1,811.225,697.306Zm-11.651-12.568a.99.99,0,0,0-.989.989v9.988a.989.989,0,0,0,.989.989h11.651a.99.99,0,0,0,.989-.989v-9.988a.991.991,0,0,0-.989-.989Z"
                                                      fill="#d1d1d1" />
                                            </g>
                                            <g id="Group_312" data-name="Group 312">
                                                <path id="Path_839" data-name="Path 839"
                                                      d="M809.9,684.6a.3.3,0,0,1-.3-.3v-1.734a4.046,4.046,0,1,0-8.091,0V684.3a.3.3,0,1,1-.6,0v-1.734a4.647,4.647,0,1,1,9.293,0V684.3A.3.3,0,0,1,809.9,684.6Z"
                                                      fill="#d1d1d1" />
                                            </g>
                                        </g>
                                        <g id="Group_314" data-name="Group 314">
                                            <path id="Path_840" data-name="Path 840"
                                                  d="M805.4,691.323a1.524,1.524,0,1,1,1.524-1.524A1.525,1.525,0,0,1,805.4,691.323Zm0-2.4a.88.88,0,1,0,.88.88A.88.88,0,0,0,805.4,688.919Z"
                                                  fill="#d1d1d1" />
                                            <line id="Line_1" data-name="Line 1" y2="3.003"
                                                  transform="translate(805.399 690.679)" fill="none" stroke="#d1d1d1"
                                                  stroke-miterlimit="10" stroke-width="0.816" />
                                        </g>
                                    </g>
                                </svg>
                            </span>
				<input name ="sifre" type="password" id="passwd3" class="form-control border-0" placeholder="<?php echo
				$textCagir['menu']['sifre']; ?>"
				       aria-label="Username" aria-describedby="addon-wrapping">
				<i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd3',this)"></i>

			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <i class="far fa-ballot "
                                   style="padding-right: 3px; justify-content: center; display: flex; "></i>
                            </span>
				<input name ="vergiDiresi" type="text" class="form-control border-0" placeholder="Vergi Daire"
				       aria-label="Username" aria-describedby="addon-wrapping">
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <i class="far fa-abacus"></i>
                            </span>
				<input name ="vergiNo" type="number" class="form-control border-0" placeholder="Vergi-No"
				       aria-label="Username" aria-describedby="addon-wrapping">
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap position-relative my-3">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <i class="far fa-city"></i>
                            </span>
				<input name ="il" type="text" class="form-control border-0" id="il"  placeholder="<?php echo
				$textCagir['form']['il'] ;?>">
				<ul id="il-list" class="d-block position-absolute  w-100" style="z-index: 9999; top: 100%;">
				</ul>
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3 position-relative">
                            <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                                <i class="far fa-building"></i>
                            </span>
				<input name ="ilce" type="text" class="form-control border-0" id="ilce"  placeholder="<?php echo $textCagir['form']['ilce'] ;?>"
				       disabled>
				<ul id="ilce-list" class="d-block position-absolute  w-100" style="top: 100%;">
				</ul>
			</div>
			<input type="hidden" name="methodName" value="bayiKayit">
			<button class="w-75 " type="submit"><?php echo $textCagir['menu']['uye-ol'] ;?></button>
		</form>
	</div>
</section>


