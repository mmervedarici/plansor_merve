<?php

include_once('Controller/kategoriController.php');
$kategoriController = new kategoriController($db);


$sayfaDetay = $kategoriController->sayfa($temaConfig['link']);

$benzerSayfaSayisi = count($sayfaDetay['sayfaData']['benzerSayfalar']);
if ($benzerSayfaSayisi > 0) {
    $benzerSayfalar = $sayfaDetay['sayfaData']['benzerSayfalar'];
} else {
    $benzerSayfalar = [];
}


?>
<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $sayfaDetay["baslik"]; ?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>

                        <li><?php echo $sayfaDetay["baslik"]; ?></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="circle1 icSayfa"></div>
                <div class="circle2 icSayfa"></div>
                <div class="cizgi icSayfa"></div>
                <div class="ucgen icSayfa"></div>
            </div>

            <?php $i=0; foreach ($benzerSayfalar as $row) {
                if($i %2 == 0){
                    $order = "order-0 order-lg-1";
                }else{
                    $order = "";
                }
                ?>
                <div class="col-12 my-4">
                    <div class="row">
                        <div class="col-12 col-lg-6  <?php echo $order; ?>">
                            <div class="detayImg">
                                <img class="img-fluid" src="../userFiles/resimler/<?php echo $row["resim"]; ?>"
                                     alt="<?php echo $row["baslik"]; ?>" />
                            </div>

                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-lg-8 ortalaX">
                                    <div class="plansorBaslik">
                                        <h4><?php echo $row["baslik"]; ?></h4>
                                    </div>

                                </div>
                                <div class="col-12 kurumsalText mt-4">
                                    <p>
                                        <?php echo substr($row["icerik"], '0', '300') . '...'; ?>
                                    </p>

                                </div>
                                <button class="mt-4 ml-auto mr-3 text-white font-weight-bold py-1 kurumsal-haberDevami">
                                    <a class="text-white" href="<?php echo $row["link"]; ?>"><?php echo $textCagir['icSayfa']['devamini-oku']; ?></a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>

        </div>
    </div>
</section>