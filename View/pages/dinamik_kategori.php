<?php

	include_once('Controller/kategoriController.php');
	$kategoriController = new kategoriController($db);


	$sayfaDetay = $kategoriController->sayfa($temaConfig['link']);

	$benzerSayfaSayisi = count($sayfaDetay['sayfaData']['benzerSayfalar']);
	if ($benzerSayfaSayisi > 0) {
		$benzerSayfalar = $sayfaDetay['sayfaData']['benzerSayfalar'];
	} else {
		$benzerSayfalar = [];
	}




?>
<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $sayfaDetay["baslik"]; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>

						<li><?php echo $sayfaDetay["baslik"]; ?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="col-12">
			<div class="circle1 icSayfa"></div>
			<div class="circle2 icSayfa"></div>
			<div class="cizgi icSayfa"></div>
			<div class="ucgen icSayfa"></div>
		</div>

		<!--                    sağ-->

		<div class="w-100 my-4">
			<div class=" w-50 w-md-100 float-lg-left icerikAlanı mr-3">
				<img class="img-fluid"    src="<?php echo $siteBilgi['siteURL']; ?>userFiles/resimler/<?php echo $sayfaDetay["resim"]; ?>" alt="kurumsal">
			</div>
			<p style="word-break: break-all;">
				<?php echo $sayfaDetay["icerik"]; ?>
			</p>
		</div>
		<?php if($benzerSayfalar):?>
			<div>
				<div class="imar-sekil">
					<div class="w-25 mb-3 mx-auto">
						<div class="plansorBaslik">
							<h4><?php echo $sayfaDetay["baslik"]; ?></h4>
						</div>
					</div>
					<div class="col-12 col-lg-3 d-flex phone-sekil" style="gap:2.5rem;">
						<?php $i = 0; ?>
						<?php foreach ($benzerSayfalar as $item) : ?>
							<div class="col-12 hizmetBtnDis sag my-3">
								<a href="<?php echo $item["link"]; ?>">
									<div class="row no-gutters hizmetBtnIc">

										<div class="col-10">
											<div class="hizmetText sag justify-content-center" style="font-size: 14px;">
												<?php echo $item["baslik"]; ?>
											</div>
										</div>
										<div class="col-2">
											<?php $i++; ?>
											<div class="hizmetNumber sag"><?php echo $i; ?></div>
										</div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
</section>
<br>
<br>
