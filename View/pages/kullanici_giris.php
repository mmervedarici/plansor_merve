<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['menu']['kullaniciGirisi'];?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="./"><?php echo $textCagir['menu']['kullanici'];?></a></li>
						<li><?php echo $textCagir['menu']['kullaniciGirisi'];?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bayi px-md-5 mx-lg-auto " style="max-width: 1240px;">
	<div class=" d-flex justify-content-between ">
		<div class="d-flex col-12 justify-content-between">
			<div class="girisyap-header  phone-active col-6 col-md-5">
				<div class="d-flex  justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.859" height="17.12" viewBox="0 0 17.859 17.12">
						<g id="Group_1571" data-name="Group 1571" transform="translate(-4877.51 -580.466)">
							<path id="Path_4277" data-name="Path 4277"
							      d="M4889.173,588.861a4.646,4.646,0,1,0-5.467,0,8.979,8.979,0,0,0-6.194,7.852.812.812,0,0,0,.216.611.822.822,0,0,0,.6.262h16.219a.818.818,0,0,0,.818-.872A8.979,8.979,0,0,0,4889.173,588.861Zm5.666,8.173a.4.4,0,0,1-.29.126H4878.33a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.29,8.555,8.555,0,0,1,6.235-7.581,4.619,4.619,0,0,0,4.535,0,8.553,8.553,0,0,1,6.234,7.582A.377.377,0,0,1,4894.839,597.034Zm-4.18-11.922a4.217,4.217,0,0,1-2.022,3.6c-.086.053-.173.1-.263.151s-.2.1-.309.147a4.216,4.216,0,0,1-3.251,0c-.106-.045-.208-.095-.309-.147s-.177-.1-.263-.151a4.217,4.217,0,1,1,6.417-3.6Z"
							      fill="rgb(202, 200, 200)" />
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['giris-yap'];?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
			<div class="uyeol-header col-md-5 col-6 ">
				<div class="d-flex justify-content-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="17.858" height="19.829" viewBox="0 0 17.858 19.829">
						<g id="Group_1577" data-name="Group 1577" transform="translate(-5506.911 -579.43)">
							<path id="Path_4278" data-name="Path 4278"
							      d="M5518.573,587.824a4.646,4.646,0,1,0-5.467,0,8.978,8.978,0,0,0-6.193,7.853.81.81,0,0,0,.215.61.823.823,0,0,0,.6.262h16.22a.824.824,0,0,0,.6-.262.812.812,0,0,0,.215-.61A8.981,8.981,0,0,0,5518.573,587.824ZM5524.24,596a.4.4,0,0,1-.29.126h-16.22a.4.4,0,0,1-.29-.126.384.384,0,0,1-.1-.289,8.555,8.555,0,0,1,6.235-7.582,4.62,4.62,0,0,0,4.536,0,8.555,8.555,0,0,1,6.234,7.582A.382.382,0,0,1,5524.24,596Zm-4.18-11.921a4.218,4.218,0,0,1-2.022,3.6c-.087.053-.174.1-.264.151s-.2.1-.308.147a4.218,4.218,0,0,1-3.252,0c-.106-.044-.208-.095-.308-.147s-.178-.1-.264-.151a4.217,4.217,0,1,1,6.418-3.6Z"
							      fill="rgb(202, 200, 200)" />
							<g id="Group_1576" data-name="Group 1576">
								<path id="Path_4279" data-name="Path 4279"
								      d="M5518.634,596.024a.892.892,0,0,0-.072-.252.505.505,0,0,0-.172-.206.433.433,0,0,0-.245-.076H5516.7v-1.576a.475.475,0,0,0-.067-.248.485.485,0,0,0-.222-.193.925.925,0,0,0-.254-.07,2.612,2.612,0,0,0-.637,0,.935.935,0,0,0-.254.07.511.511,0,0,0-.218.182.471.471,0,0,0-.077.259v1.576h-1.438a.438.438,0,0,0-.255.081.507.507,0,0,0-.162.2.909.909,0,0,0-.071.252,2.037,2.037,0,0,0-.02.3,1.776,1.776,0,0,0,.024.311.889.889,0,0,0,.08.25.526.526,0,0,0,.17.2.442.442,0,0,0,.251.076h1.421v1.582a.446.446,0,0,0,.087.267.523.523,0,0,0,.208.163.991.991,0,0,0,.254.07,2.692,2.692,0,0,0,.638,0,1,1,0,0,0,.253-.07.5.5,0,0,0,.213-.174.45.45,0,0,0,.076-.256v-1.582h1.427a.464.464,0,0,0,.247-.071.507.507,0,0,0,.179-.2.811.811,0,0,0,.08-.257,2.288,2.288,0,0,0,0-.6Zm-2.509.558v2.078l-.052.01a2.135,2.135,0,0,1-.466,0l-.058-.01v-2.078h-1.921c0-.013-.007-.03-.011-.051a1.154,1.154,0,0,1-.015-.207,1.405,1.405,0,0,1,.013-.213q0-.027.009-.045h1.925v-2.082a.557.557,0,0,1,.058-.011,2.02,2.02,0,0,1,.466,0c.021,0,.038.007.052.01v2.082l1.933.01.006.036a1.405,1.405,0,0,1,.013.213,1.368,1.368,0,0,1-.013.214c0,.018,0,.032-.008.044Z"
								      fill="#008a83" />
								<g id="Group_1575" data-name="Group 1575">
									<g id="Group_1574" data-name="Group 1574">
										<path id="Path_4280" data-name="Path 4280"
										      d="M5513.535,595.887a.088.088,0,0,0-.06.057.509.509,0,0,0-.037.14,1.676,1.676,0,0,0-.015.24,1.466,1.466,0,0,0,.017.24.512.512,0,0,0,.042.137.146.146,0,0,0,.038.051.058.058,0,0,0,.031.009h1.818v1.979a.047.047,0,0,0,.01.032.133.133,0,0,0,.054.037.537.537,0,0,0,.147.038,2.25,2.25,0,0,0,.519,0,.551.551,0,0,0,.148-.038.113.113,0,0,0,.048-.033.062.062,0,0,0,.01-.036v-1.979h1.823a.066.066,0,0,0,.037-.011.118.118,0,0,0,.038-.049.5.5,0,0,0,.039-.135,1.653,1.653,0,0,0,.015-.242,1.538,1.538,0,0,0-.016-.24.527.527,0,0,0-.037-.14.121.121,0,0,0-.034-.048l-1.865-.009v-1.973a.09.09,0,0,0-.013-.047.106.106,0,0,0-.045-.033.551.551,0,0,0-.148-.038,2.146,2.146,0,0,0-.519,0,.537.537,0,0,0-.147.038.123.123,0,0,0-.051.037.076.076,0,0,0-.013.043v1.973Z"
										      fill="#e9f6f5" />
									</g>
								</g>
							</g>
						</g>
					</svg>
					<h6 class="ml-2"><?php echo $textCagir['menu']['uye-ol'];?></h6>
				</div>
				<div class="d-flex align-items-center w-100">
					<div class="cubuk w-50"></div>
					<div class="yuvarlak"></div>
					<div class="cubuk w-50 "></div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12  d-flex justify-content-between mt-3">
		<form class="col-12  col-md-5 basvuru-girisyap " method="post">
			<div class="input-group flex-nowrap w-75 mx-auto">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="15.832" height="15.993" viewBox="0 0 15.832 15.993">
                  <g id="Group_316" data-name="Group 316" transform="translate(-796.483 -621.702)">
                    <path id="Path_841" data-name="Path 841"
                          d="M811.3,637.7H797.5a1.022,1.022,0,0,1-.745-.325,1.01,1.01,0,0,1-.268-.759,7.935,7.935,0,0,1,15.827,0,1.01,1.01,0,0,1-.268.759A1.022,1.022,0,0,1,811.3,637.7Zm-6.9-7.435a6.958,6.958,0,0,0-6.916,6.423l.016.012h13.8A6.934,6.934,0,0,0,804.4,630.26Z"
                          fill="#d1d1d1" />
                    <path id="Path_842" data-name="Path 842"
                          d="M804.4,630.245a4.271,4.271,0,1,1,4.272-4.271A4.276,4.276,0,0,1,804.4,630.245Zm0-7.543a3.271,3.271,0,1,0,3.272,3.272A3.275,3.275,0,0,0,804.4,622.7Z"
                          fill="#d1d1d1" />
                  </g>
                </svg>
              </span>
				<input name="eposta" type="text" class="form-control  border-0" placeholder="<?php echo $textCagir['form']['eposta'];?>" aria-describedby="addon-wrapping">
			</div>
			<div class="input-group flex-nowrap w-75 mx-auto my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="14.833" height="19.39" viewBox="0 0 14.833 19.39">
                  <g id="Group_315" data-name="Group 315" transform="translate(-797.983 -677.916)" opacity="0.9">
                    <g id="Group_313" data-name="Group 313">
                      <g id="Group_311" data-name="Group 311">
                        <path id="Path_838" data-name="Path 838"
                              d="M811.225,697.306H799.574a1.593,1.593,0,0,1-1.591-1.591v-9.988a1.592,1.592,0,0,1,1.591-1.59h11.651a1.592,1.592,0,0,1,1.591,1.59v9.988A1.593,1.593,0,0,1,811.225,697.306Zm-11.651-12.568a.99.99,0,0,0-.989.989v9.988a.989.989,0,0,0,.989.989h11.651a.99.99,0,0,0,.989-.989v-9.988a.991.991,0,0,0-.989-.989Z"
                              fill="#d1d1d1" />
                      </g>
                      <g id="Group_312" data-name="Group 312">
                        <path id="Path_839" data-name="Path 839"
                              d="M809.9,684.6a.3.3,0,0,1-.3-.3v-1.734a4.046,4.046,0,1,0-8.091,0V684.3a.3.3,0,1,1-.6,0v-1.734a4.647,4.647,0,1,1,9.293,0V684.3A.3.3,0,0,1,809.9,684.6Z"
                              fill="#d1d1d1" />
                      </g>
                    </g>
                    <g id="Group_314" data-name="Group 314">
                      <path id="Path_840" data-name="Path 840"
                            d="M805.4,691.323a1.524,1.524,0,1,1,1.524-1.524A1.525,1.525,0,0,1,805.4,691.323Zm0-2.4a.88.88,0,1,0,.88.88A.88.88,0,0,0,805.4,688.919Z"
                            fill="#d1d1d1" />
                      <line id="Line_1" data-name="Line 1" y2="3.003" transform="translate(805.399 690.679)" fill="none"
                            stroke="#d1d1d1" stroke-miterlimit="10" stroke-width="0.816" />
                    </g>
                  </g>
                </svg>
              </span>
				<input name="sifre" type="password" class="form-control border-0" id="passwd2" placeholder="<?php echo $textCagir['menu']['sifre'];?>" aria-label="Username" aria-describedby="addon-wrapping">
				<i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd2',this)"></i>

			</div>
            <input type="hidden" name="methodName" value="girisYap">

			<button class="w-75  " type="submit"><?php echo $textCagir['menu']['giris-yap'];?></button>
			<div class="d-md-flex d-block w-75 mt-4 mx-auto justify-content-between">

				<a href="" class="forgotPassword "><?php echo $textCagir['menu']['parola'];?></a>
			</div>
		</form>

		<form class="col-12  col-md-5 basvuru-uyeol phone-form-display" method="post">
			<div class="w-75 mx-auto input-group flex-nowrap mb-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="15.832" height="15.993" viewBox="0 0 15.832 15.993">
                  <g id="Group_316" data-name="Group 316" transform="translate(-796.483 -621.702)">
                    <path id="Path_841" data-name="Path 841"
                          d="M811.3,637.7H797.5a1.022,1.022,0,0,1-.745-.325,1.01,1.01,0,0,1-.268-.759,7.935,7.935,0,0,1,15.827,0,1.01,1.01,0,0,1-.268.759A1.022,1.022,0,0,1,811.3,637.7Zm-6.9-7.435a6.958,6.958,0,0,0-6.916,6.423l.016.012h13.8A6.934,6.934,0,0,0,804.4,630.26Z"
                          fill="#d1d1d1" />
                    <path id="Path_842" data-name="Path 842"
                          d="M804.4,630.245a4.271,4.271,0,1,1,4.272-4.271A4.276,4.276,0,0,1,804.4,630.245Zm0-7.543a3.271,3.271,0,1,0,3.272,3.272A3.275,3.275,0,0,0,804.4,622.7Z"
                          fill="#d1d1d1" />
                  </g>
                </svg>
              </span>
				<input name="adSoyad" type="text" class="form-control  border-0" placeholder="<?php echo $textCagir['form']['ad']." ". $textCagir['form']['soyad']; ?>" aria-describedby="addon-wrapping">
			</div>

			<div class="w-75 mx-auto input-group flex-nowrap my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <i class="far fa-lg fa-phone-alt  font-weight-bold"></i>
              </span>
				<input name="telefon" type="number" class="form-control  border-0" placeholder="<?php echo
				$textCagir['form']['telefon'];?>"
				       aria-describedby="addon-wrapping">
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg id="katman_1" class="mail-basvuru-svg" data-name="katman 1" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 14 14">
                  <defs>
                    <style>
                      .cls-1 {
	                      fill: #010101;
                      }
                    </style>
                  </defs>
                  <path class="cls-1"
                        d="M13.57,2.65v0l0,0,0,0a.18.18,0,0,0-.1,0H.58a.18.18,0,0,0-.1,0l0,0,0,0v0s0,0,0,0v8.66a.16.16,0,0,0,.16.16H13.42a.16.16,0,0,0,.16-.16V2.67S13.57,2.66,13.57,2.65ZM.74,3,5.16,7l-4.42,4ZM8.07,7.65h0l.52-.47L13,11.12H1l4.4-3.94.53.47A1.61,1.61,0,0,0,7,8.06,1.56,1.56,0,0,0,8.07,7.65ZM8.84,7,13.26,3v7.9ZM13,2.82,7.87,7.42a1.31,1.31,0,0,1-1.74,0L1,2.82Z" />
                </svg>
              </span>
				<input name="eposta" type="text" class="form-control  border-0" placeholder="<?php echo
				$textCagir['form']['eposta'];
				?>" aria-describedby="addon-wrapping">
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <svg xmlns="http://www.w3.org/2000/svg" width="14.833" height="19.39" viewBox="0 0 14.833 19.39">
                  <g id="Group_315" data-name="Group 315" transform="translate(-797.983 -677.916)" opacity="0.9">
                    <g id="Group_313" data-name="Group 313">
                      <g id="Group_311" data-name="Group 311">
                        <path id="Path_838" data-name="Path 838"
                              d="M811.225,697.306H799.574a1.593,1.593,0,0,1-1.591-1.591v-9.988a1.592,1.592,0,0,1,1.591-1.59h11.651a1.592,1.592,0,0,1,1.591,1.59v9.988A1.593,1.593,0,0,1,811.225,697.306Zm-11.651-12.568a.99.99,0,0,0-.989.989v9.988a.989.989,0,0,0,.989.989h11.651a.99.99,0,0,0,.989-.989v-9.988a.991.991,0,0,0-.989-.989Z"
                              fill="#d1d1d1" />
                      </g>
                      <g id="Group_312" data-name="Group 312">
                        <path id="Path_839" data-name="Path 839"
                              d="M809.9,684.6a.3.3,0,0,1-.3-.3v-1.734a4.046,4.046,0,1,0-8.091,0V684.3a.3.3,0,1,1-.6,0v-1.734a4.647,4.647,0,1,1,9.293,0V684.3A.3.3,0,0,1,809.9,684.6Z"
                              fill="#d1d1d1" />
                      </g>
                    </g>
                    <g id="Group_314" data-name="Group 314">
                      <path id="Path_840" data-name="Path 840"
                            d="M805.4,691.323a1.524,1.524,0,1,1,1.524-1.524A1.525,1.525,0,0,1,805.4,691.323Zm0-2.4a.88.88,0,1,0,.88.88A.88.88,0,0,0,805.4,688.919Z"
                            fill="#d1d1d1" />
                      <line id="Line_1" data-name="Line 1" y2="3.003" transform="translate(805.399 690.679)" fill="none"
                            stroke="#d1d1d1" stroke-miterlimit="10" stroke-width="0.816" />
                    </g>
                  </g>
                </svg>
              </span>
				<input name="sifre" type="password" id="passwd3" class="form-control border-0" placeholder="<?php echo
				$textCagir['menu']['sifre'];
				?>"
				       aria-label="Username" aria-describedby="addon-wrapping">
				<i class="far fa-eye parola uyeol-icon " onclick="parolaGoster('passwd3',this)"></i>
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap position-relative my-3">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <i class="far fa-city"></i>
              </span>
				<input name="il" type="text" class="form-control border-0" id="il" placeholder="<?php echo
				$textCagir['form']['il'];?>">
				<ul id="il-list" class="d-block position-absolute  w-100" style="z-index: 9999; top: 100%;">
				</ul>
			</div>
			<div class="w-75 mx-auto input-group flex-nowrap my-3 position-relative">
              <span class="input-group-text bg-transparent border-0" id="addon-wrapping">
                <i class="far fa-building"></i>
              </span>
				<input name="ilce" type="text" class="form-control border-0" id="ilce"  placeholder="<?php echo
				$textCagir['form']['ilce'];?>">
				<ul id="ilce-list" class="d-block position-absolute  w-100" style="top: 100%;">
				</ul>
			</div>
            <input type="hidden" name="methodName" value="uyeKayit">
			<button class="w-75 " type="submit"><?php echo $textCagir['menu']['uye-ol'];?></button>
		</form>
	</div>
</section>










<!--
<div class="alertBox succes row">
	<div class="icon col-12">
		<svg xmlns="http://www.w3.org/2000/svg" width="191.697" height="179.014" viewBox="0 0 191.697 179.014">
			<g id="Group_24" data-name="Group 24" transform="translate(-264.124 195.876)">
				<g id="Group_23" data-name="Group 23">
					<g id="Group_21" data-name="Group 21" transform="translate(30 -223.001)">
						<g id="Group_18" data-name="Group 18">
							<g id="Group_17" data-name="Group 17">
								<g id="Group_16" data-name="Group 16">
									<g id="Group_12" data-name="Group 12">
										<circle id="Ellipse_7" data-name="Ellipse 7" cx="89.007" cy="89.007" r="89.007" transform="translate(234.624 27.625)" fill="#fff"/>
										<path id="Path_17" data-name="Path 17" d="M323.631,206.139a89.507,89.507,0,1,1,89.507-89.507A89.609,89.609,0,0,1,323.631,206.139Zm0-178.014a88.507,88.507,0,1,0,88.507,88.507A88.607,88.607,0,0,0,323.631,28.125Z" fill="#23b502"/>
									</g>
								</g>
							</g>
						</g>
					</g>
					<g id="Group_13" data-name="Group 13" transform="translate(30 -223)">
						<circle id="Ellipse_8" data-name="Ellipse 8" cx="74.139" cy="74.139" r="74.139" transform="translate(249.492 42.492)" fill="#fff"/>
						<path id="Path_18" data-name="Path 18" d="M323.631,191.271a74.639,74.639,0,1,1,74.639-74.639A74.724,74.724,0,0,1,323.631,191.271Zm0-148.279a73.639,73.639,0,1,0,73.639,73.64A73.723,73.723,0,0,0,323.631,42.992Z" fill="#23b502"/>
					</g>
					<g id="Group_22" data-name="Group 22" transform="translate(-19 -218.79)">
						<g id="Group_20" data-name="Group 20" transform="translate(50 -3)">
							<g id="Group_19" data-name="Group 19">
								<g id="Group_15" data-name="Group 15">
									<g id="Group_14" data-name="Group 14">
										<circle id="Ellipse_9" data-name="Ellipse 9" cx="53.964" cy="53.964" r="53.964" transform="translate(252.727 86.217) rotate(-22.5)" fill="#23b502"/>
										<path id="Path_19" data-name="Path 19" d="M424.219,68.367c-1.66-3.19-7.522-5.377-17.425-6.5-5.426-.616-15.442-1.171-21.567,2.015-27.993,14.559-50.8,37.429-70.133,58.606l-10.742-19.238c-.752-1.347-2.514-4.5-17.944-5.766-5.436-.445-15.218-.814-19.74,1.71a6.03,6.03,0,0,0-2.591,8.424l26.876,48.133c1.165,2.086,3.732,4.185,14.537,5.408,1.5.17,3.5.366,5.739.5,6.988.424,16.232.261,19.289-3.123,3.3-3.657,6.662-7.423,9.909-11.065,24.111-27.042,49.044-55,81.109-71.682C424.918,74.031,425.445,70.724,424.219,68.367Z" opacity="0.2" style="mix-blend-mode: multiply;isolation: isolate"/>
										<path id="Path_20" data-name="Path 20" d="M418.517,60.9c-1.85-3.084-7.834-4.912-17.787-5.433-5.453-.286-15.484-.233-21.4,3.318-27.059,16.229-48.442,40.439-66.452,62.749l-11.889-18.552c-.832-1.3-2.782-4.341-18.26-4.668-5.453-.114-15.24.11-19.6,2.9a6.031,6.031,0,0,0-2.076,8.566L290.792,156.2c1.289,2.011,3.978,3.951,14.838,4.517,1.506.079,3.518.153,5.759.153,7,0,16.217-.723,19.064-4.286,3.076-3.851,6.2-7.813,9.22-11.645,22.428-28.453,45.62-57.875,76.616-76.467C419.557,66.514,419.884,63.181,418.517,60.9Z" fill="#fff"/>
										<path id="Path_21" data-name="Path 21" d="M381.657,62.678c-27.71,16.62-49.08,41.4-69.2,66.622l-15.294-23.867c-2.262-3.53-36.075-3.986-32.3,1.907q14.873,23.207,29.744,46.415c2.148,3.352,29.527,3.468,32.3,0,25.748-32.225,51.049-67.579,87.047-89.17C419.852,61.048,390.925,57.119,381.657,62.678Z" fill="#23b502"/>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
		</svg>
	</div>
	<p class="col-12 text-success font-weight-bold">Lorem ipsum dolor sit amet consectetur</p>
</div>
<div class="alertBox warning row">
	<div class="icon col-12">
		<svg xmlns="http://www.w3.org/2000/svg" width="179.014" height="179.014" viewBox="0 0 179.014 179.014">
			<g id="Group_26" data-name="Group 26" transform="translate(-1579.001 188.875)">
				<g id="Group_25" data-name="Group 25">
					<g id="Group_2" data-name="Group 2" transform="translate(0 -216)">
						<circle id="Ellipse_1" data-name="Ellipse 1" cx="89.007" cy="89.007" r="89.007" transform="translate(1579.501 27.625)" fill="#fff"/>
						<path id="Path_2" data-name="Path 2" d="M1668.508,206.139a89.507,89.507,0,1,1,89.507-89.507A89.609,89.609,0,0,1,1668.508,206.139Zm0-178.014a88.507,88.507,0,1,0,88.507,88.507A88.607,88.607,0,0,0,1668.508,28.125Z" fill="#ffed00"/>
					</g>
					<g id="Group_3" data-name="Group 3" transform="translate(0 -216)">
						<circle id="Ellipse_2" data-name="Ellipse 2" cx="74.139" cy="74.139" r="74.139" transform="translate(1594.369 42.492)" fill="#fff"/>
						<path id="Path_3" data-name="Path 3" d="M1668.508,191.271a74.639,74.639,0,1,1,74.64-74.639A74.724,74.724,0,0,1,1668.508,191.271Zm0-148.279a73.639,73.639,0,1,0,73.64,73.64A73.723,73.723,0,0,0,1668.508,42.992Z" fill="#ffed00"/>
					</g>
					<g id="Group_6" data-name="Group 6" transform="translate(0.113 -215.017)">
						<g id="Group_4" data-name="Group 4">
							<circle id="Ellipse_3" data-name="Ellipse 3" cx="53.964" cy="53.964" r="53.964" transform="translate(1614.432 61.072)" fill="#ffed00"/>
						</g>
						<g id="Group_5" data-name="Group 5">
							<path id="Path_4" data-name="Path 4" d="M1666.79,150.077c0-5.622,3.9-9.682,9.211-9.682,5.621,0,9.214,4.06,9.214,9.682,0,5.465-3.593,9.68-9.214,9.68C1670.537,159.757,1666.79,155.542,1666.79,150.077Z" opacity="0.2" style="mix-blend-mode: multiply;isolation: isolate"/>
							<path id="Path_5" data-name="Path 5" d="M1670.538,122.441l-1.868-64.019a5.466,5.466,0,0,1,5.464-5.625h3.735a5.465,5.465,0,0,1,5.463,5.625l-1.867,64.019a5.467,5.467,0,0,1-5.464,5.307h0A5.466,5.466,0,0,1,1670.538,122.441Z" opacity="0.2" style="mix-blend-mode: multiply;isolation: isolate"/>
							<path id="Path_6" data-name="Path 6" d="M1668.507,164.257c-7.816,0-13.711-6.1-13.711-14.18s5.9-14.181,13.711-14.181c7.945,0,13.713,5.963,13.713,14.181S1676.452,164.257,1668.507,164.257Zm0-19.361c-2.817,0-4.711,2.082-4.711,5.181,0,2.578,1.457,5.18,4.711,5.18,3.475,0,4.713-2.676,4.713-5.18C1673.22,148.519,1672.761,144.9,1668.507,144.9Z" fill="#fff"/>
							<path id="Path_7" data-name="Path 7" d="M1668.507,132.248a9.919,9.919,0,0,1-9.962-9.675h0l-1.867-64.019A9.966,9.966,0,0,1,1666.64,48.3h3.734a9.964,9.964,0,0,1,9.962,10.256l-1.867,64.019A9.919,9.919,0,0,1,1668.507,132.248Zm-.966-9.938a.966.966,0,0,0,1.932,0l1.867-64.018a.965.965,0,0,0-.966-.994h-3.734a.967.967,0,0,0-.966.994Z" fill="#fff"/>
							<path id="Path_8" data-name="Path 8" d="M1659.3,150.077c0-5.622,3.9-9.682,9.211-9.682,5.621,0,9.214,4.06,9.214,9.682,0,5.465-3.593,9.68-9.214,9.68C1663.042,159.757,1659.3,155.542,1659.3,150.077Z" fill="#ffed00"/>
							<path id="Path_9" data-name="Path 9" d="M1663.044,122.441l-1.868-64.019a5.466,5.466,0,0,1,5.464-5.625h3.735a5.465,5.465,0,0,1,5.463,5.625l-1.867,64.019a5.467,5.467,0,0,1-5.464,5.307h0A5.466,5.466,0,0,1,1663.044,122.441Z" fill="#ffed00"/>
						</g>
					</g>
				</g>
			</g>
		</svg>
	</div>
	<p class="col-12 text-warning font-weight-bold">Lorem ipsum dolor sit amet consectetur</p>
</div>
<div class="alertBox danger row">
	<div class="icon col-12">
		<svg xmlns="http://www.w3.org/2000/svg" width="179.015" height="179.014" viewBox="0 0 179.015 179.014">
			<g id="Group_28" data-name="Group 28" transform="translate(-916.661 238)">
				<g id="Group_27" data-name="Group 27">
					<g id="Group_8" data-name="Group 8" transform="translate(9.531 -265.125)">
						<circle id="Ellipse_4" data-name="Ellipse 4" cx="89.007" cy="89.007" r="89.007" transform="translate(907.63 27.625)" fill="#fff"/>
						<path id="Path_11" data-name="Path 11" d="M996.637,206.139a89.507,89.507,0,1,1,89.508-89.507A89.609,89.609,0,0,1,996.637,206.139Zm0-178.014a88.507,88.507,0,1,0,88.508,88.507A88.607,88.607,0,0,0,996.637,28.125Z" fill="#eb1d1d"/>
					</g>
					<g id="Group_9" data-name="Group 9" transform="translate(9.532 -264.992)">
						<circle id="Ellipse_5" data-name="Ellipse 5" cx="74.139" cy="74.139" r="74.139" transform="translate(922.498 42.492)" fill="#fff"/>
						<path id="Path_12" data-name="Path 12" d="M996.637,191.271a74.639,74.639,0,1,1,74.64-74.639A74.725,74.725,0,0,1,996.637,191.271Zm0-148.279a73.639,73.639,0,1,0,73.64,73.64A73.724,73.724,0,0,0,996.637,42.992Z" fill="#eb1d1d"/>
					</g>
					<g id="Group_10" data-name="Group 10" transform="translate(7 -263.828)">
						<circle id="Ellipse_6" data-name="Ellipse 6" cx="53.964" cy="53.964" r="53.964" transform="translate(942.561 61.072)" fill="#eb1d1d"/>
						<path id="Path_13" data-name="Path 13" d="M1069.771,70.646c-.064-1.408-.713-3.383-3.444-4.8-4.6-2.383-15.589-3.424-17.745-3.608-13.992-1.2-17.277.9-18.357,1.595a212.5,212.5,0,0,0-27.769,21.215,78.953,78.953,0,0,1-5.827-17.031c-.745-3.956-5.037-5.462-6.448-5.958-4.142-1.453-10.87-2.44-18-2.646-12.467-.355-15.03,1.846-15.994,2.675a5.437,5.437,0,0,0-1.879,5.162c2.752,14.616,11.609,30.981,21.1,46.685-7.693,9.39-15.062,19.174-22.209,28.662-3.007,3.992-6.116,8.119-9.194,12.145a4.991,4.991,0,0,0,1.1,7.129c3.643,2.786,15.573,3.9,19.142,4.176l.473.036c16.385,1.2,18.839-2.008,19.892-3.385,3.1-4.049,6.215-8.188,9.231-12.19q1.757-2.331,3.514-4.661c7.813,9.853,14.79,17.171,21.234,22.283,3.392,2.69,10.365,4.15,17.412,4.666,7.68.562,15.447,0,18.759-1.307,3.195-1.263,3.97-3.463,4.148-4.663a5.65,5.65,0,0,0-2.332-5.325c-10.762-8.536-23.358-23.859-36.534-44.423,15.114-17.774,29.362-30.369,47.166-41.76A5.21,5.21,0,0,0,1069.771,70.646Z" opacity="0.2" style="mix-blend-mode: multiply;isolation: isolate"/>
						<path id="Path_14" data-name="Path 14" d="M1062.822,64.673c-.167-1.4-.959-3.321-3.786-4.533-4.761-2.041-15.8-2.276-17.962-2.3-14.041-.175-17.164,2.164-18.191,2.932a212.462,212.462,0,0,0-26.145,23.186,79.016,79.016,0,0,1-7.056-16.56c-1.031-3.892-5.422-5.08-6.865-5.471-4.237-1.147-11.02-1.64-18.145-1.325-12.459.556-14.854,2.938-15.756,3.835a5.44,5.44,0,0,0-1.5,5.286c3.813,14.377,13.841,30.051,24.456,45.02-6.987,9.926-13.622,20.222-20.057,30.207-2.707,4.2-5.507,8.544-8.283,12.783a4.992,4.992,0,0,0,1.622,7.03c3.837,2.513,15.816,2.753,19.4,2.767h.474c16.429,0,18.642-3.378,19.592-4.828,2.793-4.265,5.6-8.62,8.316-12.831q1.581-2.454,3.164-4.906c8.512,9.256,16.005,16.046,22.8,20.673,3.579,2.435,10.641,3.382,17.706,3.382,7.7,0,15.406-1.124,18.614-2.673,3.094-1.494,3.706-3.745,3.8-4.953a5.648,5.648,0,0,0-2.714-5.141c-11.357-7.728-25.038-22.09-39.681-41.636,13.775-18.831,27.065-32.433,43.991-45.094A5.213,5.213,0,0,0,1062.822,64.673Z" fill="#fff"/>
						<path id="Path_15" data-name="Path 15" d="M1025.6,64.4A207.705,207.705,0,0,0,995.825,91.6c-4.064-7.035-8.445-15.187-10.527-23.039-1.356-5.114-34.618-4.234-33.495,0,4,15.074,15.488,32.015,25.638,46.176-10.576,14.783-20.163,30.292-30.111,45.482-1.855,2.833,30.559,4.483,33.494,0,4.93-7.527,9.773-15.131,14.689-22.686,7.434,8.435,16.658,18.039,25.943,24.357,7.166,4.876,38.212,2.139,32.3-1.885-15.5-10.545-31.6-30.194-42.753-45.351,13.161-18.394,26.809-33.734,46.9-48.761C1062.328,62.576,1030.563,60.688,1025.6,64.4Z" fill="#eb1d1d"/>
					</g>
				</g>
			</g>
		</svg>
	</div>
	<p class="col-12 text-danger font-weight-bold">Lorem ipsum dolor sit amet consectetur</p>
</div>

ALERT-->