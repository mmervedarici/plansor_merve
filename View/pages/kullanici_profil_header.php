<div class="basvuru-takip-header row">
    <div class="col-3">
        <a href="kullanici-basvuru-takip" class="baslik py-3 <?php if ( end($value) == "kullanici-basvuru-takip" ) { echo"active"; } ?>">
            <img src="../assets/img/talep-anasayfa.svg" alt="Talep Anasayfa">
            <span class="ml-lg-4"><?php echo $textCagir['icSayfa']['talep-anaSayfa']; ?></span>

        </a>
    </div>
    <div class="col-3">
        <a href="kullanici-profil-bilgileri" class="baslik py-3 <?php if (  end($value) == "kullanici-profil-bilgileri" ) { echo"active"; } ?>">
            <img src="../assets/img/peopleicon.svg">
            <span class="ml-lg-4"><?php echo $textCagir['menu']['profil-bilgileri']; ?></span>
        </a>
    </div>
    <div class="col-3">
        <a href="kullanici-sifre-degistir" class="baslik py-3 <?php if ( end($value)  == "kullanici-sifre-degistir" ) { echo"active"; } ?>">
            <img src="../assets/img/kilit.svg">
            <span class="ml-lg-4"><?php echo $textCagir['menu']['sifre-degistir']; ?></span>
        </a>
    </div>
    <div class="col-3 profil-header-cikis">
        <form method="post">
            <input type="hidden" name="methodName" value="cikisYap">
            <button type="submit" class="baslik py-3 border-0">
                <img src="../assets/img/cıkıs.svg">
                <span class="ml-lg-4 cikis"><?php echo $textCagir['menu']['cikis-yap']; ?></span>
            </button>
        </form>
    </div>
</div>