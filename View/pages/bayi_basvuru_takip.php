<?php

	if (isset($_SESSION['hbt_login'])) {

		if ($_SESSION['hbt_seviye'] == "2") {
			$siparisListesi = $siparisController->siparisListesi('2', $_SESSION['hbt_kullaniciID']);

		} else {
			echo $utility->yonlendir('./');
		}
	} else {
		echo $utility->yonlendir('./');
	}

?>
<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['form']['bayi-basvuru-takip']; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="./"><?php echo $textCagir['form']['basvuru-takip']; ?></a></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bayi mx-auto" style="position: relative;max-width: 1140px;user-select: none;">

	<?php include "bayi_profil_header.php"; ?>
	<div class="pt-5 basvuru-takip-tablo basvuru-takip-tablo-onay  w-100 position-relative "
	     style="background-color: rgba(255, 255, 255, .5);">
		<div class="d-flex w-100 position-relative mobile-none">
			<div class=" position-absolute" style="left: -140px;">
				<div class="talep-durum talep-durum-onay active px-2 py-2"><?php echo $textCagir['form']['onay-bekleyen']; ?></div>
				<div class="talep-durum talep-durum-islemdekiler px-2 py-2"><?php echo $textCagir['form']['islemdekiler']; ?></div>
				<div class="talep-durum talep-durum-tamamlandi px-2 py-2"><?php echo $textCagir['form']['tamamlandi']; ?></div>
			</div>

			<!-- tablo başlıkları -->
			<div class="d-flex align-items-center w-100">
				<div class="basvuru-takip-tablo-header-item">
					<img src="../assets/img/peopleicon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['form']['siparis-no']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/talepbilgileri-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/taleptarihi-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/siparisdurumu.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/islemler-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['menu']['islemler']; ?></span>
				</div>
			</div>

		</div>
		<div class="d-flex position-relative w-100 mobile-none f-size-12">
			<div class="position-absolute " style="left: -45px; ">
				<?php $i = 1;
					foreach ($siparisListesi['onayBekleyenler'] as $row) { ?>
						<div class="takip-talep-numara"><?php echo $i; ?></div>
						<?php $i++;
					} ?>
			</div>
			<div class="w-100">
				<?php $i = 1;
					foreach ($siparisListesi['onayBekleyenler'] as $row) { ?>
						<div class="d-flex ">
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['siparisID']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
								- <?php echo substr($row['kayitTarih'], 11, 5); ?>
							</div>
							<div class="basvuru-takip-tablo-item">

								<span class="siparis-durum onaybekliyor"><?php echo $textCagir['form']['onay-bekliyor']; ?></span>
							</div>
							<div class="basvuru-takip-tablo-item takip-tablo-islemler d-flex">
								<form method="post">
									<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
									<input type="hidden" name="methodName" value="bayiSiparis">
									<input type="hidden" name="islem" value="ret">
									<button class="takip-tablo-islemler-item-reddet">
										<?php echo $textCagir['icSayfa']['siparis-iptal']; ?>
									</button>
								</form>
								<form method="post">
									<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
									<input type="hidden" name="methodName" value="bayiSiparis">
									<input type="hidden" name="islem" value="onay">
									<button class="takip-tablo-islemler-item-onay">
										<?php echo $textCagir['icSayfa']['onayla']; ?>
									</button>

								</form>

								
							</div>
						</div>
					<?php } ?>

			</div>
		</div>
	</div>
	<!-- mobil -->
	<div class=" basvuru-takip-tablo-mobil basvuru-takip-tablo-mobil-onay w-100  ">
		<div class="d-flex mx-2">
			<div class="mobil-surecler mobil-surecler-onay d-flex justify-content-center active col-4">
				<?php echo $textCagir['form']['onay-bekleyen']; ?>
			</div>
			<div class="mobil-surecler mobil-surecler-islemdekiler col-4 d-flex align-items-center justify-content-center">
				<?php echo $textCagir['form']['islemdekiler']; ?>
			</div>
			<div class="mobil-surecler mobil-surecler-tamamlandi col-4 d-flex align-items-center justify-content-center">
				<?php echo $textCagir['form']['tamamlandi']; ?>
			</div>
		</div>
		<?php if (count($siparisListesi['onayBekleyenler']) == 0) {
			?>
			<div class="m-5 text-center">
				<?php echo $textCagir['form']['siparis-yok']; ?>
			</div>
			<?php
		} ?>
		<?php $i = 1;
			foreach ($siparisListesi['onayBekleyenler'] as $row) { ?>
				<div class="basvuru-takip-tablo-mobil-siparis my-3 mx-2">
					<div class="siparisno">Sıra No :<?php echo $i; ?></div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/peopleicon.svg" alt="">
							<span>  <?php echo $textCagir['form']['siparis-no']; ?></span>
						</div>
						<div class="col-7 p-0">
							#<?php echo $row['siparisID']; ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/talepbilgileri-icon.svg" alt="">
							<span> <?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
						</div>
						<div class="col-7 p-0">
							<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/taleptarihi-icon.svg" alt="">
							<span> <?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
						</div>
						<div class="col-7 p-0">
							<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
							- <?php echo substr($row['kayitTarih'], 11, 5); ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center ">
							<img src="../assets/img/siparisdurumu.svg" alt="">
							<span><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
						</div>
						<div class="col-7 p-0 onay-mobil">
							<div class="onaybekliyor text-center"><?php echo $textCagir['form']['onay-bekleyen']; ?></div>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center ">
							<img src="../assets/img/islemler-icon.svg" alt="">
							<span> <?php echo $textCagir['menu']['islemler']; ?></span>
						</div>
						<div class="col-7 p-0 onay-mobil onay-mobil-buton d-flex justify-content-md-center">
							<!--	                    <form method="post" action="bayi-siparis-detay">-->
							<!--		                    <input type="hidden" name="siparisID" value="-->
							<?php //echo $row['siparisID']; ?><!--">-->
							<!--		                    <button type="submit" class="takip-tablo-islemler-item p-0 border-0 bg-none">-->
							<!---->
							<!--			                    <svg xmlns="http://www.w3.org/2000/svg" width="25.259" height="25.088"-->
							<!--			                         viewBox="0 0 13.259 14.088">-->
							<!--				                    <g id="Group_1516" data-name="Group 1516"-->
							<!--				                       transform="translate(-4415.024 -589.071)">-->
							<!--					                    <path id="Path_3450" data-name="Path 3450"-->
							<!--					                          d="M4420.11,599.076q1.531.011,3.063-.012a.286.286,0,0,0,.286-.339.308.308,0,0,0-.346-.308c-.474.011-.947,0-1.421,0h0c-.5,0-1,0-1.5,0a.319.319,0,0,0-.348.316C4419.839,598.908,4419.924,599.075,4420.11,599.076Z"-->
							<!--					                          fill="none"/>-->
							<!--					                    <path id="Path_3451" data-name="Path 3451"-->
							<!--					                          d="M4423,600.773q-1.284,0-2.569,0a1.544,1.544,0,0,0-.273.015c-.192.036-.331.145-.311.353a.318.318,0,0,0,.37.313c.465-.013.931,0,1.4,0h1.372c.307,0,.485-.13.479-.347S4423.3,600.773,4423,600.773Z"-->
							<!--					                          fill="none"/>-->
							<!--					                    <path id="Path_3452" data-name="Path 3452"-->
							<!--					                          d="M4423.16,599.614c-1,0-1.991,0-2.986,0a.321.321,0,1,0,.013.641c.472-.01.945,0,1.418,0h1.518a.3.3,0,0,0,.335-.321A.279.279,0,0,0,4423.16,599.614Z"-->
							<!--					                          fill="none"/>-->
							<!--					                    <path id="Path_3453" data-name="Path 3453"-->
							<!--					                          d="M4425.369,593.566a.42.42,0,0,0-.422.4.426.426,0,0,0,.4.428.413.413,0,0,0,.027-.826Z"-->
							<!--					                          fill="none"/>-->
							<!--					                    <path id="Path_3454" data-name="Path 3454"-->
							<!--					                          d="M4426.668,593.566a.423.423,0,0,0-.42.4.434.434,0,0,0,.4.423.414.414,0,0,0,.414-.406A.409.409,0,0,0,4426.668,593.566Z"-->
							<!--					                          fill="none"/>-->
							<!--					                    <path id="Path_3455" data-name="Path 3455"-->
							<!--					                          d="M4427.231,592.26h-5.565q-2.82,0-5.64,0a.913.913,0,0,0-1,.943q-.009,2.757,0,5.514a.889.889,0,0,0,.922.934c.3.009.6-.007.9.005.233.01.324-.069.321-.315-.013-.89,0-1.78,0-2.67a.858.858,0,0,0-.032-.308h-.815v-1.079h10.8v1.079h-.942a.284.284,0,0,0-.033.11q.007,1.461,0,2.92c0,.2.085.264.27.26.274-.006.549,0,.824,0a.92.92,0,0,0,1.046-1.036q0-2.658,0-5.315A.935.935,0,0,0,4427.231,592.26Zm-1.889,2.132a.426.426,0,0,1-.4-.428.42.42,0,0,1,.422-.4.413.413,0,0,1-.027.826Zm1.31,0a.434.434,0,0,1-.4-.423.423.423,0,0,1,.42-.4.409.409,0,0,1,.4.42A.414.414,0,0,1,4426.652,594.392Z"-->
							<!--					                          fill="#777b83"/>-->
							<!--					                    <path id="Path_3456" data-name="Path 3456"-->
							<!--					                          d="M4424.309,596.611h-2.644q-1.347,0-2.694,0a.591.591,0,0,0-.64.634q0,2.631,0,5.262a.608.608,0,0,0,.656.651q2.669,0,5.338,0a.611.611,0,0,0,.655-.653q0-2.619,0-5.237A.6.6,0,0,0,4424.309,596.611Zm-4.112,1.812c.5,0,1,0,1.5,0h0c.474,0,.947.008,1.421,0a.308.308,0,0,1,.346.308.286.286,0,0,1-.286.339q-1.531.021-3.063.012c-.186,0-.271-.168-.261-.337A.319.319,0,0,1,4420.2,598.423Zm3.261,1.51a.3.3,0,0,1-.335.321H4421.6c-.473,0-.946-.006-1.418,0a.321.321,0,1,1-.013-.641c1,0,1.991,0,2.986,0A.279.279,0,0,1,4423.458,599.933Zm-.471,1.517h-1.372c-.465,0-.931-.009-1.4,0a.318.318,0,0,1-.37-.313c-.02-.208.119-.317.311-.353a1.544,1.544,0,0,1,.273-.015q1.285,0,2.569,0c.3,0,.459.116.464.33S4423.294,601.45,4422.987,601.45Z"-->
							<!--					                          fill="#008a83"/>-->
							<!--					                    <path id="Path_3457" data-name="Path 3457"-->
							<!--					                          d="M4418.661,591.349h2.992q1.51,0,3.017,0a.605.605,0,0,0,.677-.674c0-1.03,0,.1,0-.931a.6.6,0,0,0-.674-.672q-3.018,0-6.034,0a.6.6,0,0,0-.676.669c0,1.022,0-.116,0,.906A.611.611,0,0,0,4418.661,591.349Z"-->
							<!--					                          fill="#777b83"/>-->
							<!--				                    </g>-->
							<!--			                    </svg>-->
							<!---->
							<!---->
							<!--		                    </button>-->
							<!--	                    </form>-->

							<button class="takip-tablo-islemler-item detay detay-mobil">
								<svg xmlns="http://www.w3.org/2000/svg" width="12" height="8.606"
								     viewBox="0 0 12 8.606">
									<g id="Group_378" data-name="Group 378"
									   transform="translate(-2434.61 -591.635)">
										<path id="Path_876" data-name="Path 876"
										      d="M2440.61,591.635l-6,6,.9.9,5.1-5.1,5.095,5.1.905-.9Zm-1.5,7.106a1.5,1.5,0,1,0,1.5-1.5A1.5,1.5,0,0,0,2439.11,598.741Z"
										      fill="#008a83"/>
									</g>
								</svg>
							</button>
						</div>
					</div>
				</div>
				<?php $i++;
			} ?>

		<div class="mobil-surecler-surecler">
			<div class="surecler-mobil col-8 position-fixed">
				<div class="col-9 surecler-mobil-islemler">
					<div class="active"><?php echo $textCagir['form']['bayi-yonlendirme']; ?></div>
					<div><?php echo $textCagir['form']['islemde']; ?></div>
					<div><?php echo $textCagir['form']['degerleme-evrak']; ?></div>
					<div><?php echo $textCagir['icSayfa']['talep-tamamlandi']; ?></div>
				</div>
				<div class="col-8 surecler-mobil-tarihler">
					<div class="active">25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pt-5 basvuru-takip-tablo basvuru-takip-tablo-islemdekiler w-100 position-relative "
	     style="background-color: rgba(255, 255, 255, .5);">
		<div class="d-flex w-100 position-relative mobile-none">
			<div class=" position-absolute" style="left: -140px;">
				<div class="talep-durum  talep-durum-onay  px-2 py-2"><?php echo $textCagir['form']['onay-bekleyen']; ?></div>
				<div class="talep-durum  talep-durum-islemdekiler active px-2 py-2"><?php echo $textCagir['form']['islemdekiler']; ?></div>
				<div class="talep-durum  talep-durum-tamamlandi px-2 py-2"><?php echo $textCagir['form']['tamamlandi']; ?></div>
			</div>
			<div class="d-flex align-items-center w-100">
				<div class="basvuru-takip-tablo-header-item">
					<img src="../assets/img/peopleicon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['form']['siparis-no']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/talepbilgileri-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/taleptarihi-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/siparisdurumu.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/islemler-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['menu']['islemler']; ?></span>
				</div>
			</div>

		</div>
		<div class="d-flex position-relative w-100 mobile-none f-size-12">
			<div class="position-absolute " style="left: -45px; ">
				<?php $i = 1;
					foreach ($siparisListesi['islemdekiler'] as $row) { ?>

						<div class="takip-talep-numara"><?php echo $i; ?></div>
						<?php $i++;
					} ?>
			</div>
			<div class="w-100">
				<?php $i = 1;

					foreach ($siparisListesi['islemdekiler'] as $row) {
						if ($row['durum'] == 1) {
							$text = $textCagir['form']['bayi-onay'];
							$class = "isbirlikciyeyonlendirildi text-center";
						}
						if ($row['durum'] == 2) {
							$text = $textCagir['form']['islemde'];
							$class = "islemde text-center";
						}
						if ($row['durum'] == 3) {
							$text = $textCagir['form']['degerleme-evrak'];
							$class = "degerlemeevraklari text-center";
						}
						?>
						<div class="d-flex ">
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['siparisID']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
								- <?php echo substr($row['kayitTarih'], 11, 5); ?>
							</div>
							<div class="basvuru-takip-tablo-item">

								<span class="siparis-durum <?php echo $class; ?>"><?php echo $text; ?></span>

							</div>
							<div class="basvuru-takip-tablo-item islemdekiler takip-tablo-islemler row">

								<button class="takip-tablo-islemler-item p-0 border-0 dosya">
									<svg xmlns="http://www.w3.org/2000/svg" width="25.352" height="25.244"
									     viewBox="0 0 11.352 13.244">
										<g id="Group_1515" data-name="Group 1515"
										   transform="translate(-4370.614 -589.493)">
											<path id="Path_3446" data-name="Path 3446"
											      d="M4381.963,592.8a1.019,1.019,0,0,0-1.123-1.125c-.236,0-.472,0-.742,0-.018,0,0,.862,0,.7,1.363-.072,1.2-.051,1.2,1.022q0,4.023,0,8.044c0,.568-.064.635-.618.636h-6.607c-.553,0-.619-.07-.619-.637,0-.175,0-.349,0-.565h-.673c0,.269,0,.481,0,.693a1.025,1.025,0,0,0,1.156,1.163q3.439,0,6.878,0c.781,0,1.147-.374,1.15-1.163,0-.945,0-1.89,0-2.836Q4381.964,595.769,4381.963,592.8Z"
											      fill="#008a83"/>
											<path id="Path_3447" data-name="Path 3447"
											      d="M4380.467,595.946V597.1c0-.035,0-.07,0-.106C4380.468,596.645,4380.468,596.3,4380.467,595.946Z"
											      fill="#777b83"/>
											<path id="Path_3448" data-name="Path 3448"
											      d="M4380.467,595.946v-1.562C4380.467,594.905,4380.466,595.426,4380.467,595.946Z"
											      fill="#777b83"/>
											<path id="Path_3449" data-name="Path 3449"
											      d="M4373.821,601.244c.152-.007.286-.019.419-.02.679,0,1.359-.009,2.038,0a1.251,1.251,0,0,0,.943-.4c.96-.962,1.919-1.926,2.884-2.882a1.212,1.212,0,0,0,.362-.849v-1.154c0-.52,0-1.041,0-1.562v-3.766a1.009,1.009,0,0,0-1.125-1.121q-3.8-.008-7.6,0a1,1,0,0,0-1.128,1.111q-.007,4.762,0,9.526a1,1,0,0,0,1.116,1.115h2.088Zm3.082-1.1c0-.772-.015-1.493.018-2.212,0-.1.238-.263.371-.268.678-.028,1.357-.013,2.072-.013Zm-5.635-.2q0-4.592,0-9.183c0-.513.084-.6.582-.6h7.367c.5,0,.567.067.567.563q0,2.93,0,5.858v.391c-.815,0-1.581,0-2.348,0-.842,0-1.212.375-1.213,1.224,0,.768,0,1.536,0,2.344-.14.008-.261.021-.382.021q-1.983,0-3.966,0C4371.354,600.566,4371.269,600.479,4371.268,599.947Z"
											      fill="#777b83"/>
										</g>
									</svg>

								</button>
								<form  method="post">
									<input type="hidden" id="surecDetayId" value="<?php echo $row['siparisID']; ?>">
									<input type="hidden" name="methodName" value="bayiSiparis">
									<input type="hidden" name="islem" value="bayiSiparis">
									<button type="submit" class="takip-tablo-islemler-item detay-mobil detay">
										<svg xmlns="http://www.w3.org/2000/svg" width="12" height="8.606"
										     viewBox="0 0 12 8.606">
											<g id="Group_378" data-name="Group 378"
											   transform="translate(-2434.61 -591.635)">
												<path id="Path_876" data-name="Path 876"
												      d="M2440.61,591.635l-6,6,.9.9,5.1-5.1,5.095,5.1.905-.9Zm-1.5,7.106a1.5,1.5,0,1,0,1.5-1.5A1.5,1.5,0,0,0,2439.11,598.741Z"
												      fill="#008a83"/>
											</g>
										</svg>
									</button>
								</form>
								<div class="surecDetayWrapper d-none">
									<div class="surecDetay row">
										<h3 class="col-12 text-center surecDetay-title">Siparis ID: <?php echo $row['siparisID']; ?></h3>
										<div class="col-6 text-center surecDetay-item">
											<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
										</div>
										<div class="col-6 text-center surecDetay-item">
											<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
											- <?php echo substr($row['kayitTarih'], 11, 5); ?>
										</div>

									</div>
								</div>

								<button class="col-3  takip-tablo-islemler-item  ">
									Mesaj
								</button>
								<form method="post">
									<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
									<input type="hidden" name="methodName" value="bayiSiparis">
									<input type="hidden" name="islem" value="ret">

									<button class="takip-tablo-islemler-item-reddet">
										<?php echo $textCagir['icSayfa']['siparis-iptal']; ?>
									</button>
								</form>
								<form method="post">
									<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
									<input type="hidden" name="methodName" value="bayiSiparis">
									<input type="hidden" name="islem" value="onay">
									<button class="takip-tablo-islemler-item-onay">
										<?php echo $textCagir['icSayfa']['onayla']; ?>
									</button>

								</form>
							</div>
						</div>
					<?php } ?>


			</div>
		</div>
	</div>
	<div class=" basvuru-takip-tablo-mobil basvuru-takip-tablo-mobil-islemdekiler w-100  ">
		<div class="d-flex mx-2">
			<div class="mobil-surecler mobil-surecler-onay d-flex justify-content-center  col-4">
				<?php echo $textCagir['form']['onay-bekleyen']; ?>
			</div>
			<div
					class="mobil-surecler mobil-surecler-islemdekiler col-4 d-flex active align-items-center justify-content-center">
				<?php echo $textCagir['form']['islemdekiler']; ?>
			</div>
			<div
					class="mobil-surecler mobil-surecler-tamamlandi col-4 d-flex align-items-center justify-content-center">
				<?php echo $textCagir['form']['tamamlandi']; ?>
			</div>
		</div>
		<?php $i = 1;

			foreach ($siparisListesi['islemdekiler'] as $row) {
				if ($row['durum'] == 1) {
					$text = $textCagir['form']['bayi-onay'];
					$class = "isbirlikciyeyonlendirildi text-center";
				}
				if ($row['durum'] == 2) {
					$text = $textCagir['form']['islemde'];
					$class = "islemde text-center";
				}
				if ($row['durum'] == 3) {
					$text = $textCagir['form']['degerleme-evrak'];
					$class = "degerlemeevraklari text-center";
				}
				?>
				<div class="basvuru-takip-tablo-mobil-siparis my-3 mx-2">
					<div class="siparisno">Sıra No :<?php echo $i; ?></div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/peopleicon.svg" alt="">
							<span><?php echo $textCagir['form']['siparis-no']; ?></span>
						</div>
						<div class="col-7 p-0">
							#<?php echo $row['siparisID']; ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/talepbilgileri-icon.svg" alt="">
							<span> <?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
						</div>
						<div class="col-7 p-0">
							<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center">
							<img src="../assets/img/taleptarihi-icon.svg" alt="">
							<span><?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
						</div>
						<div class="col-7 p-0">
							<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
							- <?php echo substr($row['kayitTarih'], 11, 5); ?>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center ">
							<img src="../assets/img/siparisdurumu.svg" alt="">
							<span><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
						</div>
						<div class="col-7 p-0 onay-mobil">

							<div class="<?php echo $class; ?>"><?php echo $text; ?></div>
						</div>
					</div>
					<div class="kullanici-mobil d-flex text-left">
						<div class="col-5 p-0 d-flex align-content-center ">
							<img src="../assets/img/islemler-icon.svg" alt="">
							<span><?php echo $textCagir['menu']['islemler']; ?></span>
						</div>
						<div class="col-7 p-0 onay-mobil onay-mobil-buton islemdekiler-mobil d-flex justify-content-center">
							<button class="takip-tablo-islemler-item p-0 border-0">
								<svg xmlns="http://www.w3.org/2000/svg" width="25.352" height="25.244"
								     viewBox="0 0 11.352 13.244">
									<g id="Group_1515" data-name="Group 1515"
									   transform="translate(-4370.614 -589.493)">
										<path id="Path_3446" data-name="Path 3446"
										      d="M4381.963,592.8a1.019,1.019,0,0,0-1.123-1.125c-.236,0-.472,0-.742,0-.018,0,0,.862,0,.7,1.363-.072,1.2-.051,1.2,1.022q0,4.023,0,8.044c0,.568-.064.635-.618.636h-6.607c-.553,0-.619-.07-.619-.637,0-.175,0-.349,0-.565h-.673c0,.269,0,.481,0,.693a1.025,1.025,0,0,0,1.156,1.163q3.439,0,6.878,0c.781,0,1.147-.374,1.15-1.163,0-.945,0-1.89,0-2.836Q4381.964,595.769,4381.963,592.8Z"
										      fill="#008a83"/>
										<path id="Path_3447" data-name="Path 3447"
										      d="M4380.467,595.946V597.1c0-.035,0-.07,0-.106C4380.468,596.645,4380.468,596.3,4380.467,595.946Z"
										      fill="#777b83"/>
										<path id="Path_3448" data-name="Path 3448"
										      d="M4380.467,595.946v-1.562C4380.467,594.905,4380.466,595.426,4380.467,595.946Z"
										      fill="#777b83"/>
										<path id="Path_3449" data-name="Path 3449"
										      d="M4373.821,601.244c.152-.007.286-.019.419-.02.679,0,1.359-.009,2.038,0a1.251,1.251,0,0,0,.943-.4c.96-.962,1.919-1.926,2.884-2.882a1.212,1.212,0,0,0,.362-.849v-1.154c0-.52,0-1.041,0-1.562v-3.766a1.009,1.009,0,0,0-1.125-1.121q-3.8-.008-7.6,0a1,1,0,0,0-1.128,1.111q-.007,4.762,0,9.526a1,1,0,0,0,1.116,1.115h2.088Zm3.082-1.1c0-.772-.015-1.493.018-2.212,0-.1.238-.263.371-.268.678-.028,1.357-.013,2.072-.013Zm-5.635-.2q0-4.592,0-9.183c0-.513.084-.6.582-.6h7.367c.5,0,.567.067.567.563q0,2.93,0,5.858v.391c-.815,0-1.581,0-2.348,0-.842,0-1.212.375-1.213,1.224,0,.768,0,1.536,0,2.344-.14.008-.261.021-.382.021q-1.983,0-3.966,0C4371.354,600.566,4371.269,600.479,4371.268,599.947Z"
										      fill="#777b83"/>
									</g>
								</svg>

							</button>
							<form method="post" action="bayi-siparis-detay">
								<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
								<button type="submit" class="takip-tablo-islemler-item p-0 border-0 bg-none">

									<svg xmlns="http://www.w3.org/2000/svg" width="25.259" height="25.088"
									     viewBox="0 0 13.259 14.088">
										<g id="Group_1516" data-name="Group 1516"
										   transform="translate(-4415.024 -589.071)">
											<path id="Path_3450" data-name="Path 3450"
											      d="M4420.11,599.076q1.531.011,3.063-.012a.286.286,0,0,0,.286-.339.308.308,0,0,0-.346-.308c-.474.011-.947,0-1.421,0h0c-.5,0-1,0-1.5,0a.319.319,0,0,0-.348.316C4419.839,598.908,4419.924,599.075,4420.11,599.076Z"
											      fill="none"/>
											<path id="Path_3451" data-name="Path 3451"
											      d="M4423,600.773q-1.284,0-2.569,0a1.544,1.544,0,0,0-.273.015c-.192.036-.331.145-.311.353a.318.318,0,0,0,.37.313c.465-.013.931,0,1.4,0h1.372c.307,0,.485-.13.479-.347S4423.3,600.773,4423,600.773Z"
											      fill="none"/>
											<path id="Path_3452" data-name="Path 3452"
											      d="M4423.16,599.614c-1,0-1.991,0-2.986,0a.321.321,0,1,0,.013.641c.472-.01.945,0,1.418,0h1.518a.3.3,0,0,0,.335-.321A.279.279,0,0,0,4423.16,599.614Z"
											      fill="none"/>
											<path id="Path_3453" data-name="Path 3453"
											      d="M4425.369,593.566a.42.42,0,0,0-.422.4.426.426,0,0,0,.4.428.413.413,0,0,0,.027-.826Z"
											      fill="none"/>
											<path id="Path_3454" data-name="Path 3454"
											      d="M4426.668,593.566a.423.423,0,0,0-.42.4.434.434,0,0,0,.4.423.414.414,0,0,0,.414-.406A.409.409,0,0,0,4426.668,593.566Z"
											      fill="none"/>
											<path id="Path_3455" data-name="Path 3455"
											      d="M4427.231,592.26h-5.565q-2.82,0-5.64,0a.913.913,0,0,0-1,.943q-.009,2.757,0,5.514a.889.889,0,0,0,.922.934c.3.009.6-.007.9.005.233.01.324-.069.321-.315-.013-.89,0-1.78,0-2.67a.858.858,0,0,0-.032-.308h-.815v-1.079h10.8v1.079h-.942a.284.284,0,0,0-.033.11q.007,1.461,0,2.92c0,.2.085.264.27.26.274-.006.549,0,.824,0a.92.92,0,0,0,1.046-1.036q0-2.658,0-5.315A.935.935,0,0,0,4427.231,592.26Zm-1.889,2.132a.426.426,0,0,1-.4-.428.42.42,0,0,1,.422-.4.413.413,0,0,1-.027.826Zm1.31,0a.434.434,0,0,1-.4-.423.423.423,0,0,1,.42-.4.409.409,0,0,1,.4.42A.414.414,0,0,1,4426.652,594.392Z"
											      fill="#777b83"/>
											<path id="Path_3456" data-name="Path 3456"
											      d="M4424.309,596.611h-2.644q-1.347,0-2.694,0a.591.591,0,0,0-.64.634q0,2.631,0,5.262a.608.608,0,0,0,.656.651q2.669,0,5.338,0a.611.611,0,0,0,.655-.653q0-2.619,0-5.237A.6.6,0,0,0,4424.309,596.611Zm-4.112,1.812c.5,0,1,0,1.5,0h0c.474,0,.947.008,1.421,0a.308.308,0,0,1,.346.308.286.286,0,0,1-.286.339q-1.531.021-3.063.012c-.186,0-.271-.168-.261-.337A.319.319,0,0,1,4420.2,598.423Zm3.261,1.51a.3.3,0,0,1-.335.321H4421.6c-.473,0-.946-.006-1.418,0a.321.321,0,1,1-.013-.641c1,0,1.991,0,2.986,0A.279.279,0,0,1,4423.458,599.933Zm-.471,1.517h-1.372c-.465,0-.931-.009-1.4,0a.318.318,0,0,1-.37-.313c-.02-.208.119-.317.311-.353a1.544,1.544,0,0,1,.273-.015q1.285,0,2.569,0c.3,0,.459.116.464.33S4423.294,601.45,4422.987,601.45Z"
											      fill="#008a83"/>
											<path id="Path_3457" data-name="Path 3457"
											      d="M4418.661,591.349h2.992q1.51,0,3.017,0a.605.605,0,0,0,.677-.674c0-1.03,0,.1,0-.931a.6.6,0,0,0-.674-.672q-3.018,0-6.034,0a.6.6,0,0,0-.676.669c0,1.022,0-.116,0,.906A.611.611,0,0,0,4418.661,591.349Z"
											      fill="#777b83"/>
										</g>
									</svg>


								</button>
							</form>

<!--							<button class="takip-tablo-islemler-item detay-mobil detay ">-->
<!--								<svg xmlns="http://www.w3.org/2000/svg" width="12" height="8.606"-->
<!--								     viewBox="0 0 12 8.606">-->
<!--									<g id="Group_378" data-name="Group 378"-->
<!--									   transform="translate(-2434.61 -591.635)">-->
<!--										<path id="Path_876" data-name="Path 876"-->
<!--										      d="M2440.61,591.635l-6,6,.9.9,5.1-5.1,5.095,5.1.905-.9Zm-1.5,7.106a1.5,1.5,0,1,0,1.5-1.5A1.5,1.5,0,0,0,2439.11,598.741Z"-->
<!--										      fill="#008a83"/>-->
<!--									</g>-->
<!--								</svg>-->
<!--							</button>-->
						</div>
					</div>
				</div>

				<?php $i++;
			} ?>


		<div class="mobil-surecler-surecler">
			<div class="surecler-mobil col-8   position-fixed ">
				<div class="col-9 surecler-mobil-islemler">
					<div class="active"><?php echo $textCagir['form']['bayi-yonlendirme']; ?></div>
					<div><?php echo $textCagir['form']['islemde']; ?></div>
					<div><?php echo $textCagir['form']['degerleme-evrak']; ?></div>
					<div><?php echo $textCagir['icSayfa']['talep-tamamlandi']; ?></div>
				</div>
				<div class="col-8 surecler-mobil-tarihler">
					<div class="active">25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
					<div>25 Ocak 2022</div>
				</div>
			</div>
		</div>

	</div>
	<div class="pt-5 basvuru-takip-tablo basvuru-takip-tablo-tamamlandi w-100 position-relative"
	     style="background-color: rgba(255, 255, 255, .5);">
		<div class="d-flex w-100 position-relative mobile-none">
			<div class=" position-absolute" style="left: -140px;">
				<div class="talep-durum talep-durum-onay  px-2 py-2"><?php echo $textCagir['form']['onay-bekleyen']; ?></div>
				<div class="talep-durum talep-durum-islemdekiler px-2 py-2"><?php echo $textCagir['form']['islemdekiler']; ?></div>
				<div class="talep-durum talep-durum-tamamlandi active px-2 py-2">    <?php echo $textCagir['form']['tamamlandi']; ?></div>
			</div>
			<div class="d-flex align-items-center w-100">
				<div class="basvuru-takip-tablo-header-item">
					<img src="../assets/img/peopleicon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['form']['siparis-no']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/talepbilgileri-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/taleptarihi-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/siparisdurumu.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
				</div>
				<div class="basvuru-takip-tablo-header-item user-select-none">
					<img src="../assets/img/islemler-icon.svg" alt="">
					<span class="user-select-none"><?php echo $textCagir['menu']['islemler']; ?></span>
				</div>
			</div>
		</div>
		<div class="d-flex position-relative w-100 mobile-none f-size-12">
			<div class="position-absolute " style="left: -45px; ">
				<?php $i = 1;
					foreach ($siparisListesi['tamamlandi'] as $row) { ?>
						<div class="takip-talep-numara"><?php echo $i; ?></div>
						<?php $i++;
					} ?>
			</div>
			<div class="w-100">
				<?php $i = 1;
					foreach ($siparisListesi['tamamlandi'] as $row) {
						if ($row['durum'] == 4) {
							$text = $textCagir['form']['tamamlandi'];
							$class = "tamamlandi text-center";
						}
						if ($row['durum'] == 5) {
							$text = $textCagir['form']['firma-red'];
							$class = "bayiiptal text-center";
						}
						if ($row['durum'] == 6) {
							$text = $textCagir['form']['musteri-iptal'];
							$class = "bayiiptal text-center";
						}
						?>
						<div class="d-flex ">
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['siparisID']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
								- <?php echo substr($row['kayitTarih'], 11, 5); ?>
							</div>
							<div class="basvuru-takip-tablo-item">
								<span class="siparis-durum <?php echo $class; ?>"><?php echo $text; ?></span>
							</div>
							<div class="basvuru-takip-tablo-item islemdekiler takip-tablo-islemler d-flex justify-content-center">
								<button class="takip-tablo-islemler-item p-0 border-0">
									<svg xmlns="http://www.w3.org/2000/svg" width="25.352" height="25.244"
									     viewBox="0 0 11.352 13.244">
										<g id="Group_1515" data-name="Group 1515"
										   transform="translate(-4370.614 -589.493)">
											<path id="Path_3446" data-name="Path 3446"
											      d="M4381.963,592.8a1.019,1.019,0,0,0-1.123-1.125c-.236,0-.472,0-.742,0-.018,0,0,.862,0,.7,1.363-.072,1.2-.051,1.2,1.022q0,4.023,0,8.044c0,.568-.064.635-.618.636h-6.607c-.553,0-.619-.07-.619-.637,0-.175,0-.349,0-.565h-.673c0,.269,0,.481,0,.693a1.025,1.025,0,0,0,1.156,1.163q3.439,0,6.878,0c.781,0,1.147-.374,1.15-1.163,0-.945,0-1.89,0-2.836Q4381.964,595.769,4381.963,592.8Z"
											      fill="#008a83"/>
											<path id="Path_3447" data-name="Path 3447"
											      d="M4380.467,595.946V597.1c0-.035,0-.07,0-.106C4380.468,596.645,4380.468,596.3,4380.467,595.946Z"
											      fill="#777b83"/>
											<path id="Path_3448" data-name="Path 3448"
											      d="M4380.467,595.946v-1.562C4380.467,594.905,4380.466,595.426,4380.467,595.946Z"
											      fill="#777b83"/>
											<path id="Path_3449" data-name="Path 3449"
											      d="M4373.821,601.244c.152-.007.286-.019.419-.02.679,0,1.359-.009,2.038,0a1.251,1.251,0,0,0,.943-.4c.96-.962,1.919-1.926,2.884-2.882a1.212,1.212,0,0,0,.362-.849v-1.154c0-.52,0-1.041,0-1.562v-3.766a1.009,1.009,0,0,0-1.125-1.121q-3.8-.008-7.6,0a1,1,0,0,0-1.128,1.111q-.007,4.762,0,9.526a1,1,0,0,0,1.116,1.115h2.088Zm3.082-1.1c0-.772-.015-1.493.018-2.212,0-.1.238-.263.371-.268.678-.028,1.357-.013,2.072-.013Zm-5.635-.2q0-4.592,0-9.183c0-.513.084-.6.582-.6h7.367c.5,0,.567.067.567.563q0,2.93,0,5.858v.391c-.815,0-1.581,0-2.348,0-.842,0-1.212.375-1.213,1.224,0,.768,0,1.536,0,2.344-.14.008-.261.021-.382.021q-1.983,0-3.966,0C4371.354,600.566,4371.269,600.479,4371.268,599.947Z"
											      fill="#777b83"/>
										</g>
									</svg>
								</button>
								<form method="post" >
									<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
									<button type="submit" class="takip-tablo-islemler-item p-0 border-0 bg-none">
										<svg xmlns="http://www.w3.org/2000/svg" width="25.259" height="25.088"
										     viewBox="0 0 13.259 14.088">
											<g id="Group_1516" data-name="Group 1516"
											   transform="translate(-4415.024 -589.071)">
												<path id="Path_3450" data-name="Path 3450"
												      d="M4420.11,599.076q1.531.011,3.063-.012a.286.286,0,0,0,.286-.339.308.308,0,0,0-.346-.308c-.474.011-.947,0-1.421,0h0c-.5,0-1,0-1.5,0a.319.319,0,0,0-.348.316C4419.839,598.908,4419.924,599.075,4420.11,599.076Z"
												      fill="none"/>
												<path id="Path_3451" data-name="Path 3451"
												      d="M4423,600.773q-1.284,0-2.569,0a1.544,1.544,0,0,0-.273.015c-.192.036-.331.145-.311.353a.318.318,0,0,0,.37.313c.465-.013.931,0,1.4,0h1.372c.307,0,.485-.13.479-.347S4423.3,600.773,4423,600.773Z"
												      fill="none"/>
												<path id="Path_3452" data-name="Path 3452"
												      d="M4423.16,599.614c-1,0-1.991,0-2.986,0a.321.321,0,1,0,.013.641c.472-.01.945,0,1.418,0h1.518a.3.3,0,0,0,.335-.321A.279.279,0,0,0,4423.16,599.614Z"
												      fill="none"/>
												<path id="Path_3453" data-name="Path 3453"
												      d="M4425.369,593.566a.42.42,0,0,0-.422.4.426.426,0,0,0,.4.428.413.413,0,0,0,.027-.826Z"
												      fill="none"/>
												<path id="Path_3454" data-name="Path 3454"
												      d="M4426.668,593.566a.423.423,0,0,0-.42.4.434.434,0,0,0,.4.423.414.414,0,0,0,.414-.406A.409.409,0,0,0,4426.668,593.566Z"
												      fill="none"/>
												<path id="Path_3455" data-name="Path 3455"
												      d="M4427.231,592.26h-5.565q-2.82,0-5.64,0a.913.913,0,0,0-1,.943q-.009,2.757,0,5.514a.889.889,0,0,0,.922.934c.3.009.6-.007.9.005.233.01.324-.069.321-.315-.013-.89,0-1.78,0-2.67a.858.858,0,0,0-.032-.308h-.815v-1.079h10.8v1.079h-.942a.284.284,0,0,0-.033.11q.007,1.461,0,2.92c0,.2.085.264.27.26.274-.006.549,0,.824,0a.92.92,0,0,0,1.046-1.036q0-2.658,0-5.315A.935.935,0,0,0,4427.231,592.26Zm-1.889,2.132a.426.426,0,0,1-.4-.428.42.42,0,0,1,.422-.4.413.413,0,0,1-.027.826Zm1.31,0a.434.434,0,0,1-.4-.423.423.423,0,0,1,.42-.4.409.409,0,0,1,.4.42A.414.414,0,0,1,4426.652,594.392Z"
												      fill="#777b83"/>
												<path id="Path_3456" data-name="Path 3456"
												      d="M4424.309,596.611h-2.644q-1.347,0-2.694,0a.591.591,0,0,0-.64.634q0,2.631,0,5.262a.608.608,0,0,0,.656.651q2.669,0,5.338,0a.611.611,0,0,0,.655-.653q0-2.619,0-5.237A.6.6,0,0,0,4424.309,596.611Zm-4.112,1.812c.5,0,1,0,1.5,0h0c.474,0,.947.008,1.421,0a.308.308,0,0,1,.346.308.286.286,0,0,1-.286.339q-1.531.021-3.063.012c-.186,0-.271-.168-.261-.337A.319.319,0,0,1,4420.2,598.423Zm3.261,1.51a.3.3,0,0,1-.335.321H4421.6c-.473,0-.946-.006-1.418,0a.321.321,0,1,1-.013-.641c1,0,1.991,0,2.986,0A.279.279,0,0,1,4423.458,599.933Zm-.471,1.517h-1.372c-.465,0-.931-.009-1.4,0a.318.318,0,0,1-.37-.313c-.02-.208.119-.317.311-.353a1.544,1.544,0,0,1,.273-.015q1.285,0,2.569,0c.3,0,.459.116.464.33S4423.294,601.45,4422.987,601.45Z"
												      fill="#008a83"/>
												<path id="Path_3457" data-name="Path 3457"
												      d="M4418.661,591.349h2.992q1.51,0,3.017,0a.605.605,0,0,0,.677-.674c0-1.03,0,.1,0-.931a.6.6,0,0,0-.674-.672q-3.018,0-6.034,0a.6.6,0,0,0-.676.669c0,1.022,0-.116,0,.906A.611.611,0,0,0,4418.661,591.349Z"
												      fill="#777b83"/>
											</g>
										</svg>
									</button>
								</form>

							</div>
						</div>
						<?php $i++;
					} ?>
			</div>
		</div>
	</div>
	<div class="tablo-surec-detay surec-detay position-absolute  w-100 align-items-center">
		<button type="button" class="tablo-surec-detay-text-wrapper active"
		        data-bs-toggle="tooltip" data-bs-placement="top" title="26.03.2022">
			<div class="tablo-surec-detay-text">İş Birlikçiye Yönlendirme</div>
		</button>
		<div class="tablo-surec-detay-arrow">
			<svg xmlns="http://www.w3.org/2000/svg" width="11.052" height="15.411"
			     viewBox="0 0 11.052 15.411">
				<g id="Group_590" data-name="Group 590"
				   transform="translate(-1498.112 -630.073)">
					<g id="Group_589" data-name="Group 589">
						<path id="Path_901" data-name="Path 901"
						      d="M1509.164,637.779l-7.7-7.706-1.162,1.162,6.543,6.544-6.543,6.543,1.162,1.162Zm-9.126-1.927a1.927,1.927,0,1,0,1.927,1.927A1.926,1.926,0,0,0,1500.038,635.852Z"
						      fill="#008a83"/>
					</g>
				</g>
			</svg>
		</div>
		<button type="button" class="tablo-surec-detay-text-wrapper "
		        data-bs-toggle="tooltip" data-bs-placement="top" title="26.03.2022">
			<div class="tablo-surec-detay-text">İşlemde</div>
		</button>
		<div class="tablo-surec-detay-arrow">
			<svg xmlns="http://www.w3.org/2000/svg" width="11.052" height="15.411"
			     viewBox="0 0 11.052 15.411">
				<g id="Group_590" data-name="Group 590"
				   transform="translate(-1498.112 -630.073)">
					<g id="Group_589" data-name="Group 589">
						<path id="Path_901" data-name="Path 901"
						      d="M1509.164,637.779l-7.7-7.706-1.162,1.162,6.543,6.544-6.543,6.543,1.162,1.162Zm-9.126-1.927a1.927,1.927,0,1,0,1.927,1.927A1.926,1.926,0,0,0,1500.038,635.852Z"
						      fill="#008a83"/>
					</g>
				</g>
			</svg>
		</div>
		<button type="button" class="tablo-surec-detay-text-wrapper"
		        data-bs-toggle="tooltip" data-bs-placement="top" title="26.03.2022">
			<div class="tablo-surec-detay-text">Değerleme Evrakları</div>
		</button>
		<div class="tablo-surec-detay-arrow">
			<svg xmlns="http://www.w3.org/2000/svg" width="11.052" height="15.411"
			     viewBox="0 0 11.052 15.411">
				<g id="Group_590" data-name="Group 590"
				   transform="translate(-1498.112 -630.073)">
					<g id="Group_589" data-name="Group 589">
						<path id="Path_901" data-name="Path 901"
						      d="M1509.164,637.779l-7.7-7.706-1.162,1.162,6.543,6.544-6.543,6.543,1.162,1.162Zm-9.126-1.927a1.927,1.927,0,1,0,1.927,1.927A1.926,1.926,0,0,0,1500.038,635.852Z"
						      fill="#008a83"/>
					</g>
				</g>
			</svg>
		</div>
		<button type="button" class="tablo-surec-detay-text-wrapper"
		        data-bs-toggle="tooltip" data-bs-placement="top" title="26.03.2022">
			<div class="tablo-surec-detay-text">Talep Tamamlandı</div>
		</button>
	</div>
	<div class=" basvuru-takip-tablo-mobil basvuru-takip-tablo-mobil-tamamlandi w-100  ">
		<div class="basvuru-takip-tablo-mobil w-100 ">
			<div class="basvuru-takip-tablo-mobil w-100">
				<div class="d-flex mx-2">
					<div class="mobil-surecler mobil-surecler-onay d-flex justify-content-center  col-4">
						<?php echo $textCagir['form']['onay-bekleyen']; ?>
					</div>
					<div class="mobil-surecler mobil-surecler-islemdekiler col-4 d-flex align-items-center justify-content-center">
						<?php echo $textCagir['form']['islemdekiler']; ?>
					</div>
					<div class="mobil-surecler mobil-surecler-tamamlandi col-4 active d-flex align-items-center justify-content-center">
						<?php echo $textCagir['form']['tamamlandi']; ?>
					</div>
				</div>
				<?php $i = 1;
					foreach ($siparisListesi['tamamlandi'] as $row) {
						if ($row['durum'] == 4) {
							$text = $textCagir['form']['tamamlandi'];
							$class = "tamamlandi text-center";
						}
						if ($row['durum'] == 5) {
							$text = $textCagir['form']['firma-red'];
							$class = "bayiiptal text-center";
						}
						if ($row['durum'] == 6) {
							$text = $textCagir['form']['firma-red'];
							$class = "bayiiptal text-center";
						}
						?>
						<div class="basvuru-takip-tablo-mobil-siparis my-3 mx-2">
							<div class="siparisno">Sıra No :<?php echo $i; ?></div>
							<div class="kullanici-mobil d-flex text-left">
								<div class="col-5 p-0 d-flex align-content-center">
									<img src="../assets/img/peopleicon.svg" alt="">
									<span> <?php echo $textCagir['form']['siparis-no']; ?></span>
								</div>
								<div class="col-7 p-0">
									#<?php echo $row['siparisID']; ?>
								</div>
							</div>
							<div class="kullanici-mobil d-flex text-left">
								<div class="col-5 p-0 d-flex align-content-center">
									<img src="../assets/img/talepbilgileri-icon.svg" alt="">
									<span> <?php echo $textCagir['icSayfa']['talep-bilgileri']; ?></span>
								</div>
								<div class="col-7 p-0">
									<?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?>
								</div>
							</div>
							<div class="kullanici-mobil d-flex text-left">
								<div class="col-5 p-0 d-flex align-content-center">
									<img src="../assets/img/taleptarihi-icon.svg" alt="">
									<span> <?php echo $textCagir['icSayfa']['talep-tarih']; ?></span>
								</div>
								<div class="col-7 p-0">
									<?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
									- <?php echo substr($row['kayitTarih'], 11, 5); ?>
								</div>
							</div>
							<div class="kullanici-mobil d-flex text-left">
								<div class="col-5 p-0 d-flex align-content-center">
									<img src="../assets/img/siparisdurumu.svg" alt="">
									<span><?php echo $textCagir['icSayfa']['siparis-durum']; ?></span>
								</div>
								<div class="col-7 p-0 onay-mobil">
									<div class="<?php echo $class; ?>"><?php echo $text; ?></div>
								</div>
							</div>
							<div class="kullanici-mobil d-flex text-left">
								<div class="col-5 p-0 d-flex align-content-center ">
									<img src="../assets/img/islemler-icon.svg" alt="">
									<span><?php echo $textCagir['menu']['islemler']; ?></span>
								</div>
								<div class="col-7 p-0 onay-mobil onay-mobil-buton d-flex justify-content-center">

									<button class="takip-tablo-islemler-item p-0 border-0 dosya">
										<svg xmlns="http://www.w3.org/2000/svg" width="25.352" height="25.244"
										     viewBox="0 0 11.352 13.244">
											<g id="Group_1515" data-name="Group 1515"
											   transform="translate(-4370.614 -589.493)">
												<path id="Path_3446" data-name="Path 3446"
												      d="M4381.963,592.8a1.019,1.019,0,0,0-1.123-1.125c-.236,0-.472,0-.742,0-.018,0,0,.862,0,.7,1.363-.072,1.2-.051,1.2,1.022q0,4.023,0,8.044c0,.568-.064.635-.618.636h-6.607c-.553,0-.619-.07-.619-.637,0-.175,0-.349,0-.565h-.673c0,.269,0,.481,0,.693a1.025,1.025,0,0,0,1.156,1.163q3.439,0,6.878,0c.781,0,1.147-.374,1.15-1.163,0-.945,0-1.89,0-2.836Q4381.964,595.769,4381.963,592.8Z"
												      fill="#008a83"/>
												<path id="Path_3447" data-name="Path 3447"
												      d="M4380.467,595.946V597.1c0-.035,0-.07,0-.106C4380.468,596.645,4380.468,596.3,4380.467,595.946Z"
												      fill="#777b83"/>
												<path id="Path_3448" data-name="Path 3448"
												      d="M4380.467,595.946v-1.562C4380.467,594.905,4380.466,595.426,4380.467,595.946Z"
												      fill="#777b83"/>
												<path id="Path_3449" data-name="Path 3449"
												      d="M4373.821,601.244c.152-.007.286-.019.419-.02.679,0,1.359-.009,2.038,0a1.251,1.251,0,0,0,.943-.4c.96-.962,1.919-1.926,2.884-2.882a1.212,1.212,0,0,0,.362-.849v-1.154c0-.52,0-1.041,0-1.562v-3.766a1.009,1.009,0,0,0-1.125-1.121q-3.8-.008-7.6,0a1,1,0,0,0-1.128,1.111q-.007,4.762,0,9.526a1,1,0,0,0,1.116,1.115h2.088Zm3.082-1.1c0-.772-.015-1.493.018-2.212,0-.1.238-.263.371-.268.678-.028,1.357-.013,2.072-.013Zm-5.635-.2q0-4.592,0-9.183c0-.513.084-.6.582-.6h7.367c.5,0,.567.067.567.563q0,2.93,0,5.858v.391c-.815,0-1.581,0-2.348,0-.842,0-1.212.375-1.213,1.224,0,.768,0,1.536,0,2.344-.14.008-.261.021-.382.021q-1.983,0-3.966,0C4371.354,600.566,4371.269,600.479,4371.268,599.947Z"
												      fill="#777b83"/>
											</g>
										</svg>

									</button>
									<div class="dosyaYuklemeWrapper d-none">
										<div class="dosyaYukleme">
											<form action="" method="post" class="row dosyayuklemeform" enctype="multipart/form-data">
												<input type="hidden" id="siparisID" name="siparisID" value="<?php
													echo $row['siparisID']; ?>">
												<input type="hidden" name="methodName" value="bayiSiparis">
												<input type="hidden" name="islem" value="dosyaYukle">
												<div class="yuklemealani">
													<div class="uploadIcon">
														<i class="far fa-upload"></i>
													</div>
													<div>Lütfen Yüklemek istediğiniz dosyaları seçin</div>
													<label class="custom-file-upload">
														<div>Dosya Seç</div>
														<input type="file" multiple id="file" name="dosya" class="d-none">
													</label>
												</div>
												<div> <input type="text" name="aciklama"> </div>
												<div class="col-12 filename"></div>
												<button type="submit" class="col-12">Gönder</button>
											</form>
										</div>
									</div>
									<form method="post" action="bayi-siparis-detay">
										<input type="hidden" name="siparisID" value="<?php echo $row['siparisID']; ?>">
										<button type="submit" class="takip-tablo-islemler-item p-0 border-0 bg-none">

											<svg xmlns="http://www.w3.org/2000/svg" width="25.259" height="25.088"
											     viewBox="0 0 13.259 14.088">
												<g id="Group_1516" data-name="Group 1516"
												   transform="translate(-4415.024 -589.071)">
													<path id="Path_3450" data-name="Path 3450"
													      d="M4420.11,599.076q1.531.011,3.063-.012a.286.286,0,0,0,.286-.339.308.308,0,0,0-.346-.308c-.474.011-.947,0-1.421,0h0c-.5,0-1,0-1.5,0a.319.319,0,0,0-.348.316C4419.839,598.908,4419.924,599.075,4420.11,599.076Z"
													      fill="none"/>
													<path id="Path_3451" data-name="Path 3451"
													      d="M4423,600.773q-1.284,0-2.569,0a1.544,1.544,0,0,0-.273.015c-.192.036-.331.145-.311.353a.318.318,0,0,0,.37.313c.465-.013.931,0,1.4,0h1.372c.307,0,.485-.13.479-.347S4423.3,600.773,4423,600.773Z"
													      fill="none"/>
													<path id="Path_3452" data-name="Path 3452"
													      d="M4423.16,599.614c-1,0-1.991,0-2.986,0a.321.321,0,1,0,.013.641c.472-.01.945,0,1.418,0h1.518a.3.3,0,0,0,.335-.321A.279.279,0,0,0,4423.16,599.614Z"
													      fill="none"/>
													<path id="Path_3453" data-name="Path 3453"
													      d="M4425.369,593.566a.42.42,0,0,0-.422.4.426.426,0,0,0,.4.428.413.413,0,0,0,.027-.826Z"
													      fill="none"/>
													<path id="Path_3454" data-name="Path 3454"
													      d="M4426.668,593.566a.423.423,0,0,0-.42.4.434.434,0,0,0,.4.423.414.414,0,0,0,.414-.406A.409.409,0,0,0,4426.668,593.566Z"
													      fill="none"/>
													<path id="Path_3455" data-name="Path 3455"
													      d="M4427.231,592.26h-5.565q-2.82,0-5.64,0a.913.913,0,0,0-1,.943q-.009,2.757,0,5.514a.889.889,0,0,0,.922.934c.3.009.6-.007.9.005.233.01.324-.069.321-.315-.013-.89,0-1.78,0-2.67a.858.858,0,0,0-.032-.308h-.815v-1.079h10.8v1.079h-.942a.284.284,0,0,0-.033.11q.007,1.461,0,2.92c0,.2.085.264.27.26.274-.006.549,0,.824,0a.92.92,0,0,0,1.046-1.036q0-2.658,0-5.315A.935.935,0,0,0,4427.231,592.26Zm-1.889,2.132a.426.426,0,0,1-.4-.428.42.42,0,0,1,.422-.4.413.413,0,0,1-.027.826Zm1.31,0a.434.434,0,0,1-.4-.423.423.423,0,0,1,.42-.4.409.409,0,0,1,.4.42A.414.414,0,0,1,4426.652,594.392Z"
													      fill="#777b83"/>
													<path id="Path_3456" data-name="Path 3456"
													      d="M4424.309,596.611h-2.644q-1.347,0-2.694,0a.591.591,0,0,0-.64.634q0,2.631,0,5.262a.608.608,0,0,0,.656.651q2.669,0,5.338,0a.611.611,0,0,0,.655-.653q0-2.619,0-5.237A.6.6,0,0,0,4424.309,596.611Zm-4.112,1.812c.5,0,1,0,1.5,0h0c.474,0,.947.008,1.421,0a.308.308,0,0,1,.346.308.286.286,0,0,1-.286.339q-1.531.021-3.063.012c-.186,0-.271-.168-.261-.337A.319.319,0,0,1,4420.2,598.423Zm3.261,1.51a.3.3,0,0,1-.335.321H4421.6c-.473,0-.946-.006-1.418,0a.321.321,0,1,1-.013-.641c1,0,1.991,0,2.986,0A.279.279,0,0,1,4423.458,599.933Zm-.471,1.517h-1.372c-.465,0-.931-.009-1.4,0a.318.318,0,0,1-.37-.313c-.02-.208.119-.317.311-.353a1.544,1.544,0,0,1,.273-.015q1.285,0,2.569,0c.3,0,.459.116.464.33S4423.294,601.45,4422.987,601.45Z"
													      fill="#008a83"/>
													<path id="Path_3457" data-name="Path 3457"
													      d="M4418.661,591.349h2.992q1.51,0,3.017,0a.605.605,0,0,0,.677-.674c0-1.03,0,.1,0-.931a.6.6,0,0,0-.674-.672q-3.018,0-6.034,0a.6.6,0,0,0-.676.669c0,1.022,0-.116,0,.906A.611.611,0,0,0,4418.661,591.349Z"
													      fill="#777b83"/>
												</g>
											</svg>


										</button>
									</form>
									<button method="post" class="takip-tablo-islemler-item detay-mobil detay">
										<svg xmlns="http://www.w3.org/2000/svg" width="12" height="8.606"
										     viewBox="0 0 12 8.606">
											<g id="Group_378" data-name="Group 378"
											   transform="translate(-2434.61 -591.635)">
												<path id="Path_876" data-name="Path 876"
												      d="M2440.61,591.635l-6,6,.9.9,5.1-5.1,5.095,5.1.905-.9Zm-1.5,7.106a1.5,1.5,0,1,0,1.5-1.5A1.5,1.5,0,0,0,2439.11,598.741Z"
												      fill="#008a83"/>
											</g>
										</svg>
									</button>
								</div>
							</div>
						</div>


						<!-- mobil süreç detayları buraya gelcek     -->
						<div class="mobil-surecler-surecler">
							<div class="surecler-mobil col-8   position-fixed  ">
								<div class="col-9 surecler-mobil-islemler">
									<div class="active"><?php echo $textCagir['form']['bayi-yonlendirme']; ?></div>
									<div><?php echo $textCagir['form']['islemde']; ?></div>
									<div><?php echo $textCagir['form']['degerleme-evrak']; ?></div>
									<div><?php echo $textCagir['icSayfa']['talep-tamamlandi']; ?></div>
								</div>
								<div class="col-8 surecler-mobil-tarihler">
									<div class="active">25 Ocak 2022</div>
									<div>    <?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?></div>
									<div>    <?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?></div>
									<div>    <?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?></div>
								</div>
							</div>
						</div>
						<?php $i++;
					} ?>

			</div>
		</div>
	</div>


</section>

<!--<form action="" method="post">-->
<!--	<input type="hidden" id="surecDetayId" value="--><?php //echo $row['siparisID']; ?><!--">-->
<!--	<input type="hidden" name="methodName" value="bayiSiparis">-->
<!--	<input type="hidden" name="islem" value="siparisDetay">-->
<!--</form>-->
<!--<div class="surecDetayWrapper d-none">-->
<!--	<div class="surecDetay row">-->
<!--		<h3 class="col-12 text-center surecDetay-title">Siparis ID: --><?php //echo $row['siparisID']; ?><!--</h3>-->
<!--		<div class="col-6 text-center surecDetay-item">-->
<!--			--><?php //echo $row['hizmetBaslik']; ?><!-- <br> --><?php //echo $row['urunBaslik']; ?>
<!--		</div>-->
<!--		<div class="col-6 text-center surecDetay-item">-->
<!--			--><?php //echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
<!--			- --><?php //echo substr($row['kayitTarih'], 11, 5); ?>
<!--		</div>-->
<!---->
<!--	</div>-->
<!--</div>-->

<!---->
<!--<div class="dosyaYuklemeWrapper d-none">-->
<!--	<div class="dosyaYukleme">-->
<!--		<form action="" method="post" class="row dosyayuklemeform" enctype="multipart/form-data">-->
<!--			<input type="hidden" id="siparisID" name="siparisID" value="-1">-->
<!--			<input type="hidden" name="methodName" value="bayiSiparis">-->
<!--			<input type="hidden" name="islem" value="dosyaYukle">-->
<!--			<div class="yuklemealani">-->
<!--				<div class="uploadIcon">-->
<!--					<i class="far fa-upload"></i>-->
<!--				</div>-->
<!--				<div>Lütfen Yüklemek istediğiniz dosyaları seçin</div>-->
<!--				<label class="custom-file-upload">-->
<!--					<div>Dosya Seç</div>-->
<!--					<input type="file" multiple id="file" name="dosya" class="d-none">-->
<!--				</label>-->
<!--			</div>-->
<!--			<div> <input type="text" name="aciklama"> </div>-->
<!--			<div class="col-12 filename"></div>-->
<!--			<button type="submit" class="col-12">Gönder</button>-->
<!--		</form>-->
<!--	</div>-->
<!--</div>-->