<?php
	include_once("controller/hizmetController.php");
	$hizmetController = new hizmetController($db);
	$hizmetListesi = $hizmetController->hizmetKategori();


	if (isset($_SESSION['hbt_login'])) {

		if ($_SESSION['hbt_seviye'] == "1") {
			$siparisListesi = $siparisController->siparisListesi('2', $_SESSION['hbt_kullaniciID']);

		} else {
			echo $utility->yonlendir('./');
		}
	} else {
		echo $utility->yonlendir('kullanici-giris');
	}


?>

<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['menu']['basvuru-yap']; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="basvuru-yap"><?php echo $textCagir['icSayfa']['basvuru-sayfasi'];
								?></a></li>
						<li><?php echo $textCagir['menu']['basvuru-yap']; ?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mx-auto" style="position: relative;max-width: 1140px;">
	<div class="ic-kisim ">
		<div class="ic-kisim-header ">

			<div class="basvuru-header basvuru-header-active basvuru-phone-header-active">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper ">
						<i class="far fa-cogs"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['hizmetini-sec']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-file-invoice"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-credit-card"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['odeme-yap']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-thumbs-up"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['talep-gor']; ?></div>
			</div>

		</div>
	</div>
	<div class="basvuru-hizmetler" style="position: relative;">
		<div class="basvuru-hizmetler-mainButton-wrapper ">
			<?php
				foreach ($hizmetListesi as $item) {

					?>
					<button class="basvuru-hizmetler-mainButton-item <?php echo $item['tag']; ?>  col-md-4">
						<div class="basvuru-hizmetler-mainButton-inner-wrapper justify-content-lg-between justify-content-xl-between">
							<img src="../assets/img/basvuru-<?php echo $item['tag']; ?>-icon.svg" class="d-md-none">
							<span class="p-md-0 text-md-center"><?php echo $item['baslik']; ?> </span>
						</div>
						<div class="triangle"></div>
					</button>
				<?php } ?>


		</div>
		<?php
			foreach ($hizmetListesi as $item) {

				?>
				<div class="bavsuru-hizmetler-altButton-wrapper <?php echo $item['tag']; ?> opacity-0 d-none flex-md-column flex-lg-row  align-items-md-start  justify-content-lg-between justify-content-xl-between flex-wrap mt-5">
					<?php $i = 1;
						foreach ($item['urunler'] as $urunDetay) { ?>
							<?php if ($urunDetay['diger'] == 0): ?>
								<button class="bavsuru-hizmetler-altButton-item d-flex flex-column align-items-center"
								        value="<?php echo $urunDetay['hizmet_detayID']; ?>"
								        onclick="hizmetlerValue(this)">
									<div class="number "><?php echo $i; ?></div>
									<div class="option">
										<?php echo $urunDetay['baslik']; ?> <i class="far fa-check"></i>
									</div>
								</button>
							<?php elseif ($urunDetay['diger'] == 1): ?>
								<label for="" class="d-block text-left"><?php echo $textCagir['hizmet']['mesaj']; ?> </label>
								<textarea name="mesaj" class="w-100 d-block" id="hizmetDigerMesaj" value="<?php echo
								$urunDetay['hizmet_detayID']; ?>"></textarea>
							<?php endif; ?>
							<?php $i++;
						} ?>

				</div>
			<?php } ?>


		<form method="post" action="bilgilerini-gir">
			<input type="hidden" name="methodName" value="siparis">
			<input type="hidden" name="step" value="baslat">
			<input type="hidden" name="mesaj" value="____" id="mesaj">
			<input type="hidden" name="urunID" value="0" id="urunID">
			<!-- Yukarıda ürün seçiilince otomatik olarka bu inputun value kısmna ürün ıd gelcek.  !-->
			<button type="submit" class="basvuru-hizmetler-send d-block mx-auto ">
				<?php echo $textCagir['hizmet']['devam-et']; ?>
			</button>
		</form>
	</div>
</section>