<?php
include_once("controller/siparisController.php");
if ($_SESSION['hbt_login']) {
	if (isset($_POST['siparisID'])) {

		$siparisData = $siparisController->siparisDetay($_POST['siparisID']);
		$siparisModel = new siparisModel($db);
		$urunData = $siparisModel->urunDetay($siparisData['urunID']);

		if ($siparisData['musteriID'] != $_SESSION['hbt_kullaniciID']) {
			echo $utility->yonlendir('./');
		}
	} else {
		echo $utility->yonlendir('./');
	}
} else {
	echo $utility->yonlendir('./');
}


?>


<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $textCagir['icSayfa']['siparis-duzenle']; ?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
                        <li>
                            <a title="<?php echo $textCagir['menu']['kullanici']; ?>" href="kullanici-basvuru-takip"><?php echo $textCagir['menu']['kullanici']; ?></a>
                        </li>
                        <li><?php echo $textCagir['icSayfa']['siparis-duzenle']; ?></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bayi px-md-5 mx-lg-auto container">



	<?php include("kullanici_profil_header.php"); ?>
    <div class="position-relative" style="top:0px">
        <div class="parsel-bilgileri  col-lg-5 col-md-8 col-12 mx-auto" style="top: 50px;">
            <div class="parsel-bilgileri-header d-flex justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="25.786" height="16.211"
                     viewBox="0 0 25.786 16.211">
                    <g id="Group_1199" data-name="Group 1199" transform="translate(-7229.27 -580.777)">
                        <g id="Group_1198" data-name="Group 1198">
                            <path id="Path_3216" data-name="Path 3216"
                                  d="M7240.635,580.777c.634.233,1.265.475,1.9.7,1.227.431,2.459.851,3.688,1.276.075.026.146.062.156.066-.788.82-1.569,1.666-2.385,2.476-2.277,2.261-4.573,4.5-6.851,6.763a.387.387,0,0,1-.538.074q-3.664-1.837-7.337-3.659v-.07a3.337,3.337,0,0,0,.333-.179q3.491-2.376,6.978-4.757,1.961-1.34,3.913-2.688Z"
                                  fill="#008a83"/>
                            <path id="Path_3217" data-name="Path 3217"
                                  d="M7255.056,586.117c-.518.632-1.045,1.257-1.555,1.9-1.653,2.076-3.3,4.157-4.979,6.274l-6.011-4.844c.3-.315.587-.619.879-.918,1.519-1.556,3.043-3.106,4.554-4.669a.489.489,0,0,1,.607-.141c2.1.747,4.2,1.479,6.3,2.214a1.944,1.944,0,0,0,.2.047Z"
                                  fill="#008a83"/>
                            <path id="Path_3218" data-name="Path 3218"
                                  d="M7246.3,596.988c-.681-.355-1.358-.719-2.045-1.064q-2.655-1.336-5.316-2.66c-.062-.031-.121-.068-.212-.12,1.147-1.117,1.669-1.608,2.821-2.729l6.032,4.991c-.614.74-.606.856-1.209,1.582Z"
                                  fill="#008a83"/>
                        </g>
                    </g>
                </svg>
                <h6 class="ml-2"><?php echo $textCagir['icSayfa']['siparis-duzenle']; ?></h6>
            </div>
            <div class="parsel-bilgileri-sekıl d-flex align-items-center">
                <div class="cubuk"></div>
                <div class="yuvarlak"></div>
                <div class="cubuk"></div>
            </div>
            <form class="parsel-bilgileri-form col-12" action="siparis-duzenle" method="post">
x
                <input name="il" class="col-12" type="text" value="<?php echo $siparisData['il']; ?>">
                <input name="ilce" class="col-12" type="text" value="<?php echo $siparisData['ilce']; ?>">
                <input name="mahalle" class="col-12" type="text" value="<?php echo $siparisData['mahalle']; ?>">
                <input name="ada" class="col-12" type="text" value="<?php echo $siparisData['ada']; ?>">
                <input name="parsel" class="col-12" type="text" value="<?php echo $siparisData['parsel']; ?>">
                <textarea name="aciklama" class="col-12" id="" cols="20" rows="4"><?php echo $siparisData['aciklama']; ?></textarea>
	          
		            <input type="hidden" name="siparisID" value="<?php echo $_POST['siparisID']; ?>">
		            <input type="hidden" name="methodName" value="siparisDuzenle">
                <button type="submit" class="parsel-bilgileri-buton col-12"><?php echo $textCagir['icSayfa']['kaydet']; ?></button>
            </form>
        </div>
    </div>
</section>