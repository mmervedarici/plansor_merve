<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $textCagir['menu']['sozlesmeler'];?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
                        <li><?php echo $textCagir['menu']['sozlesmeler'];?></li>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="circle1 icSayfa"></div>
                <div class="circle2 icSayfa"></div>
                <div class="cizgi icSayfa"></div>
                <div class="ucgen icSayfa"></div>
            </div>
            <div class="col-12 my-4">

                <a class="sozlesmeDis" href="belgeyolu">
                    <div class="download-popup ml-2 text-center rounded font-weight-bold d-inline-block">
                        <div class="text px-2 py-1"><?php echo $textCagir['icSayfa']['indir'];?></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-9 d-flex align-items-center">
                            <div class="sozlesmeBtn-download"> <i class="far fa-download "></i> </div>
                            <div class="sozlesmeBaslik ml-3">
	                            <?php echo $textCagir['icSayfa']['sozlesme-baslik'];?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 my-4">
                <a class="sozlesmeDis" href="belgeyolu">
                    <div class="download-popup ml-2  text-center rounded font-weight-bold d-inline-block">
                        <div class="text px-2 py-1"><?php echo $textCagir['icSayfa']['indir'];?></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-9 d-flex align-items-center">
                            <div class="sozlesmeBtn-download"> <i class="far fa-download "></i> </div>

                            <div class="sozlesmeBaslik ml-3">
	                            <?php echo $textCagir['icSayfa']['sozlesme-baslik'];?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 my-4">
                <a class="sozlesmeDis" href="belgeyolu">
                    <div class="download-popup ml-2  text-center rounded font-weight-bold d-inline-block">
                        <div class="text px-2 py-1"><?php echo $textCagir['icSayfa']['indir'];?></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-9 d-flex align-items-center">
                            <div class="sozlesmeBtn-download"> <i class="far fa-download "></i> </div>

                            <div class="sozlesmeBaslik ml-3">
	                            <?php echo $textCagir['icSayfa']['sozlesme-baslik'];?>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
            <div class="col-12 my-4">
                <a class="sozlesmeDis" href="belgeyolu">
                    <div class="download-popup ml-2  text-center rounded font-weight-bold d-inline-block">
                        <div class="text px-2 py-1"><?php echo $textCagir['icSayfa']['indir'];?></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-9 d-flex align-items-center">
                            <div class="sozlesmeBtn-download"> <i class="far fa-download "></i> </div>
                            <div class="sozlesmeBaslik ml-3">
	                            <?php echo $textCagir['icSayfa']['sozlesme-baslik'];?>
                            </div>
                        </div>

                    </div>
                </a>
            </div>
            <div class="col-12 my-3">
                <a class="sozlesmeDis" href="belgeyolu">
                    <div class="download-popup ml-2  text-center rounded font-weight-bold d-inline-block">
                        <div class="text px-2 py-1"><?php echo $textCagir['menu']['tapu'];?></div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-9 d-flex align-items-center">
                            <div class="sozlesmeBtn-download"> <i class="far fa-download "></i>
                            </div>
                            <div class="sozlesmeBaslik ml-3">
	                            <?php echo $textCagir['icSayfa']['sozlesme-baslik'];?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>