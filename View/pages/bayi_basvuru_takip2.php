<?php

	if (isset($_SESSION['hbt_login'])) {

		if ($_SESSION['hbt_seviye'] == "1") {
			$siparisListesi = $siparisController->siparisListesi('null', $_SESSION['hbt_kullaniciID']);
			$siparisList = $siparisController->siparisList('$siparisID');
		} else {
			echo $utility->yonlendir('basvuru-yap');
		}
	} else {
		echo $utility->yonlendir('./');
	}


?>
	<section>
		<div class="container my-5 pt-lg-4">
			<div class="row">
				<div class="col-12">
					<div class="icCircle1"></div>
					<div class="icCircle2"></div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="breadBaslik">
						<h1><?php echo $textCagir['form']['basvuru-takip']; ?></h1>
					</div>
				</div>
				<div class="col-lg-6"></div>
				<div class="col-lg-6"></div>
				<div class="col-12 col-lg-6">
					<div class="breadCrumbLinkDis">
						<div class="breadCrumbLink">
							<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
							<li><?php echo $textCagir['form']['basvuru-takip']; ?></li>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="mx-auto container">
		<?php include "kullanici_profil_header.php"; ?>
	</section>

	<section class="mt-5">
		<div class="row no-gutters justify-content-center">
			<div class="col-2 col">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical"
				     style="border-left: 1px solid #028b83;border-top: 1px solid #028b83;border-bottom: 1px solid #028b83;">
					<a class="nav-link active" id="v-pills-onaybekleyenler-tab" data-toggle="pill"
					   href="#v-pills-onaybekleyenler" role="tab" aria-controls="v-pills-onaybekleyenler"
					   aria-selected="true"><?php echo $textCagir['form']['onay-bekleyen']; ?></a>
					<a class="nav-link" id="v-pills-islemdekiler-tab" data-toggle="pill" href="#v-pills-islemdekiler"
					   role="tab" aria-controls="v-pills-islemdekiler"
					   aria-selected="false"><?php echo $textCagir['form']['islemdekiler']; ?></a>
					<a class="nav-link" id="v-pills-tamamlandi-tab" data-toggle="pill" href="#v-pills-tamamlandi"
					   role="tab" aria-controls="v-pills-tamamlandi"
					   aria-selected="false"><?php echo $textCagir['form']['tamamlandi']; ?></a>
				</div>
			</div>
			<div class="col-9">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-onaybekleyenler" role="tabpanel"
					     aria-labelledby="v-pills-onaybekleyenler-tab">

						<!----------------- ONAY BEKLEYENLER --------------->
						<table class="table table-hover bg-white f-size-12">
							<thead>
							<tr class="table-secondary">
								<th scope="col">
									<img src="../assets/img/peopleicon.svg" alt=""><br>
									<?php echo $textCagir['form']['siparis-no']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/talepbilgileri-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-bilgileri']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/taleptarihi-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-tarih']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/siparisdurumu.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['siparis-durum']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/islemler-icon.svg" alt=""><br>
									<?php echo $textCagir['menu']['islemler']; ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($siparisListesi['onayBekleyenler'] as $row) { ?>
								<tr>
									<td vert>#<?php echo $row['siparisID']; ?></td>
									<td><?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?></td>
									<td><?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
										<br/>
										<?php echo substr($row['kayitTarih'], 11, 5); ?></td>
									<td>
										<button class="w-75 btn btn-sm onaybekliyor text-center"><?php echo $textCagir['form']['onay-bekleyen']; ?></button>
									</td>
									<td>
										<div class="row no-gutters padding0">
											<div class="col">
												<form method="post" action="siparis-duzenle">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm" data-toggle="modal">
														<i class="fa fa-pencil text-warning"></i><br>
														<?php echo $textCagir['icSayfa']['siparis-duzenle']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<input type="hidden" name="methodName" value="kullaniciSiparis">
													<input type="hidden" name="islem" value="ret">
													<button class="btn btn-sm" data-toggle="modal">
														<i class="fa fa-window-close text-danger"></i><br>
														<?php echo $textCagir['icSayfa']['siparis-iptal']; ?>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
						<!----------------- ONAY BEKLEYENLER --------------->
					</div>


					<div class="tab-pane fade" id="v-pills-islemdekiler" role="tabpanel"
					     aria-labelledby="v-pills-islemdekiler-tab">
						<!----------------- İŞLEMDEKİLER --------------->
						<table class="table table-hover bg-white f-size-12">
							<thead>
							<tr class="table-secondary">
								<th scope="col">
									<img src="../assets/img/peopleicon.svg" alt=""><br>
									<?php echo $textCagir['form']['siparis-no']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/talepbilgileri-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-bilgileri']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/taleptarihi-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-tarih']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/siparisdurumu.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['siparis-durum']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/islemler-icon.svg" alt=""><br>
									<?php echo $textCagir['menu']['islemler']; ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($siparisListesi['islemdekiler'] as $row) {
								if ($row['durum'] == 1) {
									$text = $textCagir['form']['bayi-onay'];
									$class = "isbirlikciyeyonlendirildi text-center";
								}
								if ($row['durum'] == 2) {
									$text = $textCagir['form']['islemde'];
									$class = "islemde text-center";
								}
								if ($row['durum'] == 3) {
									$text = $textCagir['form']['degerleme-evrak'];
									$class = "degerlemeevraklari text-center";
								}
								?>
								<tr>
									<td>#<?php echo $row['siparisID']; ?></td>
									<td><?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?></td>
									<td><?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
										<br/>
										<?php echo substr($row['kayitTarih'], 11, 5); ?></td>
									<td>
										<button class="w-75 btn btn-sm <?php echo $class; ?> text-center"><?php echo $text; ?></button>
									</td>
									<td>

										<div class="row no-gutters padding0">
											<div class="col">


												<button class="btn btn-sm" data-toggle="modal"
												        data-target="#exampleModal-<?php echo $row['siparisID']; ?>">
													<i class="fa fa-calendar-alt"></i><br>
													<?php echo $textCagir['anaSayfa']['surecler']; ?>
												</button>

											</div>

											<div class="modal fade " id="exampleModal-<?php echo $row['siparisID']; ?>"
											     tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">Sipariş No:
																#<?php echo $row['siparisID']; ?></h5>
															<button type="button" class="close" data-dismiss="modal"
															        aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<table class="table table-warning">
																<thead>
																<tr>
																	<th scope="col"><?php echo $textCagir['form']['sira-no']; ?></th>
																	<th scope="col"><?php echo $textCagir['form']['islem-adi']; ?></th>
																	<th scope="col"><?php echo $textCagir['form']['islem-tarihi']; ?></th>
																</tr>
																</thead>
																<tbody>
																<?php foreach ($siparisList as $item) :{
																	if ($item['durum'] == 0) {
																		$text = $textCagir['form']['onay-bekliyor'];
																	}
																	if ($item['durum'] == 1) {
																		$text = $textCagir['form']['bayi-onay'];
																	}
																	if ($item['durum'] == 2) {
																		$text = $textCagir['form']['islemde'];
																	}
																	if ($item['durum'] == 3) {
																		$text = $textCagir['form']['degerlendirmede'];
																	}
																	if ($item['durum'] == 4) {
																		$text = $textCagir['form']['tamamlandi'];
																	}
																	if ($item['durum'] == 5) {
																		$text = $textCagir['form']['firma-red'];
																	}
																	if ($item['durum'] == 6) {
																		$text = $textCagir['form']['musteri-iptal'];
																	}
																	?>
																	<tr>
																		<th scope="row"><?php echo $item['siparisID']; ?></th>
																		<td><?php echo $text; ?>
																		</td>
																		<td><?php echo $utility->tarihYazdir(substr
																			($item['kayitTarih'], 0, 10)); ?></td>
																	</tr>
																<?php } endforeach; ?>

																</tbody>
															</table>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary"
															        data-dismiss="modal">Kapat
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-info-square"></i><br>
														<?php echo $textCagir['form']['incele']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post" action="" enctype="multipart/form-data">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<input type="hidden" name="islem" value="dosyaYukle">
													<button class="btn btn-sm">
														<i class="fa fa-file"></i><br>
														<?php echo $textCagir['form']['dosya']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-comment-alt"></i><br>
														<?php echo $textCagir['hizmet']['mesaj']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-check text-info"></i><br>
														<?php echo $textCagir['form']['tamamla']; ?>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
								<?php
							} ?>
							</tbody>
						</table>
						<!----------------- İŞLEMDEKİLER --------------->
					</div>


					<div class="tab-pane fade" id="v-pills-tamamlandi" role="tabpanel"
					     aria-labelledby="v-pills-tamamlandi-tab">
						<!----------------- TAMAMLANDI --------------->
						<table class="table table-hover bg-white f-size-12">
							<thead>
							<tr class="table-secondary">
								<th scope="col">
									<img src="../assets/img/peopleicon.svg" alt=""><br>
									<?php echo $textCagir['form']['siparis-no']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/talepbilgileri-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-bilgileri']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/taleptarihi-icon.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['talep-tarih']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/siparisdurumu.svg" alt=""><br>
									<?php echo $textCagir['icSayfa']['siparis-durum']; ?>
								</th>
								<th scope="col">
									<img src="../assets/img/islemler-icon.svg" alt=""><br>
									<?php echo $textCagir['menu']['islemler']; ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($siparisListesi['tamamlandi'] as $row) {
								if ($row['durum'] == 4) {
									$text = $textCagir['form']['tamamlandi'];
									$class = "tamamlandi text-center";
								}
								if ($row['durum'] == 5) {
									$text = $textCagir['form']['firma-red'];
									$class = "bayiiptal text-center";
								}
								if ($row['durum'] == 6) {
									$text = $textCagir['form']['musteri-iptal'];
									$class = "bayiiptal text-center";
								}
								?>
								<tr>
									<td>#<?php echo $row['siparisID']; ?></td>
									<td><?php echo $row['hizmetBaslik']; ?> <br> <?php echo $row['urunBaslik']; ?></td>
									<td><?php echo $utility->tarihYazdir(substr($row['kayitTarih'], 0, 10)); ?>
										<br/>
										<?php echo substr($row['kayitTarih'], 11, 5); ?></td>
									<td>
										<button class="w-75 btn btn-sm <?php echo $class; ?> text-center"><?php echo $text; ?></button>
									</td>
									<td>

										<div class="row no-gutters padding0">
											<div class="col">
												<button class="btn btn-sm" data-toggle="modal"
												        data-target="#exampleModal-<?php echo $row['siparisID']; ?>">
													<i class="fa fa-calendar-alt"></i><br>
													<?php echo $textCagir['anaSayfa']['surecler']; ?>
												</button>
											</div>

											<div class="modal fade " id="exampleModal-<?php echo $row['siparisID']; ?>"
											     tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">Sipariş No:
																#<?php echo $row['siparisID']; ?></h5>
															<button type="button" class="close" data-dismiss="modal"
															        aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<table class="table table-warning">
																<thead>
																<tr>
																	<th scope="col"><?php echo $textCagir['form']['sira-no']; ?></th>
																	<th scope="col"><?php echo $textCagir['form']['islem-adi']; ?></th>
																	<th scope="col"><?php echo $textCagir['form']['islem-tarihi']; ?></th>
																</tr>
																</thead>
																<tbody>
																<tr>

																	<th scope="row">1</th>
																	<td>Sipariş Verildi</td>
																	<td>08.04.2022</td>
																</tr>
																<tr>
																	<th scope="row">2</th>
																	<td>Çözüm Ortağına Yönlendirildi</td>
																	<td>08.04.2022</td>
																</tr>
																<tr>
																	<th scope="row">3</th>
																	<td>Dosyalar iletildi</td>
																	<td>08.04.2022</td>
																</tr>
																</tbody>
															</table>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary"
															        data-dismiss="modal">Kapat
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-info-square"></i><br>
														<?php echo $textCagir['form']['incele']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-file"></i><br>
														<?php echo $textCagir['form']['dosya']; ?>
													</button>
												</form>
											</div>
											<div class="col">
												<form method="post" action="siparis-detay">
													<input type="hidden" name="siparisID"
													       value="<?php echo $row['siparisID']; ?>">
													<button class="btn btn-sm">
														<i class="fa fa-comment-alt"></i><br>
														<?php echo $textCagir['hizmet']['mesaj']; ?>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
						<!----------------- TAMAMLANDI --------------->
					</div>
				</div>
			</div>
		</div>
	</section>


<?php //foreach ($siparisList as $row) {
//
//	$surecData[$row['surecID']]['surecID'] = $row['surecID'];
//	$surecData[$row['surecID']]['siparisID'] = $row['siparisID'];
//	$surecData[$row['surecID']]['bayiID'] = $row['bayiID'];
//	$surecData[$row['surecID']]['kayitTarih'] = $row['kayitTarih'];
//
////	while ($row = mysqli_fetch_assoc($sorgu)) {
////
////
////		//print_r($ogrenciData);
////		//
////	}
//}
//	print("<pre>" . print_r($row['surecID'], true) . "</pre>");
//?>