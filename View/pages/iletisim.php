<section>
    <div class="container my-5 pt-lg-4">
        <div class="row">
            <div class="col-12">
                <div class="icCircle1"></div>
                <div class="icCircle2"></div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="breadBaslik">
                    <h1><?php echo $textCagir['menu']['iletisim']; ?></h1>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-12 col-lg-6">
                <div class="breadCrumbLinkDis">
                    <div class="breadCrumbLink">
                        <a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
                        <li><?php echo $textCagir['menu']['iletisim-form']; ?></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 mt-5 iletCizgi">
                <a class="iletisimDis" href="tel:+9<?php echo $siteBilgi['data']["sabitTelefon"]; ?>">
                    <div class="row no-gutters my-3">
                        <div class="col-12">
                            <div class="iletisimIcon">
                                <i class="fas fa-headphones-alt"></i>
                            </div>
                            <div class="iletisimText">
                                <?php echo $siteBilgi['data']["sabitTelefon"]; ?>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="iletisimDis" href="tel:+9<?php echo $siteBilgi['data']["telefon"]; ?>">
                    <div class="row no-gutters my-3">
                        <div class="col-12">
                            <div class="iletisimIcon">
                                <i class="fas fa-mobile-android"></i>
                            </div>
                            <div class="iletisimText">
                                <?php echo $siteBilgi['data']["telefon"]; ?>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="iletisimDis" href="mailto:<?php echo $siteBilgi['data']["eposta"]; ?>">
                    <div class="row no-gutters my-3">
                        <div class="col-12">
                            <div class="iletisimIcon">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="iletisimText">
                                <?php echo $siteBilgi['data']["eposta"]; ?>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="iletisimDis" href="tel">
                    <div class="row no-gutters my-3">
                        <div class="col-12">
                            <div class="iletisimIcon">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="iletisimText">
                                <?php echo $siteBilgi['data']["adres"]; ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-lg-8 text-right">
                <img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>/../assets/img/katmanlar/iletisim-1.png"
                     alt="iletisim">
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row my-5">
            <div class="col-12 col-lg-3 ortalaX">
                <div class="plansorBaslik">
                    <h4><?php echo $textCagir['menu']['iletisim-form']; ?></h4>
                </div>
            </div>
            <div class="col-12 mt-4">
                <form class="w-100"  method="POST">
                    <div class="row">
                        <div class="col-12 col-lg-4 my-2">
                            <span class="input input--kohana">
                                <input class="input__field input__field--kohana" name="adSoyad" type="text"
                                       id="input-29" placeholder="<?php echo $textCagir['menu']['ad-soyad']; ?>"
                                       required="">
                                <label class="input__label input__label--kohana" for="input-29">
                                    <i class="far fa-fw fa-user-alt icon icon--kohana"></i>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-lg-4 my-2">
                            <span class="input input--kohana">
                                <input class="input__field input__field--kohana" name="telefon" type="number"
                                       id="input-30" placeholder="<?php echo $textCagir['form']['telefon']; ?>"
                                       required="">
                                <label class="input__label input__label--kohana" for="input-30">
                                    <i class="far fa-fw fa-phone-alt icon icon--kohana"></i>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-lg-4 my-2">
                            <span class="input input--kohana">
                                <input class="input__field input__field--kohana" name="eposta" type="email"
                                       id="input-31" placeholder="<?php echo $textCagir['form']['eposta']; ?>"
                                       required="">
                                <label class="input__label input__label--kohana" for="input-31">
                                    <i class="far fa-fw fa-envelope icon icon--kohana"></i>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 my-2">
                            <span class="input input--kohana">
                                <textarea class="input__field input__field--kohana iletisimMesaj" name="mesaj"
                                          type="text" id="input-32" rows="6" placeholder="<?php echo
                                $textCagir['hizmet']['mesaj']; ?>" required=""></textarea>
                                <label class="input__label input__label--kohana" for="input-32">
                                    <i class="far fa-fw fa-pen icon icon--kohana"></i>

                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-lg-6 text-right text-lg-left">
                            <div class="g-recaptcha" data-sitekey="6Lel4Z4UAAAAAOa8LO1Q9mqKRUiMYl_00o5mXJrR"></div>
                        </div>
                        <input type="hidden" name="methodName" value="iletisimFormu">
                        <div class="col-12 col-lg-6 my-3 text-right">
                            <button class="site-button" type="submit" id="gonder">
                                <span><?php echo $textCagir['form']['gonder']; ?></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
