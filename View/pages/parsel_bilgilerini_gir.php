<?php
	if (isset($_SESSION['step'])) {
		if(!isset($_SESSION['hbt_login'])){
			echo $utility->yonlendir('bilgilerini-gir');
		}
		if($_SESSION['step'] == "odeme"){
			echo $utility->yonlendir('odeme-yap');
		}
		if($_SESSION['step'] == "arsiv"){
			echo $utility->yonlendir('basvuru-yap');
		}

	} else {
		echo $utility->yonlendir('basvuru-yap');
	}

if ($_SESSION['hbt_seviye'] == 2) {
	echo $utility->yonlendir('bayi-basvuru-takip');
}
else {
?>

<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['menu']['basvuru-yap'];?> </h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="./"><?php echo $textCagir['icSayfa']['basvuru-sayfasi'];?>
							</a></li>
						<li><?php echo $textCagir['menu']['basvuru-yap'];?> </li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mx-auto" style="position: relative;max-width: 1140px;">
	<div class="ic-kisim ">
		<div class="ic-kisim-header ">

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper ">
						<i class="far fa-cogs"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['hizmetini-sec'] ; ?></div>
			</div>

			<div class="basvuru-header  basvuru-header-active basvuru-phone-header-active">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-file-invoice"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['bilgilerini-gir'] ; ?></div>
			</div>

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-credit-card"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['odeme-yap'] ; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-thumbs-up"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['talep-gor'] ; ?></div>
			</div>

		</div>
	</div>



	<div class="position-relative" style="top:100px">
		<div class="siparisDetay-header w-100 mt-5">
			<div class="siparisDetay-header-ust">
				<div class="siparisDetay-header-ust-baslik d-flex mb-0 align-items-center justify-content-center">
					<img src="../assets/img/note.svg" alt="">
					<h3><?php echo $textCagir['icSayfa']['siparis-detay']; ?></h3>
				</div>
				<div class="siparisDetay-header-ust-baslik-line mt-2 d-flex align-items-center">
					<div class="line"></div>
					<div class="circle"></div>
					<div class="line"></div>
				</div>
			</div>
			<div class="siparisDetay-header-alt w-100 py-3 px-4 d-lg-flex text-center mt-4 justify-content-between">
				<div class="siparis-aciklama-section text-center">
					<?php echo $_SESSION['urun']['hizmetBaslik'] ; ?>
				</div>
				<div class="cızgıler"><img src="../assets/img/arrow-01.svg" width="40" height="40" alt="">
				</div>
				<div class="siparis-aciklama-section text-center">
					<?php echo $_SESSION['urun']['urunBaslik'] ; ?>
				</div>

			</div>
		</div>
		<div class="parsel-bilgileri  col-lg-5 col-md-8 col-12 mx-auto" style="top: 50px;">
			<div class="parsel-bilgileri-header d-flex justify-content-center">
				<svg xmlns="http://www.w3.org/2000/svg" width="25.786" height="16.211"
				     viewBox="0 0 25.786 16.211">
					<g id="Group_1199" data-name="Group 1199" transform="translate(-7229.27 -580.777)">
						<g id="Group_1198" data-name="Group 1198">
							<path id="Path_3216" data-name="Path 3216"
							      d="M7240.635,580.777c.634.233,1.265.475,1.9.7,1.227.431,2.459.851,3.688,1.276.075.026.146.062.156.066-.788.82-1.569,1.666-2.385,2.476-2.277,2.261-4.573,4.5-6.851,6.763a.387.387,0,0,1-.538.074q-3.664-1.837-7.337-3.659v-.07a3.337,3.337,0,0,0,.333-.179q3.491-2.376,6.978-4.757,1.961-1.34,3.913-2.688Z"
							      fill="#008a83" />
							<path id="Path_3217" data-name="Path 3217"
							      d="M7255.056,586.117c-.518.632-1.045,1.257-1.555,1.9-1.653,2.076-3.3,4.157-4.979,6.274l-6.011-4.844c.3-.315.587-.619.879-.918,1.519-1.556,3.043-3.106,4.554-4.669a.489.489,0,0,1,.607-.141c2.1.747,4.2,1.479,6.3,2.214a1.944,1.944,0,0,0,.2.047Z"
							      fill="#008a83" />
							<path id="Path_3218" data-name="Path 3218"
							      d="M7246.3,596.988c-.681-.355-1.358-.719-2.045-1.064q-2.655-1.336-5.316-2.66c-.062-.031-.121-.068-.212-.12,1.147-1.117,1.669-1.608,2.821-2.729l6.032,4.991c-.614.74-.606.856-1.209,1.582Z"
							      fill="#008a83" />
						</g>
					</g>
				</svg>
				<h6 class="ml-2"><?php echo $textCagir['icSayfa']['parsel-bilgileri'];?></h6>
			</div>
			<div class="parsel-bilgileri-sekıl d-flex align-items-center">
				<div class="cubuk"></div>
				<div class="yuvarlak"></div>
				<div class="cubuk"></div>
			</div>
			<form class="parsel-bilgileri-form col-12" action="odeme-yap" method="post">
				<input name="il" class="col-12" type="text" placeholder="<?php echo $textCagir['form']['il'];?>">
				<input name="ilce" class="col-12" type="text" placeholder="<?php echo $textCagir['form']['ilce'];?>">
				<input name="mahalle" class="col-12" type="text" placeholder="<?php echo $textCagir['form']['mahalle'];?>">
				<input name="ada" class="col-12" type="text" placeholder="<?php echo $textCagir['form']['ada'];?>">
				<input name="parsel" class="col-12" type="text" placeholder="<?php echo $textCagir['form']['parsel'];?>">
				<textarea name="aciklama" class="col-12" id="" cols="20" rows="4" placeholder="<?php echo $textCagir['form']['aciklama'];?>"></textarea>
				<input type="hidden" name="methodName" value="siparis">
				<input type="hidden" name="step" value="odemeEkrani">
				<input type="hidden" name="islem" value="onKayit">
				<button type="submit" class="parsel-bilgileri-buton col-12"><?php echo $textCagir['hizmet']['devam-et'];?></button>
			</form>

		</div>
	</div>


</section>


<?php } ?>