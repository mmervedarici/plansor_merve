<?php
	if (isset($_SESSION['step'])) {
		if (!isset($_SESSION['hbt_login'])) {
			echo $utility->yonlendir('bilgilerini-gir');
		}
		if ($_SESSION['step'] == "odeme") {

		} else {
			echo $utility->yonlendir('parsel-bilgilerini-gir');
		}

	} else {
		echo $utility->yonlendir('basvuru-yap');
	}

?>
<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['menu']['basvuru-yap']; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="./"><?php echo $textCagir['icSayfa']['basvuru-sayfasi'];
								?></a></li>
						<li><?php echo $textCagir['menu']['basvuru-yap']; ?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mx-auto" style="position: relative;max-width: 1140px;">
	<div class="ic-kisim ">

		<div class="ic-kisim-header ">

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper ">
						<i class="far fa-cogs"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['hizmetini-sec']; ?></div>
			</div>

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-file-invoice"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></div>
			</div>

			<div class="basvuru-header basvuru-header-active basvuru-phone-header-active">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-credit-card"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['odeme-yap']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-thumbs-up"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['talep-gor']; ?></div>
			</div>

		</div>
	</div>
	<div class="siparisDetay" style="position: relative; top: 150px; margin-bottom: 150px;">
		<div class="siparisDetay-header w-100">
			<div class="siparisDetay-header-ust">
				<div
						class="siparisDetay-header-ust-baslık d-flex mb-0 align-items-center justify-content-center">
					<img src="<?php echo $siteBilgi['siteURL']; ?>/./assets/img/note.svg" alt="">
					<h3><?php echo $textCagir['icSayfa']['siparis-detay']; ?></h3>
				</div>
				<div class="siparisDetay-header-ust-baslık-line mt-2 d-flex align-items-center">
					<div class="line"></div>
					<div class="circle"></div>
					<div class="line"></div>
				</div>
			</div>
			<div
					class="siparisDetay-header-alt w-100 py-3 px-4 d-lg-flex text-center mt-4 justify-content-between">
				<div class="siparis-acıklama-section text-center"> <?php echo $_SESSION['urun']['hizmetBaslik']; ?>
				</div>
				<div class="cızgıler"><img src="<?php echo $siteBilgi['siteURL']; ?>assets/img/arrow-01.svg" width="40"
				                           height="40" alt="">
				</div>
				<div class="siparis-acıklama-section text-center"><?php echo $_SESSION['urun']['urunBaslik']; ?>
				</div>
				<div class="cızgıler"><img src="<?php echo $siteBilgi['siteURL']; ?>assets/img/arrow-01.svg" width="40"
				                           height="40" alt="">
				</div>
				<div class="siparis-acıklama-section price-wrapper">
					<div class="price font-weight-bolder"><?php echo $_SESSION['urun']['fiyat']; ?> ₺</div>
				</div>
			</div>
		</div>

		<div class="mt-5 w-100 d-flex">
			<div class="w-50 w-md-100">
				<div class="siparisDetay-header w-100">
					<div class="siparisDetay-header-ust">
						<div
								class="siparisDetay-header-ust-baslık d-flex mb-0 align-items-center justify-content-center">
							<img src="../assets/img/credit-card.svg" alt="">
							<h3><?php echo $textCagir['icSayfa']['kredi-karti']; ?> /<?php echo
								$textCagir['icSayfa']['banka-karti']; ?></h3>
						</div>
						<div class="siparisDetay-header-ust-baslık-line mt-2 d-flex align-items-center">
							<div class="line"></div>
							<div class="circle"></div>
							<div class="line"></div>
						</div>
					</div>
				</div>
				<form action="" class=" creadit-card mt-2">
					<div class="card-section-group d-lg-flex my-2 align-items-center">
						<label class="d-md-block text-left"
						       for=""><?php echo $textCagir['icSayfa']['kart-no']; ?></label>
						<input type="number" class="card-number w-md-100"
						       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
						       maxlength="16">
					</div>
					<div class="card-section-group my-2 d-lg-flex align-items-center">
						<label class="d-md-block text-left"
						       for=""><?php echo $textCagir['icSayfa']['kart-uzerindeki-isim']; ?></label>
						<input type="text" class="card-owner w-md-100">

					</div>
					<div class="card-section-group my-2 d-lg-flex align-items-center">
						<label class="d-md-block text-left" for="">Son Kullanma Tarihi</label>
						<select name="" id="card-month">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
						<select class="ml-2" name="" id="card-year">

						</select>
					</div>



					<div class="card-section-group d-flex flex-md-column flex-lg-row align-items-lg-center">
						<label class="d-md-block text-left"
						       for=""><?php echo $textCagir['icSayfa']['cvc2']; ?></label>
						<input type="number" class="card-cvc w-md-100"
						       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
						       maxlength="3">
						<span class="ml-lg-4 cvc-number"><?php echo $textCagir['icSayfa']['cvc2-nedir']; ?></span>
						<span class="cvc-aciklama-sekil"><img src="./arrow-01.svg" alt=""></span>
						<span class="cvc-aciklama"><?php echo $textCagir['form']['guvenlik-kodu'];?></span>
					</div>
					<div class="secure mt-4 text-center d-flex align-items-center justify-content-center">
						<div class="unlem">!</div>
						<div class="text"><?php echo $textCagir['icSayfa']['guvenlik-mesaj']; ?></div>
					</div>
				</form>
			</div>


			<div class="creadit-card-onizleme flip-box w-50 pl-2 mt-3 pb-4 d-none d-lg-flex d-xl-flex ">
				<div class="flip-box-inner">
					<div class="flip-box-front">
						<img src="../assets/img/kredi-karti-01.svg">
						<div class="icerik">
							<div class="credit-card-onizleme-no">1234 5678 9012 3456</div>
							<div class="credit-card-onizleme-snt">
								<span class="credit-card-onizleme-sntMonth">06</span>/
								<span class="credit-card-onizleme-sntYear">25</span>
							</div>
							<div class="credit-card-onizleme-owner"><?php echo $textCagir['form']['ad']; ?><?php echo
								$textCagir['form']['soyad']; ?></div>
						</div>
					</div>
					<div class="flip-box-back">
						<div class="cvc-text">353</div>
						<img src="../assets/img/kredi-kartı-arka.svg">
					</div>
				</div>
			</div>
		</div>
		<form method="post" action="basvuru-tamamla">
			<input type="hidden" name="methodName" value="siparis">
			<input type="hidden" name="step" value="tamamlandi">
			<div class="d-md-flex mt-md-5 mb-5 justify-content-md-between pl-md-5 pr-md-5">
				<button class="basvuru-back-button w-sm-100"><a href="parsel-bilgilerini-gir"><?php echo
						$textCagir['icSayfa']['geri-don']; ?></a>
                </button>
				<button type="submit" class="basvuru-hizmetler-send d-block mx-md-auto  w-sm-100"><?php echo
						$textCagir['hizmet']['devam-et']; ?>
				</button>
			</div>
		</form>
	</div>
</section>







