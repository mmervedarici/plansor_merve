<?php
	if (isset($_SESSION)) {

		if ($_SESSION['step'] == "arsiv") {
            $siparisData = $siparisController->siparisDetay($_SESSION['siparis']['siparisID']);
            unset($_SESSION['siparis']);

		} else {
			echo $utility->yonlendir('basvuru-yap');
		}
	}

?>
<section>
	<div class="container my-5 pt-lg-4">
		<div class="row">
			<div class="col-12">
				<div class="icCircle1"></div>
				<div class="icCircle2"></div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="breadBaslik">
					<h1><?php echo $textCagir['menu']['basvuru-yap']; ?></h1>
				</div>
			</div>
			<div class="col-lg-6"></div>
			<div class="col-lg-6"></div>
			<div class="col-12 col-lg-6">
				<div class="breadCrumbLinkDis">
					<div class="breadCrumbLink">
						<a title="Anasayfa" href="./"><i class="fas fa-home"></i></a>
						<li><a title="Haberler" href="basvuru-yap"><?php echo $textCagir['menu']['basvuru-yap'];
								?></a></li>
						<li><?php echo $textCagir['icSayfa']['basvuru-tamamla']; ?></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mx-auto" style="position:relative; max-width:1140px;">
	<div class="ic-kisim ">
		<div class="ic-kisim-header ">

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper ">
						<i class="far fa-cogs"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['hizmetini-sec']; ?></div>
			</div>

			<div class="basvuru-header ">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-file-invoice"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['bilgilerini-gir']; ?></div>
			</div>

			<div class="basvuru-header">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-credit-card"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['odeme-yap']; ?></div>
			</div>

			<div class="basvuru-header basvuru-header-active basvuru-phone-header-active">
				<div class="basvuru-header-cerceve">
					<div class="basvuru-header-icon-wrapper">
						<i class="far fa-thumbs-up"></i>
						<div class="triangle-icon"></div>
					</div>
				</div>
				<div class="yazilar"><?php echo $textCagir['icSayfa']['talep-gor']; ?></div>
			</div>
		</div>
	</div>


	<div class="siparis-ozet col-md-12 mx-auto">
		<div class="siparis d-flex  col-md-3 mx-auto  ">
			<svg xmlns="http://www.w3.org/2000/svg" width="14.785" height="15.303"
			     viewBox="0 0 14.785 15.303">
				<g id="Group_1008" data-name="Group 1008" transform="translate(-11687.901 -587.329)">
					<g id="Group_1007" data-name="Group 1007">
						<path id="Path_2199" data-name="Path 2199"
						      d="M11699.918,598.109c0,1.2.017,2.391-.007,3.585a.965.965,0,0,1-1,.933c-.278.009-.555,0-.833,0h-8.977c-.835,0-1.2-.369-1.2-1.208q0-6.452,0-12.9a1.07,1.07,0,0,1,.775-1.151,1.341,1.341,0,0,1,.334-.036q4.893,0,9.783,0a1.054,1.054,0,0,1,1.126,1.122c.006,1.284,0,2.567,0,3.851a.408.408,0,0,1-.017.178c-.335.613-.676,1.223-1.066,1.818v-5.855h-9.829v13.073h9.82c0-.327.021-.663,0-1a1.735,1.735,0,0,1,.287-1.1c.269-.43.5-.885.75-1.329Z"
						      fill="#008a83"/>
						<path id="Path_2200" data-name="Path 2200"
						      d="M11696.959,600.435c0-.462,0-.866,0-1.27a.377.377,0,0,1,.068-.165q1.875-3.223,3.755-6.446a1.662,1.662,0,0,1,.1-.136l1.075.616c-.054.1-.1.183-.144.264q-1.77,3.062-3.533,6.124a1.305,1.305,0,0,1-.532.543C11697.5,600.1,11697.26,600.255,11696.959,600.435Z"
						      fill="#008a83"/>
						<path id="Path_2201" data-name="Path 2201" d="M11697.883,592.036v.736h-7.934v-.736Z"
						      fill="#008a83"/>
						<path id="Path_2202" data-name="Path 2202" d="M11689.939,596.364v-.738h7.935v.738Z"
						      fill="#008a83"/>
						<path id="Path_2203" data-name="Path 2203" d="M11689.954,590.149H11696v.756h-6.044Z"
						      fill="#008a83"/>
						<path id="Path_2204" data-name="Path 2204" d="M11696.01,593.752v.738h-6.062v-.738Z"
						      fill="#008a83"/>
						<path id="Path_2205" data-name="Path 2205" d="M11689.939,598.081v-.738H11696v.738Z"
						      fill="#008a83"/>
						<path id="Path_2206" data-name="Path 2206"
						      d="M11691.4,599.921a.736.736,0,0,1-.766.745.755.755,0,0,1-.746-.746.776.776,0,0,1,.762-.766A.755.755,0,0,1,11691.4,599.921Z"
						      fill="#008a83"/>
						<path id="Path_2207" data-name="Path 2207"
						      d="M11693.808,599.928a.754.754,0,0,1-.766.753.765.765,0,0,1-.75-.748.773.773,0,0,1,.758-.769A.761.761,0,0,1,11693.808,599.928Z"
						      fill="#008a83"/>
						<path id="Path_2208" data-name="Path 2208"
						      d="M11702.687,591.893l-.423.736-1.1-.63.425-.736Z" fill="#008a83"/>
					</g>
				</g>
			</svg>
			<div class="text-center"><?php echo $textCagir['icSayfa']['siparis-detay']; ?></div>

		</div>
		<div class="talepsekil d-flex  align-items-center">
			<div class="talepcizgi"></div>
			<div class="talepyuvarlak"></div>
			<div class="talepcizgi"></div>
		</div>
		<div class="anabilgiler mt-md-3">
			<div class="bilgiler  col-md-12">
				<div class="bilgibasliklari"><?php echo $textCagir['form']['kisisel-bilgi']; ?></div>
				<div class="sekil d-flex align-items-center justify-content-center ">
					<div class="sekilcizgi"></div>
					<div class="sekilyuvarlak"></div>
					<div class="sekilcizgi"></div>
				</div>
				<div class="text-center genelbilgi ">
					<div class="basliklar col-md-12 d-flex">
						<div><?php echo $textCagir['form']['ad']; ?><?php echo $textCagir['form']['soyad']; ?></div>
						<div><?php echo $textCagir['form']['telefon']; ?></div>
						<div><?php echo $textCagir['form']['eposta']; ?></div>
						<div><?php echo $textCagir['form']['aciklama']; ?></div>
					</div>
					<div class="baslikbilgi col-md-12 d-flex">
						<div> <?php echo $_SESSION['hbt_adSoyad']; ?> </div>
						<div> <?php echo $_SESSION['hbt_telefon']; ?> </div>
						<div><?php echo $_SESSION['hbt_user']; ?></div>
						<div><?php echo $siparisData['aciklama']; ?></div>
					</div>
				</div>
			</div>
			<div class="bilgiler col-md-12">
				<div class="bilgibasliklari"><?php echo $textCagir['icSayfa']['parsel-bilgileri']; ?></div>
				<div class="sekil d-flex align-items-center justify-content-center">
					<div class="sekilcizgi"></div>
					<div class="sekilyuvarlak"></div>
					<div class="sekilcizgi"></div>
				</div>
				<div class="text-center genelbilgi">
					<div class="basliklar col-md-12 d-flex">
						<div><?php echo $textCagir['form']['il']; ?></div>
						<div><?php echo $textCagir['form']['ilce']; ?></div>
						<div><?php echo $textCagir['form']['mahalle']; ?></div>
						<div><?php echo $textCagir['form']['ada']; ?></div>
						<div><?php echo $textCagir['form']['parsel']; ?></div>
					</div>
					<div class="baslikbilgi col-md-12 d-flex">
						<div><?php echo $siparisData['il']; ?></div>
						<div><?php echo $siparisData['ilce']; ?></div>
						<div><?php echo $siparisData['mahalle']; ?></div>
						<div><?php echo $siparisData['ada']; ?></div>
						<div><?php echo $siparisData['parsel']; ?></div>
					</div>
				</div>
			</div>
			<div class="bilgiler col-md-12">
				<div class="bilgibasliklari"><?php echo $textCagir['icSayfa']['hizmet-bilgileri']; ?></div>
				<div class="sekil d-flex align-items-center justify-content-center">
					<div class="sekilcizgi"></div>
					<div class="sekilyuvarlak"></div>
					<div class="sekilcizgi"></div>
				</div>
				<div class="text-center genelbilgi ">
					<div class="basliklar col-md-12 d-flex">
						<div><?php echo $textCagir['icSayfa']['talep-tarih']; ?></div>
						<div><?php echo $textCagir['icSayfa']['talep-saat']; ?></div>
						<div><?php echo $textCagir['form']['hizmet']; ?></div>
						<div><?php echo $textCagir['icSayfa']['hizmet-detay']; ?></div>
						<div><?php echo $textCagir['icSayfa']['fiyat']; ?></div>
					</div>
					<div class="baslikbilgi col-md-12 d-flex">
						<div><?php echo substr($siparisData['kayitTarih'],0,10); ?></div>
						<div><?php echo substr($siparisData['kayitTarih'],11,5); ?></div>
						<div><?php echo $_SESSION['urun']['hizmetBaslik']; ?></div>
						<div><?php echo $_SESSION['urun']['urunBaslik']; ?></div>
						<div style="font-weight: bolder !important; color: #008a83; "> <?php echo $_SESSION['urun']['fiyat']; ?>
							₺
						</div>
					</div>
				</div>
			</div>

		</div>


	</div>
	<div class="d-md-flex mt-md-1 justify-content-center ">
		<button class="basvuru-back-button w-sm-100 mr-md-3 talep-buton"><a
					href="./"><?php echo $textCagir['menu']['anaSayfa-don']; ?></a>
		</button>
		<button class="basvuru-back-button w-sm-100 ml-md-3 talep-buton"><a href="kullanici-profil-bilgileri"><?php echo
				$textCagir['menu']['profile-don']; ?></a>
		</button>
	</div>

</section>