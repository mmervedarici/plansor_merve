<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/anime.min.js"></script>
<script src="../assets/js/mask.js"></script>
<script src="../assets/js/swiper.js"></script>
<script src="../assets/bootstrap/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/hcOffCanvasNav.min.js"></script>
<script src="../assets/js/fancybox.min.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/form.js"></script>
<script src="../assets/js/basvuru.js"></script>
<script src="../assets/js/formParola.js"></script>
<script src="../assets/js/odeme.js"></script>
<script src="../assets/js/scripts.js"></script>
<script src="../assets/js/detaybuton.js"></script>


<script>
    $('#myTab a[href="#profile"]').tab('show') // Select tab by name
    $('#myTab li:first-child a').tab('show') // Select first tab
    $('#myTab li:last-child a').tab('show') // Select last tab
    $('#myTab li:nth-child(1) a').tab('show') // Select third tab
</script>

<script>
    var swiper3 = new Swiper("#haberSwiper", {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 2,
        navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
        breakpoints: {
            480: {slidesPerView: 2, spaceBetween: 20},
            768: {slidesPerView: 2, spaceBetween: 20},
            992: {slidesPerView: 4, spaceBetween: 40}
        }

    });
    var swiper4 = new Swiper(".detayGaleri", {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 2,
        navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
        breakpoints: {
            480: {slidesPerView: 2, spaceBetween: 20},
            768: {slidesPerView: 2, spaceBetween: 20},
            992: {slidesPerView: 4, spaceBetween: 40}
        }

    });
    var swiper5 = new Swiper(".detayGaleri2", {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 2,
        navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
        breakpoints: {
            480: {slidesPerView: 2, spaceBetween: 20},
            768: {slidesPerView: 2, spaceBetween: 20},
            992: {slidesPerView: 6, spaceBetween: 40}
        }

    });
</script>
<script>
    // Başvuru hizmet seç sayfası
    // hizmetlerin value değerlerinin form inputlara aktarılması

    function hizmetlerValue(event) {
        document.querySelector("#urunID").value = event.value;
    }

    console.log(document.querySelector("#hizmetDigerMesaj"))
    $("#hizmetDigerMesaj").on("input", (e) => {
        let mesajText = e.target.value;
        document.querySelector("#mesaj").value = mesajText;
        document.querySelector("#urunID").value = "11";
        if (mesajText === "") {
            document.querySelector("#urunID").value = "0";
        }
    })
</script>
<script>
    var fullPath = document.getElementById('upload').value;
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        console.log(filename);
    }
</script>
<script>
    const dosya = document.querySelectorAll(".takip-tablo-islemler-item.dosya")
    const dosyaYuklemeEkrani = document.querySelector(".dosyaYuklemeWrapper")
    dosya.forEach((el, index) => {
        el.onclick = () => {
            $("#siparisID").val(index);
            dosyaYuklemeEkrani.classList.remove("d-none")
        }
    })
    $("#file").on("input", (e) => {
        let filepath = $("#file").val().split(/(\\|\/)/g).pop()
        let fileNameText = document.createElement("DIV")
        fileNameText.innerText = filepath
        $(".filename").append(fileNameText)
    })
</script>

<script>

    $.ajax({
        type: 'GET',
        url: 'http://localhost/plansor/tr/kullanici-basvuru-takip',
        success: function (ajaxCevap) {
            console.log(ajaxCevap)
        }
    });

</script>
</body>
</html>