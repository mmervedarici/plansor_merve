<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $temaConfig['title']; ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/swiper.css">
    <link rel="stylesheet" href="../assets/css/hcOffCanvasNav.min.css">
    <link rel="stylesheet" href="../assets/css/fancybox.min.css">
    <link rel="stylesheet" href="../assets/css/animate.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
	<link rel="shortcut icon" href="../assets/img/favicon.png" />

</head>
<body>

<?php
$value = explode("/", $_SERVER['REQUEST_URI']);
?>

<main class="mainAnasayfa" id="mainDiv">
    <header>
        <div class="container">
            <div class="row ">
                <div class="col-12 col-lg-3 text-lg-left text-center header-phone">
                    <ul class="ustDil">
                        <li>
                            <a href="<?php echo $siteBilgi['siteURL']; ?>tr/<?php echo end($value); ?>" title="Türkçe" class="tr"></a>
                        </li>
                        <li>
                            <a href="<?php echo $siteBilgi['siteURL']; ?>en/<?php echo end($value); ?>" title="İngilizce" class="en"></a>
                        </li>
                        <li>
                            <a href="<?php echo $siteBilgi['siteURL']; ?>de/<?php echo end($value); ?>" title="Almanca" class="de"></a>
                        </li>
                    </ul>
                    <div class="mobil-header-wrapper col-12 justify-content-around">
						<?php if ($hesapData['login'] == 0) { ?>
                            <a class="header-btn kullanici " href="kullanici-giris">
                                <span><?php echo $textCagir['menu']['giris-yap']; ?></span>
                                <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                     style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">
            <path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
            c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
            c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
            c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
            c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
            c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
            c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
            c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
            C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                    <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
            c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                    <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
            c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                    <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
            c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                    <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
            C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                    <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
            C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                    <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
            C14.2,2.6,13.6,2.7,13.3,3.3z"/>
        </svg>
                            </a>
						<?php } ?>
						<?php if ($hesapData['login'] == 1) { ?>
							<?php
							$link = 'kullanici-basvuru-takip';
							$text = $textCagir['form']['kullanici-panel'];
							$class = 'basvuru';
							if ($_SESSION['hbt_seviye'] == 2) {
								$link = 'bayi-basvuru-takip';
								$text = $textCagir['form']['bayi-panel'];
								$class = 'basvuru';
							}
							?>
                            <a class="header-btn <?php echo $class; ?>" href="<?php echo $link; ?>">
                                <span><?php echo $text; ?></span>
								<? //xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                     style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">

								<path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
									c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
									c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
									c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
									c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
									c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
									c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
									c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
									C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                    <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
									c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                    <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
									c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                    <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
											c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                    <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
									C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                    <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
										C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                    <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
												C14.2,2.6,13.6,2.7,13.3,3.3z"/>
							</svg>
                            </a>
						<?php } ?>

                        <a class="header-btn basvuru" href="basvuru-yap">
                            <span><?php echo $textCagir['menu']['basvuru-yap']; ?></span>
                            <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                 style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">
        <style type="text/css">
            .st0 {
                fill: #ffff;
            }
        </style>
                                <path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
        c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
        c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
        c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
        c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
        c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
        c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
        c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
        C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
        c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
        c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
        c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
        C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
        C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
        C14.2,2.6,13.6,2.7,13.3,3.3z"/>
    </svg>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-9 text-right" id="navbarUst">
					<?php if ($hesapData['login'] == 0) { ?>
                        <a class="header-btn kullanıcı " href="bayi-giris">
                            <span><?php echo $textCagir['form']['bayi-giris']; ?></span>
							<? //xml version="1.0" encoding="utf-8"?>
                            <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                 style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">
								<path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
									c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
									c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
									c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
									c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
									c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
									c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
									c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
									C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
									c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
								 c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
										 c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
										C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
									 C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
										C14.2,2.6,13.6,2.7,13.3,3.3z"/>
							</svg>
                        </a>
					<?php } ?>
					<?php if ($hesapData['login'] == 0) { ?>
                        <a class="header-btn kullanıcı " href="kullanici-giris">
                            <span><?php echo $textCagir['menu']['kullaniciGirisi']; ?></span>
							<? //xml version="1.0" encoding="utf-8"?>
                            <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                 style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">

								<path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
									c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
									c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
									c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
									c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
									c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
									c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
									c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
									C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
									c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
									c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
											c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
									C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
										C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
												C14.2,2.6,13.6,2.7,13.3,3.3z"/>
							</svg>
                        </a>
					<?php } ?>
					<?php if ($hesapData['login'] == 1) { ?>
						<?php
						$link = 'kullanici-basvuru-takip';
						$text = $textCagir['form']['kullanici-panel'];
						$class = 'basvuru';
						if ($_SESSION['hbt_seviye'] == 2) {
							$link = 'bayi-basvuru-takip';
							$text = $textCagir['form']['bayi-panel'];
							$class = 'basvuru';
						}
						?>
                        <a class="header-btn <?php echo $class; ?>" href="<?php echo $link; ?>">
                            <span><?php echo $text; ?></span>
							<? //xml version="1.0" encoding="utf-8"?>
                            <span class="fa fa-user-cog"></span>
                        </a>
					<?php } ?>
					<?php if ($hesapData['seviye'] == 1) { ?>
                        <a class="header-btn basvuru" href="basvuru-yap">
                            <span><?php echo $textCagir['menu']['basvuruYap']; ?></span>

							<? //xml version="1.0" encoding="utf-8"?>
                            <svg version="1.1" id="katman_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25.1 23.3"
                                 style="enable-background:new 0 0 25.1 23.3;" xml:space="preserve">
								<style type="text/css">
                                    .st0 {
                                        fill: #ffff;
                                    }
                                </style>
                                <path class="st0" d="M6.4,20.1l-1.6-0.9c-2.3-1.3-3.1-4.3-1.8-6.6l2.7-4.7l0,0c0.5-0.7,1.5-0.9,2.2-0.5l0,0c0.3,0.1,0.5,0.4,0.7,0.6
									c0.6-0.7,1.5-0.8,2.3-0.4c0.3,0.2,0.5,0.4,0.6,0.6c0.6-0.4,1.4-0.5,2-0.1c0.2,0.1,0.4,0.3,0.5,0.5l1.1-1.8l0.1-0.1
									c0.7-0.7,1.8-0.8,2.5-0.1c0.5,0.5,0.7,1.2,0.5,1.9v0.1l-3.5,6.1l0.6-0.4c0.6-0.4,1.3-0.5,2-0.4C18,14,18.6,14.4,19,15
									c0.3,0.4,0.2,0.9-0.2,1.2c0,0,0,0,0,0l-5.1,3.6C11.5,21.3,8.7,21.4,6.4,20.1L6.4,20.1z M17.1,14.9c-0.4-0.1-0.9,0-1.2,0.2l-2.3,1.5
									c-0.2,0.1-0.5,0.1-0.6-0.1c-0.2-0.2-0.2-0.4-0.1-0.6l4.6-8c0.1-0.3,0-0.7-0.3-0.9c-0.3-0.2-0.7-0.1-1,0.1l-1.8,3.1
									c-0.1,0.2-0.4,0.3-0.7,0.2c-0.2-0.1-0.3-0.4-0.2-0.6c0.2-0.4,0-0.8-0.3-1c-0.3-0.2-0.8-0.1-1,0.2l-0.5,0.8c-0.1,0.2-0.4,0.3-0.7,0.2
									c-0.2-0.1-0.3-0.4-0.2-0.7c0,0,0,0,0,0c0.2-0.4,0.1-0.8-0.3-1s-0.8-0.1-1,0.3l0,0l-0.6,1C8.7,9.8,8.4,9.8,8.2,9.7C8,9.6,8,9.4,8,9.2
									c0.2-0.4,0-0.8-0.4-1C7.3,8,6.9,8.1,6.7,8.4L4,13.1c-1.1,1.8-0.4,4.2,1.4,5.3l1.5,0.9c1.9,1.1,4.3,1,6.2-0.2l5-3.5
									C17.9,15.2,17.5,14.9,17.1,14.9z M18.2,15.4L18.2,15.4z"/>
                                <path class="st0" d="M12.2,13.5c0.2,0,0.3-0.1,0.4-0.2l1.9-3.3c0.1-0.2,0-0.5-0.2-0.7c-0.2-0.1-0.6-0.1-0.7,0.2l-1.9,3.2
									c-0.1,0.2-0.1,0.5,0.2,0.7C12,13.5,12.1,13.5,12.2,13.5z"/>
                                <path class="st0" d="M10,12.3c0.2,0,0.3-0.1,0.4-0.2L12,9.2c0.1-0.2,0.1-0.6-0.2-0.7c-0.2-0.1-0.5-0.1-0.7,0.2c0,0,0,0,0,0l-1.6,2.8
										c-0.1,0.2-0.1,0.5,0.2,0.7c0,0,0,0,0,0C9.8,12.3,9.9,12.3,10,12.3z"/>
                                <path class="st0" d="M7.8,11c0.2,0,0.3-0.1,0.4-0.2l1.2-2.1C9.6,8.4,9.5,8.1,9.2,8C9,7.8,8.7,7.9,8.6,8.2l-1.2,2.1
										c-0.1,0.2,0,0.5,0.2,0.7C7.6,11,7.7,11,7.8,11z"/>
                                <path class="st0" d="M17.7,3.1c-0.4,0.7-0.2,1.7,0.6,2.1c0.7,0.4,1.7,0.2,2.1-0.6s0.2-1.7-0.6-2.1c0,0,0,0,0,0
									C19.1,2.1,18.2,2.3,17.7,3.1z"/>
                                <path class="st0" d="M20.6,7.5C20.3,8,20.5,8.7,21,9c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5C21.6,6.8,20.9,7,20.6,7.5
									C20.6,7.5,20.6,7.5,20.6,7.5z"/>
                                <path class="st0" d="M13.3,3.3c-0.3,0.5-0.1,1.2,0.4,1.5c0.5,0.3,1.2,0.1,1.5-0.4c0.3-0.5,0.1-1.2-0.4-1.5
								C14.2,2.6,13.6,2.7,13.3,3.3z"/>
							</svg>
                        </a>
					<?php } ?>
                </div>

                <div class="col-12 col-lg-12 text-right ">
                    <nav class="navbar navbar-expand-lg p-0">
                        <a class="navbar-brand siteLogo" href="./">
                            <img src="../assets/img/katmanlar/logo-2.png" alt="Plansor"/>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item <?php echo $theme->activeLink(); ?>">
                                    <a class="nav-link" href="./">
                                        <i class="fas fa-home"></i> <?php echo $textCagir['menu']['anaSayfa']; ?>
                                    </a>
                                </li>
                                <li class="nav-item dropdown <?php echo $theme->activeLink('kurumsal'); ?>">
                                    <a class="nav-link dropdown-toggle" href="kurumsal">
                                        <i class="far fa-globe"></i> <?php echo $textCagir['menu']['kurumsal']; ?>
                                    </a>
                                    <ul class="dropdown-menu fade-down">
                                        <li>
                                            <a class="dropdown-item" href="hakkimizda"><?php echo $textCagir['menu']['hakkimizda']; ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="vizyon"><?php echo $textCagir['menu']['vizyon'];
												?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="misyon"><?php echo $textCagir['menu']['misyon'];
												?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="degerlerimiz"><?php echo $textCagir['menu']['degerlerimiz']; ?></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown <?php echo $theme->activeLink('hizmetler'); ?>">
                                    <a class="nav-link dropdown-toggle" href="hizmetler">
                                        <i class="far fa-cogs"></i> <?php echo $textCagir['menu']['hizmetler']; ?>
                                    </a>
                                    <ul class="dropdown-menu fade-down">
                                        <li class="nav-item right dropdown">
                                            <a class="dropdown-item" href="imar">
												<?php echo $textCagir['menu']['imar']; ?>
                                            </a>
                                            <ul class="right-menu fade-down pl-0 pb-2">
                                                <li>
                                                    <a class="dropdown-item" href="imar-durum-bilgisi"><?php echo $textCagir['menu']['imar-durum-bilgisi']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="parsele-kac-daire-sigar"><?php echo $textCagir['menu']['parsele-kac-daire-sigar']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="kat-irtifaki-kurulmasi"><?php echo $textCagir['menu']['kat-irtifaki-kurulmasi']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="proje-yatirim-yonlendirme"><?php echo $textCagir['menu']['proje-yatirim-yonlendirme']; ?></a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item right dropdown">
                                            <a class="dropdown-item" href="tapu"><?php echo $textCagir['menu']['tapu']; ?></a>
                                            <ul class="right-menu fade-down pl-0 pb-2">
                                                <li>
                                                    <a class="dropdown-item" href="mulkiyet-analizi"><?php echo $textCagir['menu']['mulkiyet-analizi']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="hisse-cozumleme"><?php echo $textCagir['menu']['hisse-cozumleme']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="emsal-deger-belirleme"><?php echo $textCagir['menu']['emsal-deger-belirleme']; ?></a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="intikal-islemleri"><?php echo $textCagir['menu']['intikal-islemleri']; ?></a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item right dropdown">
                                            <a class="dropdown-item" href="diger"><?php echo $textCagir['menu']['diger']; ?></a>

                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="javascript:void(0);">
                                        <i class="fas fa-hands-helping"></i> <?php echo $textCagir['menu']['isOrtakligi']; ?>
                                    </a>
                                    <ul class="dropdown-menu fade-down">
                                        <li>
                                            <a class="dropdown-item" href="sozlesmeler"><?php echo $textCagir['menu']['sozlesmeler']; ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="avantajlar"><?php echo $textCagir['menu']['avantajlar']; ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="kullanici-giris"><?php echo
	                                            $textCagir['menu']['basvuru-yap']; ?></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="javascript:void(0);">
                                        <i class="far fa-newspaper"></i> <?php echo $textCagir['menu']['bulten']; ?>
                                    </a>
                                    <ul class="dropdown-menu fade-down">
                                        <li>
                                            <a class="dropdown-item" href="haberler"><?php echo $textCagir['menu']['haber']; ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="blog"><?php echo $textCagir['menu']['blog']; ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="basinda-biz"><?php echo $textCagir['menu']['basinda-biz']; ?></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item <?php echo $theme->activeLink('iletisim'); ?>">
                                    <a class="nav-link" href="iletisim">
                                        <i class="far fa-phone"></i><?php echo $textCagir['menu']['iletisim']; ?></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

