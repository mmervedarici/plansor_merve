<footer class="wow fadeIn" data-wow-delay="300ms">
    <div class="container pb-5">
        <div class="row my-5">
            <div class="col-lg-8 offset-lg-2 text-center mt-5">
                <div class="footerLogo">
                    <img src="../assets/img/katmanlar/logo-1.png" alt="Plansor">
                </div>
            </div>
            <div class="col-12 text-center footerSlogan my-lg-4"></div>
            <div class="col-12 mt-lg-5">
                <div class="row">
                    <div class="col-12 col-lg-4 mt-5 order-lg-0 order-1">
                        <div class="footerBaslik wow fadeInUp" data-wow-delay="400ms">
							<?php echo $textCagir['menu']['hizli-erisim']; ?>
                        </div>
                        <ul class="footerHizliErisim row no-gutters">
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="./" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['anaSayfa']; ?>

                                            </span>
                                </a>
                            </li>
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="tapu" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['tapu']; ?>
                                            </span>
                                </a>
                            </li>
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="kurumsal" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['kurumsal']; ?>
                                            </span>
                                </a>
                            </li>
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="imar" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['imar']; ?>
                                            </span>
                                </a>
                            </li>
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="hizmetler" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['hizmetler']; ?>
                                            </span>
                                </a>
                            </li>
                            <li class="col-6 wow zoomIn" data-wow-delay="500ms">
                                <a href="iletisim" class="col-12" style="text-align: start; padding: 0;">
                                    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 22.22 24.88'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bisolation:isolate;%7D.cls-2%7Bfill:%233e5967;%7D.cls-3%7Bmix-blend-mode:multiply;opacity:0.3;%7D.cls-4%7Bfill:%23343434;%7D%3C/style%3E%3C/defs%3E%3Cg class='cls-1'%3E%3Cg id='katman_1' data-name='katman 1'%3E%3Cpolygon class='cls-2' points='5.26 1.16 5.26 4.57 16.37 12.44 5.26 20.31 5.26 23.72 21.18 12.44 5.26 1.16'/%3E%3Cg class='cls-3'%3E%3Cpolygon class='cls-4' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3Cpolygon class='cls-2' points='1.04 4.5 1.04 20.38 12.25 12.44 1.04 4.5'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
                                         alt="">
                                    <span>
                                    <?php echo $textCagir['menu']['iletisim']; ?>
                                            </span>
                                </a>
                            </li>
                        </ul>
                    </div>


                    <div class="col-12 col-lg-4 mt-5 order-lg-1 order-0">
                        <div class="footerBaslik wow fadeInUp" data-wow-delay="400ms">
							<?php echo $textCagir['menu']['iletisim-bilgileri']; ?>
                        </div>

                        <ul class="footerIletisimBilgileri row">
                            <li class="col-12 wow zoomIn" data-wow-delay="700ms">
                                <a href="tel:+9<?php echo $siteBilgi['data']["sabitTelefon"]; ?>">
                                    <div class="row no-gutters">
                                        <div
                                                class="col-2 text-center d-flex justify-content-center align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="22.171"
                                                 height="22.187" viewBox="0 0 22.171 22.187">
                                                <g id="Group_52" data-name="Group 52"
                                                   transform="translate(-829.077 -1312.773)">
                                                    <path id="Path_103" data-name="Path 103"
                                                          d="M844.163,1333.4c1.34,0,2.638.053,3.93-.019a1.908,1.908,0,0,0,1.6-1.8c0-2.995.059-5.995-.11-8.982a8.446,8.446,0,0,0-3.7-6.342,9.472,9.472,0,0,0-14.969,5.629,15.73,15.73,0,0,0-.283,2.854c-.027.475-.308.507-.639.5s-.62.009-.649-.485a11.3,11.3,0,0,1,7.269-11.083,10.816,10.816,0,0,1,14.021,7.46,11.786,11.786,0,0,1,.338,2.68c.044,2.449.017,4.9.014,7.35a3.32,3.32,0,0,1-3.53,3.55q-3.9,0-7.8,0a1.957,1.957,0,0,1-2.144-1.968,1.983,1.983,0,0,1,2.157-2.021c.754-.006,1.508,0,2.261,0a1.979,1.979,0,0,1,2.233,2.262C844.161,1333.093,844.163,1333.206,844.163,1333.4Zm-3.349.016c.469,0,.938,0,1.408,0a.619.619,0,0,0,.691-.648.628.628,0,0,0-.661-.739q-1.407-.025-2.815,0c-.435.008-.63.286-.626.706a.6.6,0,0,0,.651.677C839.913,1333.42,840.363,1333.415,840.814,1333.415Z"
                                                          fill="none" stroke="#000" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="0.5"/>
                                                    <path id="Path_104" data-name="Path 104"
                                                          d="M842.923,1325.255c0-.847-.008-1.693,0-2.54a1.37,1.37,0,0,1,1.51-1.5,9.828,9.828,0,0,1,1.684.076,2.448,2.448,0,0,1,2.123,2.3,22.433,22.433,0,0,1,0,3.269,2.609,2.609,0,0,1-2.684,2.442c-.394.009-.79.011-1.185,0a1.365,1.365,0,0,1-1.451-1.451C842.913,1326.986,842.923,1326.121,842.923,1325.255Zm4.063.017c0-.451.009-.9,0-1.353a1.374,1.374,0,0,0-1.378-1.407c-.3-.011-.6.015-.9-.008-.384-.029-.478.151-.473.5.014,1.2.005,2.4.005,3.606,0,1.409,0,1.409,1.391,1.369a1.333,1.333,0,0,0,1.356-1.357C847,1326.174,846.985,1325.723,846.986,1325.272Z"
                                                          fill="none" stroke="#000" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="0.5"/>
                                                    <path id="Path_105" data-name="Path 105"
                                                          d="M837.426,1325.266c0,.847.011,1.694,0,2.54a1.368,1.368,0,0,1-1.521,1.495,9.8,9.8,0,0,1-1.627-.079,2.576,2.576,0,0,1-2.2-2.543c-.03-.959-.029-1.92,0-2.879a2.656,2.656,0,0,1,2.613-2.59c.414-.012.828,0,1.242,0a1.366,1.366,0,0,1,1.5,1.46c.018.432.005.865.005,1.3Zm-1.3.048v-1.467c0-1.421,0-1.421-1.433-1.337a1.344,1.344,0,0,0-1.309,1.3q-.031,1.467,0,2.935a1.287,1.287,0,0,0,1.231,1.236c.357.016.716-.016,1.072.008s.45-.142.443-.469C836.117,1326.782,836.128,1326.048,836.127,1325.314Z"
                                                          fill="none" stroke="#000" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="0.5"/>
                                                </g>
                                            </svg>


                                        </div>
                                        <div class="col-10">
											<?php echo $siteBilgi['data']["sabitTelefon"]; ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="col-12 wow zoomIn" data-wow-delay="700ms">
                                <a href="tel:+9<?php echo $siteBilgi['data']["telefon"]; ?>">
                                    <div class="row no-gutters">
                                        <div
                                                class="col-2 text-center d-flex justify-content-center align-items-center">
                                            <i class="far fa-lg fa-phone-alt mb-1 font-weight-light"></i>
                                        </div>
                                        <div class="col-10">
											<?php echo $siteBilgi['data']["telefon"]; ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="col-12 wow zoomIn" data-wow-delay="700ms">
                                <a href="mailto:info@plansor.com.tr">
                                    <div class="row no-gutters">
                                        <div
                                                class="col-2 text-center d-flex justify-content-center align-items-center">
                                            <svg id="katman_1" data-name="katman 1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 21.31 14.55">
                                                <defs>
                                                    <style>
                                                        .cls-1 {
                                                            fill: #010101;
                                                        }
                                                    </style>
                                                </defs>
                                                <path class="cls-1"
                                                      d="M21.14.33s0,0,0-.06l0-.06,0,0A.31.31,0,0,0,20.9.11H.41A.28.28,0,0,0,.25.17l0,0,0,.06V.33s0,0,0,0V14.19a.25.25,0,0,0,.25.25H20.9a.25.25,0,0,0,.25-.25V.36S21.14.34,21.14.33ZM.66.92l7,6.29-7,6.29Zm11.71,7.4.87-.78,7,6.27H1.06l7-6.27.87.78a2.59,2.59,0,0,0,3.43,0Zm1.24-1.11,7-6.29V13.5ZM20.24.61,12,7.94a2.07,2.07,0,0,1-2.77,0L1.06.61Z"/>
                                            </svg>

                                        </div>
                                        <div class="col-10">
											<?php echo $siteBilgi['data']["eposta"]; ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="col-12 wow zoomIn" data-wow-delay="700ms">
                                <div class="row no-gutters">
                                    <div
                                            class="col-2 text-center d-flex justify-content-center align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14.567"
                                             height="21.023" viewBox="0 0 14.567 21.023">
                                            <path id="Path_100" data-name="Path 100"
                                                  d="M846.261,1448.665a7.017,7.017,0,0,0-14.035,0s-.49,6.867,7.017,13.506C846.751,1455.532,846.261,1448.665,846.261,1448.665Zm-7.018,2.962a2.961,2.961,0,1,1,2.962-2.962A2.962,2.962,0,0,1,839.243,1451.627Z"
                                                  transform="translate(-831.96 -1441.398)" fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-width="0.5"/>
                                        </svg>

                                    </div>
                                    <div class="col-10">
										<?php echo $siteBilgi['data']["adres"]; ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="col-12 col-lg-4 mt-5 order-lg-2 order-2">
                        <div class="footerBaslik wow fadeInUp" data-wow-delay="400ms">
							<?php echo $textCagir['menu']['tum-platformlarda']; ?>
                        </div>

                        <div class="telTextImg mt-4">
                            <div class="row">
                                <div class="col-12 text-center my-2 wow fadeInUp" data-wow-delay="400ms">
                                    <a href="./">
                                        <img class="img-fluid" src="../assets/img/katmanlar/1.png" alt="plansor"></a>
                                </div>
                                <div class="col-12 text-center my-2 wow fadeInUp" data-wow-delay="500ms">
                                    <a href="./">
                                        <img class="img-fluid" src="../assets/img/katmanlar/2.png" alt="plansor"></a>
                                </div>
                                <div class="col-12 text-center my-2 wow fadeInUp" data-wow-delay="600ms">
                                    <a href="./">
                                        <img class="img-fluid" src="../assets/img/katmanlar/3.png" alt="plansor"></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-alt-bg">
        <div class="footer-alt-ic container">
            <div class="row">
                <div class="col-4 d-flex justify-content-between">
                    <div class="footer-ust-bg">
                        <div class="footer-ust-img">
                            <div class="hakanbt-logo">
                                <div class="hakanbt">
                                    <a id="hakanbiltek" href="https://www.hakanbt.com.tr" title="Sivas Web Tasarım">
                                        <img src="../assets/img/hakanbt.png" alt="Sivas Web Tasarım">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <p class="ml-auto my-0 telif-hakki-renk font-weight-bolder">  <?php echo $textCagir['menu']['telif'];
						?></p>
                </div>
            </div>
        </div>
    </div>
</footer>


<a href="#" class="scrollup">
    <i class="fas fa-chevron-up"></i>
</a>

<ul class="sol-iletisim-menu d-none d-lg-block">
    <li>
        <a href="<?php echo $siteBilgi['data']["facebook"]; ?>">
            <i class="fab fa-facebook-f"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $siteBilgi['data']["instagram"]; ?>">
            <i class="fab fa-instagram"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $siteBilgi['data']["twitter"]; ?>">
            <i class="fab fa-twitter"></i>
        </a>
    </li>
    <li>
        <a href="<?php echo $siteBilgi['data']["youtube"]; ?>">
            <i class="fab fa-youtube"></i>
        </a>
    </li>
</ul>

</main>

<?php include "js.php"; ?>