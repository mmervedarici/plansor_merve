<?php
include_once('Controller/haberController.php');
$haberController = new haberController($db);
$haberList = $haberController->haberListesi(false,10);
?>
<section>
    <div class="container">
        <div class="row my-5">
            <div class="col-12 col-lg-3 ortalaX">
                <div class="plansorBaslik">
                    <h4><?php echo $textCagir['menu']['haber']; ?></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="hizmetUcgen1 haberler"></div>
            </div>
            <div class="col-12">
                <div class="6" #swiperRef="" class="swiper haberSwiper" id="haberSwiper">
                    <div class="swiper-wrapper">
                        <?php foreach ($haberList as $item): ?>
                          <div class="swiper-slide">
                            <a class="haberDis" href="<?php echo $item["link"]; ?>">
                                <div class="haberImg">
                                    <img class="img-fluid" src="../userFiles/resimler/<?php echo $item["resim"]; ?>" alt="">
                                </div>
                                <div class="haberTarih">
                                    <div class="haberTarihIc">
                                        <?php echo $utility->tarihYazdir(substr($item["kayitTarih"],0,10)); ?>
                                    </div>
                                </div>
                                <div class="haberText">
                                    <p><?php echo $item["baslik"]; ?></p>
                                </div>
                                <div class="haberDevami"><?php echo $textCagir['icSayfa']['detayli-incele']; ?>
                                    <svg id="katman_2" data-name="katman 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.11 23.29">
                                        <defs>
                                            <style>.cls-1 {
                                                fill: #ffffff;
                                            }

                                            .cls-2 {
                                                fill: #ffffff;
                                            }</style>
                                        </defs>
                                        <path class="cls-1" d="M6.45,20.11l-1.56-.9a4.88,4.88,0,0,1-1.78-6.65L5.83,7.85l0,0a1.78,1.78,0,0,1,2.23-.54h0a1.72,1.72,0,0,1,.69.62A1.79,1.79,0,0,1,11,7.54a1.87,1.87,0,0,1,.62.63,1.79,1.79,0,0,1,2-.13,1.85,1.85,0,0,1,.53.51l1.06-1.84.06-.07a1.78,1.78,0,0,1,3,1.76l0,.08-3.54,6.15.57-.37a2.61,2.61,0,0,1,2-.39A2.64,2.64,0,0,1,19,15a.87.87,0,0,1-.24,1.21l-5.09,3.57a6.75,6.75,0,0,1-7.24.32Zm10.69-5.25a1.62,1.62,0,0,0-1.25.24l-2.33,1.53a.5.5,0,0,1-.62-.06.49.49,0,0,1-.09-.61L17.41,8a.78.78,0,0,0-.34-.89.8.8,0,0,0-1,.13l-1.77,3.07a.52.52,0,0,1-.67.2.51.51,0,0,1-.23-.65.78.78,0,0,0-.3-1,.79.79,0,0,0-1,.19l-.46.81a.51.51,0,0,1-.68.19.5.5,0,0,1-.2-.68.76.76,0,0,0-1.31-.78l-.57,1A.5.5,0,0,1,8,9.17a.77.77,0,0,0-1.31-.79L4,13.06A3.87,3.87,0,0,0,5.4,18.34l1.55.9A5.77,5.77,0,0,0,13.11,19l5-3.51A1.63,1.63,0,0,0,17.14,14.86Zm1.07.53h0Z"/>
                                        <path class="cls-1" d="M12.17,13.54a.51.51,0,0,0,.44-.25L14.49,10a.52.52,0,0,0-.19-.69.51.51,0,0,0-.69.19l-1.87,3.25a.51.51,0,0,0,.18.69A.57.57,0,0,0,12.17,13.54Z"/>
                                        <path class="cls-1" d="M10,12.28a.51.51,0,0,0,.44-.25L12,9.24a.51.51,0,0,0-.19-.69.49.49,0,0,0-.68.19L9.55,11.52a.5.5,0,0,0,.18.69A.47.47,0,0,0,10,12.28Z"/>
                                        <path class="cls-1" d="M7.79,11a.52.52,0,0,0,.44-.25l1.2-2.09a.5.5,0,1,0-.87-.5L7.35,10.26a.52.52,0,0,0,.19.69A.54.54,0,0,0,7.79,11Z"/>
                                        <path class="cls-2" d="M17.74,3.06a1.57,1.57,0,1,0,2.14-.57A1.57,1.57,0,0,0,17.74,3.06Z"/>
                                        <path class="cls-2" d="M20.59,7.51A1.1,1.1,0,1,0,22.1,7.1,1.1,1.1,0,0,0,20.59,7.51Z"/>
                                        <path class="cls-2" d="M13.26,3.27a1.1,1.1,0,1,0,1.51-.4A1.11,1.11,0,0,0,13.26,3.27Z"/>
                                    </svg>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</div>
</section>

