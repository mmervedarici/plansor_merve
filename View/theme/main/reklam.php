<section>
    <div class="container my-5 pt-5">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="telImgDis">
                    <div class="telImgCircle">
                        <img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>assets/img/katmanlar/telefon.png" alt="plansor"/>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="telText mt-4 mt-lg-0">
                    <h4>
						<?php echo $textCagir['anaSayfa']['cok-yakinda']; ?> <br>
                    </h4>
                    <span><?php echo $textCagir['anaSayfa']['reklam-mesaj']; ?></span>
                </div>
                <div class="telTextBaslik">
					<?php echo $textCagir['anaSayfa']['download']; ?>
                </div>
                <div class="telTextImg mt-4">
                    <div class="row">
                        <div class="col-12 col-lg-4 text-center my-2">
                            <a href="javascript:void(0);"><img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>assets/img/katmanlar/1.png" alt="plansor"/></a>
                        </div>
                        <div class="col-12 col-lg-4 text-center my-2">
                            <a href="javascript:void(0);"><img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>assets/img/katmanlar/2.png" alt="plansor"/></a>
                        </div>
                        <div class="col-12 col-lg-4 text-center my-2">
                            <a href="javascript:void(0);"><img class="img-fluid" src="<?php echo $siteBilgi['siteURL']; ?>assets/img/katmanlar/3.png" alt="plansor"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>