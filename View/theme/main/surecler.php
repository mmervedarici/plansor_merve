<section class="renkBg">
    <section>
        <div class="container my-lg-5">
            <div class="row">
                <div class="col-12 col-lg-3 ortalaX">
                    <div class="plansorBaslik ust">
                        <h4><?php echo $textCagir['anaSayfa']['surecler'];?></h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="circle1 surec"></div>
                <div class="circle2 surec"></div>
                <div class="cizgi surec"></div>
                <div class="ucgen"></div>
                <div class="cizgi2"></div>
            </div>
            <div class="row">
                <div class="col-6 col-lg">
                    <div class="surecDis">
                        <div class="surecIcon">
                            <i class="far fa-cogs"></i>
                        </div>
                        <div class="surecText">
                            <P> <?php echo $textCagir['anaSayfa']['hizmet'];?> <br><?php echo $textCagir['anaSayfa']['sec'];?></P>
                            <div class="number">1</div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg">
                    <div class="surecDis">
                        <div class="surecIcon"><i class="far fa-file-invoice"></i></div>
                        <div class="surecText">
                            <p><?php echo $textCagir['anaSayfa']['bilgi'];?> <br> <?php echo $textCagir['anaSayfa']['gir'];?></p>
                            <div class="number">2</div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg">
                    <div class="surecDis">
                        <div class="surecIcon"><i class="far fa-credit-card"></i></div>
                        <div class="surecText">
                            <P><?php echo $textCagir['anaSayfa']['odeme'];?> <br> <?php echo $textCagir['anaSayfa']['yap'];?></P>
                            <div class="number">3</div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg">
                    <div class="surecDis">
                        <div class="surecIcon"><i class="far fa-thumbs-up"></i></div>
                        <div class="surecText">
                            <P><?php echo $textCagir['anaSayfa']['talebini'];?> <br> <?php echo
	                            $textCagir['anaSayfa']['tamamla'];?></P>
                            <div class="number">4</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg">
                    <a href="basvuru-yap">
                        <div class="surecDis active">
                            <div class="surecIcon"><i class="far fa-lightbulb-on"></i></div>
                            <div class="surecText">
                                <p> <?php echo $textCagir['anaSayfa']['hemen-talep-olustur'];?></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>