<?php
include_once('Controller/sliderController.php');
$sliderController = new sliderController($db);
$slider = $sliderController->slider();
?>
<section class="anaSliderSection wow fadeInDown">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="cizgi"></div>
            </div>
            <div class="col-12 dalBg">
                <div class="swiper-container" id="anaSlider">
                    <div class="swiper-wrapper">
						<?php foreach ($slider as $item) : ?>
                            <div class="swiper-slide">
                                <div class="row no-gutters">
                                    <div class="col-12 col-lg-6 order-0 order-lg-1">
                                        <div class="slideGorselAlani">
                                            <img src="../<?php echo $item["resim"] ?>" alt="slider" class="img-fluid"/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-5 order-1 order-lg-0">
                                        <div class="slideIcerikAlani">
                                            <div class="d-block">
                                                <div class="slideAnaBaslik text-center text-lg-right">
													<?php echo $item["baslik"] ?>
                                                </div>
                                                <div class="slideAltBaslik text-center text-lg-right">
													<?php echo $item["altBaslik"] ?>
                                                </div>
                                                <a href="<?php echo $item["link1"] ?>" class="slideIcerikButon">
													<?php echo $textCagir['icSayfa']['detayli-incele']; ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-1 order-2 order-lg-2 paginationCol">
                                        <div class="swiper-pagination wow fadeIn" data-wow-delay="400ms"></div>
                                    </div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slideOk geri anaSliderGeri"></div>
    <div class="slideOk ileri anaSliderIleri"></div>


</section>